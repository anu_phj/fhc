fhc
===

A Symfony project created on July 20, 2015, 9:25 am.
1) Installing the Standard Edition via composer
  If you don't have Composer yet, download it following the instructions on
  http://getcomposer.org/ or just run one of the following command:

    curl -s http://getcomposer.org/installer | php

 
2) Install required dependencies, with following command at Symfony's root folder

    php composer.phar install

3) The "php composer.phar install" command will lead you to fill parameters if "app/config/parameters.yml" file doesnot exists.
   If not exists Copy parameters.yml.dist as parameters.yml and configure accordingly.
   database_host, database_name, database_user, database_password can be changed according to server.

4) Give persmissions to cache and logs folders. 
   <prjoect-root-directory> should be replace with actual name.
 
    chmod -R 777 <prjoect-root-directory>/app/cache
    chmod -R 777 <prjoect-root-directory>/app/logs

5) Create database. At Symfony's root folder run following command

    php app/console doctrine:database:create

5) Update database scheme. At Symfony's root folder run following command.

    php app/console doctrine:schema:update --force
    
6) Assets link to web folder. At Symfony's root folder run following command.

    php app/console assets:install web --symlink

7) To install ACL, run the following command

	php app/console init:acl
	
8) To add dummy user, run the following command	
	
	php app/console dummy:user

9) To upload documents for Users
	
	mkdir data
	mkdir data/uploads
	mkdir data/uploads/user
	chmod -R 777 data/uploads/user

10) To upload company logo

	mkdir data/uploads/company
	chmod -R 777 data/uploads/company