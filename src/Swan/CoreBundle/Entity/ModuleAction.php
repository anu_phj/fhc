<?php

namespace Swan\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ModuleAction
 */
class ModuleAction
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $slug;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $userModuleAction;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $rolePermission;

    /**
     * @var \Swan\CoreBundle\Entity\Module
     */
    private $module;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->userModuleAction = new \Doctrine\Common\Collections\ArrayCollection();
        $this->rolePermission = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ModuleAction
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return ModuleAction
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return ModuleAction
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Add userModuleAction
     *
     * @param \Swan\CoreBundle\Entity\UserModuleAction $userModuleAction
     * @return ModuleAction
     */
    public function addUserModuleAction(\Swan\CoreBundle\Entity\UserModuleAction $userModuleAction)
    {
        $this->userModuleAction[] = $userModuleAction;

        return $this;
    }

    /**
     * Remove userModuleAction
     *
     * @param \Swan\CoreBundle\Entity\UserModuleAction $userModuleAction
     */
    public function removeUserModuleAction(\Swan\CoreBundle\Entity\UserModuleAction $userModuleAction)
    {
        $this->userModuleAction->removeElement($userModuleAction);
    }

    /**
     * Get userModuleAction
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUserModuleAction()
    {
        return $this->userModuleAction;
    }

    /**
     * Add rolePermission
     *
     * @param \Swan\CoreBundle\Entity\RolePermission $rolePermission
     * @return ModuleAction
     */
    public function addRolePermission(\Swan\CoreBundle\Entity\RolePermission $rolePermission)
    {
        $this->rolePermission[] = $rolePermission;

        return $this;
    }

    /**
     * Remove rolePermission
     *
     * @param \Swan\CoreBundle\Entity\RolePermission $rolePermission
     */
    public function removeRolePermission(\Swan\CoreBundle\Entity\RolePermission $rolePermission)
    {
        $this->rolePermission->removeElement($rolePermission);
    }

    /**
     * Get rolePermission
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRolePermission()
    {
        return $this->rolePermission;
    }

    /**
     * Set module
     *
     * @param \Swan\CoreBundle\Entity\Module $module
     * @return ModuleAction
     */
    public function setModule(\Swan\CoreBundle\Entity\Module $module = null)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Get module
     *
     * @return \Swan\CoreBundle\Entity\Module 
     */
    public function getModule()
    {
        return $this->module;
    }
}
