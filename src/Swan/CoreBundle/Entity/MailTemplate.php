<?php

namespace Swan\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MailTemplate
 */
class MailTemplate
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $companyId;

    /**
     * @var string
     */
    private $subject;

    /**
     * @var string
     */
    private $body;

    /**
     * @var integer
     */
    private $mailTypeId;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set companyId
     *
     * @param integer $companyId
     * @return MailTemplate
     */
    public function setCompanyId($companyId)
    {
        $this->companyId = $companyId;

        return $this;
    }

    /**
     * Get companyId
     *
     * @return integer 
     */
    public function getCompanyId()
    {
        return $this->companyId;
    }

    /**
     * Set subject
     *
     * @param string $subject
     * @return MailTemplate
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string 
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set body
     *
     * @param string $body
     * @return MailTemplate
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string 
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set mailTypeId
     *
     * @param integer $mailTypeId
     * @return MailTemplate
     */
    public function setMailTypeId($mailTypeId)
    {
        $this->mailTypeId = $mailTypeId;

        return $this;
    }

    /**
     * Get mailTypeId
     *
     * @return integer 
     */
    public function getMailTypeId()
    {
        return $this->mailTypeId;
    }
    /**
     * @var \Swan\CoreBundle\Entity\MailType
     */
    private $mailType;

    /**
     * @var \Swan\CoreBundle\Entity\Company
     */
    private $company;


    /**
     * Set mailType
     *
     * @param \Swan\CoreBundle\Entity\MailType $mailType
     * @return MailTemplate
     */
    public function setMailType(\Swan\CoreBundle\Entity\MailType $mailType = null)
    {
        $this->mailType = $mailType;

        return $this;
    }

    /**
     * Get mailType
     *
     * @return \Swan\CoreBundle\Entity\MailType
     */
    public function getMailType()
    {
        return $this->mailType;
    }

    /**
     * Set company
     *
     * @param \Swan\CoreBundle\Entity\Company $company
     * @return MailTemplate
     */
    public function setCompany(\Swan\CoreBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \Swan\CoreBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }
}
