<?php

namespace Swan\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Company
 */
class Company
{
	public function __toString()
	{
		return $this->name;
	}
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $arnCode;

    /**
     * @var \Swan\CoreBundle\Entity\CompanySetting
     */
    private $companySetting;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $documentType;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $mailTemplate;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $fleetStat;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $user;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->documentType = new \Doctrine\Common\Collections\ArrayCollection();
        $this->mailTemplate = new \Doctrine\Common\Collections\ArrayCollection();
        $this->fleetStat = new \Doctrine\Common\Collections\ArrayCollection();
        $this->user = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Company
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set arnCode
     *
     * @param string $arnCode
     * @return Company
     */
    public function setArnCode($arnCode)
    {
        $this->arnCode = $arnCode;

        return $this;
    }

    /**
     * Get arnCode
     *
     * @return string 
     */
    public function getArnCode()
    {
        return $this->arnCode;
    }

    /**
     * Set companySetting
     *
     * @param \Swan\CoreBundle\Entity\CompanySetting $companySetting
     * @return Company
     */
    public function setCompanySetting(\Swan\CoreBundle\Entity\CompanySetting $companySetting = null)
    {
        $this->companySetting = $companySetting;

        return $this;
    }

    /**
     * Get companySetting
     *
     * @return \Swan\CoreBundle\Entity\CompanySetting 
     */
    public function getCompanySetting()
    {
        return $this->companySetting;
    }

    /**
     * Add documentType
     *
     * @param \Swan\CoreBundle\Entity\DocumentType $documentType
     * @return Company
     */
    public function addDocumentType(\Swan\CoreBundle\Entity\DocumentType $documentType)
    {
        $this->documentType[] = $documentType;

        return $this;
    }

    /**
     * Remove documentType
     *
     * @param \Swan\CoreBundle\Entity\DocumentType $documentType
     */
    public function removeDocumentType(\Swan\CoreBundle\Entity\DocumentType $documentType)
    {
        $this->documentType->removeElement($documentType);
    }

    /**
     * Get documentType
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDocumentType()
    {
        return $this->documentType;
    }

    /**
     * Add mailTemplate
     *
     * @param \Swan\CoreBundle\Entity\MailTemplate $mailTemplate
     * @return Company
     */
    public function addMailTemplate(\Swan\CoreBundle\Entity\MailTemplate $mailTemplate)
    {
        $this->mailTemplate[] = $mailTemplate;

        return $this;
    }

    /**
     * Remove mailTemplate
     *
     * @param \Swan\CoreBundle\Entity\MailTemplate $mailTemplate
     */
    public function removeMailTemplate(\Swan\CoreBundle\Entity\MailTemplate $mailTemplate)
    {
        $this->mailTemplate->removeElement($mailTemplate);
    }

    /**
     * Get mailTemplate
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMailTemplate()
    {
        return $this->mailTemplate;
    }

    /**
     * Add fleetStat
     *
     * @param \Swan\CoreBundle\Entity\FleetStat $fleetStat
     * @return Company
     */
    public function addFleetStat(\Swan\CoreBundle\Entity\FleetStat $fleetStat)
    {
        $this->fleetStat[] = $fleetStat;

        return $this;
    }

    /**
     * Remove fleetStat
     *
     * @param \Swan\CoreBundle\Entity\FleetStat $fleetStat
     */
    public function removeFleetStat(\Swan\CoreBundle\Entity\FleetStat $fleetStat)
    {
        $this->fleetStat->removeElement($fleetStat);
    }

    /**
     * Get fleetStat
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFleetStat()
    {
        return $this->fleetStat;
    }

    /**
     * Add user
     *
     * @param \Swan\CoreBundle\Entity\User $user
     * @return Company
     */
    public function addUser(\Swan\CoreBundle\Entity\User $user)
    {
        $this->user[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \Swan\CoreBundle\Entity\User $user
     */
    public function removeUser(\Swan\CoreBundle\Entity\User $user)
    {
        $this->user->removeElement($user);
    }

    /**
     * Get user
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUser()
    {
        return $this->user;
    }
}
