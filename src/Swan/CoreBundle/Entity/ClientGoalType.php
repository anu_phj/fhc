<?php

namespace Swan\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ClientGoalType
 */
class ClientGoalType
{
		
	public function __toString()
    {
        return $this->name;
    }
	
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ClientGoalType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $clientGoal;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->clientGoal = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add clientGoal
     *
     * @param \Swan\CoreBundle\Entity\ClientGoal $clientGoal
     * @return ClientGoalType
     */
    public function addClientGoal(\Swan\CoreBundle\Entity\ClientGoal $clientGoal)
    {
        $this->clientGoal[] = $clientGoal;

        return $this;
    }

    /**
     * Remove clientGoal
     *
     * @param \Swan\CoreBundle\Entity\ClientGoal $clientGoal
     */
    public function removeClientGoal(\Swan\CoreBundle\Entity\ClientGoal $clientGoal)
    {
        $this->clientGoal->removeElement($clientGoal);
    }

    /**
     * Get clientGoal
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getClientGoal()
    {
        return $this->clientGoal;
    }
}
