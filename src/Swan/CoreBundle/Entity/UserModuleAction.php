<?php

namespace Swan\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserModuleAction
 */
class UserModuleAction
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var boolean
     */
    private $isActive;

    /**
     * @var \Swan\CoreBundle\Entity\User
     */
    private $user;

    /**
     * @var \Swan\CoreBundle\Entity\ModuleAction
     */
    private $moduleAction;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return UserModuleAction
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set user
     *
     * @param \Swan\CoreBundle\Entity\User $user
     * @return UserModuleAction
     */
    public function setUser(\Swan\CoreBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Swan\CoreBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set moduleAction
     *
     * @param \Swan\CoreBundle\Entity\ModuleAction $moduleAction
     * @return UserModuleAction
     */
    public function setModuleAction(\Swan\CoreBundle\Entity\ModuleAction $moduleAction = null)
    {
        $this->moduleAction = $moduleAction;

        return $this;
    }

    /**
     * Get moduleAction
     *
     * @return \Swan\CoreBundle\Entity\ModuleAction 
     */
    public function getModuleAction()
    {
        return $this->moduleAction;
    }
}
