<?php

namespace Swan\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\AdvancedUserInterface as AdvancedUserInterface;


/**
 * User
 */
class User  implements AdvancedUserInterface
{
	/**
     * Checks if user acount is expired.
     *
     * @return Boolean
     */
    public function isAccountNonExpired()
    {
		return true;
    }
    
    /**
     * Checks if user acount is Not Locked.
     *
     * @return Boolean
     */
    public function isAccountNonLocked()
    {
        return true;
    }

    /**
     * Checks if user acounts Credentials are not expired.
     *
     * @return Boolean
     */
    public function isCredentialsNonExpired()
    {
        return true;
    }

    /**
     * Checks if user acount is Active.
     *
     * @return Boolean
     */
    public function isEnabled()
    {	
		if ($this->deleted === false) {
			
			return true;
		}
		
		return false;
    }

    /**
     * Returns the roles granted to the user.
     *
     * @return User
     */
    public function getRoles()
    {
        return $this->isAdmin===true ? array('ROLE_SUPER_ADMIN') : array('ROLE_CLIENT');
    }

    /**
     * Removes sensitive data from the user.
     *
     * @return void
     */
    public function eraseCredentials()
    {
        return true;
    }

    /**
     * Get salt
     *
     * @return boolean 
     */
    public function getSalt()
    {
        return NULL;
    }

    /**
     * Get userName
     *
     * @return string 
     */
    public function getUserName()
    {
        return $this->email;
    }
	
    /**
     * @var string
     */
    private $password;

    
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $role;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Get role
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRole()
    {
        return $this->role;
    }
    
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var \DateTime
     */
    private $dob;

    /**
     * @var string
     */
    private $occupation;

    /**
     * @var string
     */
    private $gender;

    /**
     * @var string
     */
    private $maritalStatus;

    /**
     * @var string
     */
    private $address;

    /**
     * @var string
     */
    private $pincode;

    /**
     * @var string
     */
    private $mobile;

    /**
     * @var string
     */
    private $email;

    /**
     * @var boolean
     */
    private $inactive;

    /**
     * @var boolean
     */
    private $deleted;

    /**
     * @var integer
     */
    private $type;

    /**
     * @var boolean
     */
    private $isAdmin;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $clientFamilyDetail;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $clientGoal;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $clientNote;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $clientNoteAddedBy;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $upload;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $clientDocumentUploadBy;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $userAssumption;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $clientAsset;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $clientGoalAsset;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $goalReport;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $userModuleAction;

    /**
     * @var \Swan\CoreBundle\Entity\Country
     */
    private $country;

    /**
     * @var \Swan\CoreBundle\Entity\State
     */
    private $state;

    /**
     * @var \Swan\CoreBundle\Entity\City
     */
    private $city;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $company;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->clientFamilyDetail = new \Doctrine\Common\Collections\ArrayCollection();
        $this->clientGoal = new \Doctrine\Common\Collections\ArrayCollection();
        $this->clientNote = new \Doctrine\Common\Collections\ArrayCollection();
        $this->clientNoteAddedBy = new \Doctrine\Common\Collections\ArrayCollection();
        $this->upload = new \Doctrine\Common\Collections\ArrayCollection();
        $this->clientDocumentUploadBy = new \Doctrine\Common\Collections\ArrayCollection();
        $this->userAssumption = new \Doctrine\Common\Collections\ArrayCollection();
        $this->clientAsset = new \Doctrine\Common\Collections\ArrayCollection();
        $this->clientGoalAsset = new \Doctrine\Common\Collections\ArrayCollection();
        $this->goalReport = new \Doctrine\Common\Collections\ArrayCollection();
        $this->userModuleAction = new \Doctrine\Common\Collections\ArrayCollection();
        $this->role = new \Doctrine\Common\Collections\ArrayCollection();
        $this->company = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set name
     *
     * @param string $name
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set dob
     *
     * @param \DateTime $dob
     * @return User
     */
    public function setDob($dob)
    {
        $this->dob = $dob;

        return $this;
    }

    /**
     * Get dob
     *
     * @return \DateTime 
     */
    public function getDob()
    {
        return $this->dob;
    }

    /**
     * Set occupation
     *
     * @param string $occupation
     * @return User
     */
    public function setOccupation($occupation)
    {
        $this->occupation = $occupation;

        return $this;
    }

    /**
     * Get occupation
     *
     * @return string 
     */
    public function getOccupation()
    {
        return $this->occupation;
    }

    /**
     * Set gender
     *
     * @param string $gender
     * @return User
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string 
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set maritalStatus
     *
     * @param string $maritalStatus
     * @return User
     */
    public function setMaritalStatus($maritalStatus)
    {
        $this->maritalStatus = $maritalStatus;

        return $this;
    }

    /**
     * Get maritalStatus
     *
     * @return string 
     */
    public function getMaritalStatus()
    {
        return $this->maritalStatus;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return User
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set pincode
     *
     * @param string $pincode
     * @return User
     */
    public function setPincode($pincode)
    {
        $this->pincode = $pincode;

        return $this;
    }

    /**
     * Get pincode
     *
     * @return string 
     */
    public function getPincode()
    {
        return $this->pincode;
    }

    /**
     * Set mobile
     *
     * @param string $mobile
     * @return User
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get mobile
     *
     * @return string 
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Set inactive
     *
     * @param boolean $inactive
     * @return User
     */
    public function setInactive($inactive)
    {
        $this->inactive = $inactive;

        return $this;
    }

    /**
     * Get inactive
     *
     * @return boolean 
     */
    public function getInactive()
    {
        return $this->inactive;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     * @return User
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean 
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return User
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set isAdmin
     *
     * @param boolean $isAdmin
     * @return User
     */
    public function setIsAdmin($isAdmin)
    {
        $this->isAdmin = $isAdmin;

        return $this;
    }

    /**
     * Get isAdmin
     *
     * @return boolean 
     */
    public function getIsAdmin()
    {
        return $this->isAdmin;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return User
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return User
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Add clientFamilyDetail
     *
     * @param \Swan\CoreBundle\Entity\ClientFamilyDetail $clientFamilyDetail
     * @return User
     */
    public function addClientFamilyDetail(\Swan\CoreBundle\Entity\ClientFamilyDetail $clientFamilyDetail)
    {
        $this->clientFamilyDetail[] = $clientFamilyDetail;

        return $this;
    }

    /**
     * Remove clientFamilyDetail
     *
     * @param \Swan\CoreBundle\Entity\ClientFamilyDetail $clientFamilyDetail
     */
    public function removeClientFamilyDetail(\Swan\CoreBundle\Entity\ClientFamilyDetail $clientFamilyDetail)
    {
        $this->clientFamilyDetail->removeElement($clientFamilyDetail);
    }

    /**
     * Get clientFamilyDetail
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getClientFamilyDetail()
    {
        return $this->clientFamilyDetail;
    }

    /**
     * Add clientGoal
     *
     * @param \Swan\CoreBundle\Entity\ClientGoal $clientGoal
     * @return User
     */
    public function addClientGoal(\Swan\CoreBundle\Entity\ClientGoal $clientGoal)
    {
        $this->clientGoal[] = $clientGoal;

        return $this;
    }

    /**
     * Remove clientGoal
     *
     * @param \Swan\CoreBundle\Entity\ClientGoal $clientGoal
     */
    public function removeClientGoal(\Swan\CoreBundle\Entity\ClientGoal $clientGoal)
    {
        $this->clientGoal->removeElement($clientGoal);
    }

    /**
     * Get clientGoal
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getClientGoal()
    {
        return $this->clientGoal;
    }

    /**
     * Add clientNote
     *
     * @param \Swan\CoreBundle\Entity\ClientNote $clientNote
     * @return User
     */
    public function addClientNote(\Swan\CoreBundle\Entity\ClientNote $clientNote)
    {
        $this->clientNote[] = $clientNote;

        return $this;
    }

    /**
     * Remove clientNote
     *
     * @param \Swan\CoreBundle\Entity\ClientNote $clientNote
     */
    public function removeClientNote(\Swan\CoreBundle\Entity\ClientNote $clientNote)
    {
        $this->clientNote->removeElement($clientNote);
    }

    /**
     * Get clientNote
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getClientNote()
    {
        return $this->clientNote;
    }

    /**
     * Add clientNoteAddedBy
     *
     * @param \Swan\CoreBundle\Entity\ClientNote $clientNoteAddedBy
     * @return User
     */
    public function addClientNoteAddedBy(\Swan\CoreBundle\Entity\ClientNote $clientNoteAddedBy)
    {
        $this->clientNoteAddedBy[] = $clientNoteAddedBy;

        return $this;
    }

    /**
     * Remove clientNoteAddedBy
     *
     * @param \Swan\CoreBundle\Entity\ClientNote $clientNoteAddedBy
     */
    public function removeClientNoteAddedBy(\Swan\CoreBundle\Entity\ClientNote $clientNoteAddedBy)
    {
        $this->clientNoteAddedBy->removeElement($clientNoteAddedBy);
    }

    /**
     * Get clientNoteAddedBy
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getClientNoteAddedBy()
    {
        return $this->clientNoteAddedBy;
    }

    /**
     * Add upload
     *
     * @param \Swan\CoreBundle\Entity\Upload $upload
     * @return User
     */
    public function addUpload(\Swan\CoreBundle\Entity\Upload $upload)
    {
        $this->upload[] = $upload;

        return $this;
    }

    /**
     * Remove upload
     *
     * @param \Swan\CoreBundle\Entity\Upload $upload
     */
    public function removeUpload(\Swan\CoreBundle\Entity\Upload $upload)
    {
        $this->upload->removeElement($upload);
    }

    /**
     * Get upload
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUpload()
    {
        return $this->upload;
    }

    /**
     * Add clientDocumentUploadBy
     *
     * @param \Swan\CoreBundle\Entity\Upload $clientDocumentUploadBy
     * @return User
     */
    public function addClientDocumentUploadBy(\Swan\CoreBundle\Entity\Upload $clientDocumentUploadBy)
    {
        $this->clientDocumentUploadBy[] = $clientDocumentUploadBy;

        return $this;
    }

    /**
     * Remove clientDocumentUploadBy
     *
     * @param \Swan\CoreBundle\Entity\Upload $clientDocumentUploadBy
     */
    public function removeClientDocumentUploadBy(\Swan\CoreBundle\Entity\Upload $clientDocumentUploadBy)
    {
        $this->clientDocumentUploadBy->removeElement($clientDocumentUploadBy);
    }

    /**
     * Get clientDocumentUploadBy
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getClientDocumentUploadBy()
    {
        return $this->clientDocumentUploadBy;
    }

    /**
     * Add userAssumption
     *
     * @param \Swan\CoreBundle\Entity\UserAssumption $userAssumption
     * @return User
     */
    public function addUserAssumption(\Swan\CoreBundle\Entity\UserAssumption $userAssumption)
    {
        $this->userAssumption[] = $userAssumption;

        return $this;
    }

    /**
     * Remove userAssumption
     *
     * @param \Swan\CoreBundle\Entity\UserAssumption $userAssumption
     */
    public function removeUserAssumption(\Swan\CoreBundle\Entity\UserAssumption $userAssumption)
    {
        $this->userAssumption->removeElement($userAssumption);
    }

    /**
     * Get userAssumption
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUserAssumption()
    {
        return $this->userAssumption;
    }

    /**
     * Add clientAsset
     *
     * @param \Swan\CoreBundle\Entity\ClientAsset $clientAsset
     * @return User
     */
    public function addClientAsset(\Swan\CoreBundle\Entity\ClientAsset $clientAsset)
    {
        $this->clientAsset[] = $clientAsset;

        return $this;
    }

    /**
     * Remove clientAsset
     *
     * @param \Swan\CoreBundle\Entity\ClientAsset $clientAsset
     */
    public function removeClientAsset(\Swan\CoreBundle\Entity\ClientAsset $clientAsset)
    {
        $this->clientAsset->removeElement($clientAsset);
    }

    /**
     * Get clientAsset
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getClientAsset()
    {
        return $this->clientAsset;
    }

    /**
     * Add clientGoalAsset
     *
     * @param \Swan\CoreBundle\Entity\ClientGoalAsset $clientGoalAsset
     * @return User
     */
    public function addClientGoalAsset(\Swan\CoreBundle\Entity\ClientGoalAsset $clientGoalAsset)
    {
        $this->clientGoalAsset[] = $clientGoalAsset;

        return $this;
    }

    /**
     * Remove clientGoalAsset
     *
     * @param \Swan\CoreBundle\Entity\ClientGoalAsset $clientGoalAsset
     */
    public function removeClientGoalAsset(\Swan\CoreBundle\Entity\ClientGoalAsset $clientGoalAsset)
    {
        $this->clientGoalAsset->removeElement($clientGoalAsset);
    }

    /**
     * Get clientGoalAsset
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getClientGoalAsset()
    {
        return $this->clientGoalAsset;
    }

    /**
     * Add goalReport
     *
     * @param \Swan\CoreBundle\Entity\GoalReport $goalReport
     * @return User
     */
    public function addGoalReport(\Swan\CoreBundle\Entity\GoalReport $goalReport)
    {
        $this->goalReport[] = $goalReport;

        return $this;
    }

    /**
     * Remove goalReport
     *
     * @param \Swan\CoreBundle\Entity\GoalReport $goalReport
     */
    public function removeGoalReport(\Swan\CoreBundle\Entity\GoalReport $goalReport)
    {
        $this->goalReport->removeElement($goalReport);
    }

    /**
     * Get goalReport
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGoalReport()
    {
        return $this->goalReport;
    }

    /**
     * Add userModuleAction
     *
     * @param \Swan\CoreBundle\Entity\UserModuleAction $userModuleAction
     * @return User
     */
    public function addUserModuleAction(\Swan\CoreBundle\Entity\UserModuleAction $userModuleAction)
    {
        $this->userModuleAction[] = $userModuleAction;

        return $this;
    }

    /**
     * Remove userModuleAction
     *
     * @param \Swan\CoreBundle\Entity\UserModuleAction $userModuleAction
     */
    public function removeUserModuleAction(\Swan\CoreBundle\Entity\UserModuleAction $userModuleAction)
    {
        $this->userModuleAction->removeElement($userModuleAction);
    }

    /**
     * Get userModuleAction
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUserModuleAction()
    {
        return $this->userModuleAction;
    }

    /**
     * Set country
     *
     * @param \Swan\CoreBundle\Entity\Country $country
     * @return User
     */
    public function setCountry(\Swan\CoreBundle\Entity\Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \Swan\CoreBundle\Entity\Country 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set state
     *
     * @param \Swan\CoreBundle\Entity\State $state
     * @return User
     */
    public function setState(\Swan\CoreBundle\Entity\State $state = null)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return \Swan\CoreBundle\Entity\State 
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set city
     *
     * @param \Swan\CoreBundle\Entity\City $city
     * @return User
     */
    public function setCity(\Swan\CoreBundle\Entity\City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \Swan\CoreBundle\Entity\City 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Add role
     *
     * @param \Swan\CoreBundle\Entity\Role $role
     * @return User
     */
    public function addRole(\Swan\CoreBundle\Entity\Role $role)
    {
        $this->role[] = $role;

        return $this;
    }

    /**
     * Remove role
     *
     * @param \Swan\CoreBundle\Entity\Role $role
     */
    public function removeRole(\Swan\CoreBundle\Entity\Role $role)
    {
        $this->role->removeElement($role);
    }

    /**
     * Add company
     *
     * @param \Swan\CoreBundle\Entity\Company $company
     * @return User
     */
    public function addCompany(\Swan\CoreBundle\Entity\Company $company)
    {
        $this->company[] = $company;

        return $this;
    }

    /**
     * Remove company
     *
     * @param \Swan\CoreBundle\Entity\Company $company
     */
    public function removeCompany(\Swan\CoreBundle\Entity\Company $company)
    {
        $this->company->removeElement($company);
    }

    /**
     * Get company
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCompany()
    {
        return $this->company;
    }
}
