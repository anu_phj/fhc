<?php

namespace Swan\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Module
 */
class Module
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $slug;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $moduleAction;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->moduleAction = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Module
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Module
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Add moduleAction
     *
     * @param \Swan\CoreBundle\Entity\ModuleAction $moduleAction
     * @return Module
     */
    public function addModuleAction(\Swan\CoreBundle\Entity\ModuleAction $moduleAction)
    {
        $this->moduleAction[] = $moduleAction;

        return $this;
    }

    /**
     * Remove moduleAction
     *
     * @param \Swan\CoreBundle\Entity\ModuleAction $moduleAction
     */
    public function removeModuleAction(\Swan\CoreBundle\Entity\ModuleAction $moduleAction)
    {
        $this->moduleAction->removeElement($moduleAction);
    }

    /**
     * Get moduleAction
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getModuleAction()
    {
        return $this->moduleAction;
    }
}
