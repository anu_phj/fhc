<?php

namespace Swan\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserAssumption
 */
class UserAssumption
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $answer;

    /**
     * @var \Swan\CoreBundle\Entity\User
     */
    private $user;

    /**
     * @var \Swan\CoreBundle\Entity\Assumption
     */
    private $assumption;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set answer
     *
     * @param string $answer
     * @return UserAssumption
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;

        return $this;
    }

    /**
     * Get answer
     *
     * @return string 
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Set user
     *
     * @param \Swan\CoreBundle\Entity\User $user
     * @return UserAssumption
     */
    public function setUser(\Swan\CoreBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Swan\CoreBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set assumption
     *
     * @param \Swan\CoreBundle\Entity\Assumption $assumption
     * @return UserAssumption
     */
    public function setAssumption(\Swan\CoreBundle\Entity\Assumption $assumption = null)
    {
        $this->assumption = $assumption;

        return $this;
    }

    /**
     * Get assumption
     *
     * @return \Swan\CoreBundle\Entity\Assumption 
     */
    public function getAssumption()
    {
        return $this->assumption;
    }
}
