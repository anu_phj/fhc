<?php

namespace Swan\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RolePermission
 */
class RolePermission
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Swan\CoreBundle\Entity\Role
     */
    private $role;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set role
     *
     * @param \Swan\CoreBundle\Entity\Role $role
     * @return RolePermission
     */
    public function setRole(\Swan\CoreBundle\Entity\Role $role = null)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return \Swan\CoreBundle\Entity\Role 
     */
    public function getRole()
    {
        return $this->role;
    }
    /**
     * @var \Swan\CoreBundle\Entity\ModuleAction
     */
    private $moduleAction;


    /**
     * Set moduleAction
     *
     * @param \Swan\CoreBundle\Entity\ModuleAction $moduleAction
     * @return RolePermission
     */
    public function setModuleAction(\Swan\CoreBundle\Entity\ModuleAction $moduleAction = null)
    {
        $this->moduleAction = $moduleAction;

        return $this;
    }

    /**
     * Get moduleAction
     *
     * @return \Swan\CoreBundle\Entity\ModuleAction 
     */
    public function getModuleAction()
    {
        return $this->moduleAction;
    }
}
