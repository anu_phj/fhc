<?php

namespace Swan\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FleetStat
 */
class FleetStat
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $totalActiveVehicle;

    /**
     * @var integer
     */
    private $totalActiveVehicleLease;

    /**
     * @var integer
     */
    private $totalActiveVehicleLeaseLimited;

    /**
     * @var integer
     */
    private $totalActiveVehicleRent;

    /**
     * @var integer
     */
    private $totalActiveVehicleFuel;

    /**
     * @var integer
     */
    private $totalActiveVehicleOwnedByCompany;

    /**
     * @var \DateTime
     */
    private $dateGenerated;

    /**
     * @var \Swan\CoreBundle\Entity\Company
     */
    private $company;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set totalActiveVehicle
     *
     * @param integer $totalActiveVehicle
     * @return FleetStat
     */
    public function setTotalActiveVehicle($totalActiveVehicle)
    {
        $this->totalActiveVehicle = $totalActiveVehicle;

        return $this;
    }

    /**
     * Get totalActiveVehicle
     *
     * @return integer 
     */
    public function getTotalActiveVehicle()
    {
        return $this->totalActiveVehicle;
    }

    /**
     * Set totalActiveVehicleLease
     *
     * @param integer $totalActiveVehicleLease
     * @return FleetStat
     */
    public function setTotalActiveVehicleLease($totalActiveVehicleLease)
    {
        $this->totalActiveVehicleLease = $totalActiveVehicleLease;

        return $this;
    }

    /**
     * Get totalActiveVehicleLease
     *
     * @return integer 
     */
    public function getTotalActiveVehicleLease()
    {
        return $this->totalActiveVehicleLease;
    }

    /**
     * Set totalActiveVehicleLeaseLimited
     *
     * @param integer $totalActiveVehicleLeaseLimited
     * @return FleetStat
     */
    public function setTotalActiveVehicleLeaseLimited($totalActiveVehicleLeaseLimited)
    {
        $this->totalActiveVehicleLeaseLimited = $totalActiveVehicleLeaseLimited;

        return $this;
    }

    /**
     * Get totalActiveVehicleLeaseLimited
     *
     * @return integer 
     */
    public function getTotalActiveVehicleLeaseLimited()
    {
        return $this->totalActiveVehicleLeaseLimited;
    }

    /**
     * Set totalActiveVehicleRent
     *
     * @param integer $totalActiveVehicleRent
     * @return FleetStat
     */
    public function setTotalActiveVehicleRent($totalActiveVehicleRent)
    {
        $this->totalActiveVehicleRent = $totalActiveVehicleRent;

        return $this;
    }

    /**
     * Get totalActiveVehicleRent
     *
     * @return integer 
     */
    public function getTotalActiveVehicleRent()
    {
        return $this->totalActiveVehicleRent;
    }

    /**
     * Set totalActiveVehicleFuel
     *
     * @param integer $totalActiveVehicleFuel
     * @return FleetStat
     */
    public function setTotalActiveVehicleFuel($totalActiveVehicleFuel)
    {
        $this->totalActiveVehicleFuel = $totalActiveVehicleFuel;

        return $this;
    }

    /**
     * Get totalActiveVehicleFuel
     *
     * @return integer 
     */
    public function getTotalActiveVehicleFuel()
    {
        return $this->totalActiveVehicleFuel;
    }

    /**
     * Set totalActiveVehicleOwnedByCompany
     *
     * @param integer $totalActiveVehicleOwnedByCompany
     * @return FleetStat
     */
    public function setTotalActiveVehicleOwnedByCompany($totalActiveVehicleOwnedByCompany)
    {
        $this->totalActiveVehicleOwnedByCompany = $totalActiveVehicleOwnedByCompany;

        return $this;
    }

    /**
     * Get totalActiveVehicleOwnedByCompany
     *
     * @return integer 
     */
    public function getTotalActiveVehicleOwnedByCompany()
    {
        return $this->totalActiveVehicleOwnedByCompany;
    }

    /**
     * Set dateGenerated
     *
     * @param \DateTime $dateGenerated
     * @return FleetStat
     */
    public function setDateGenerated($dateGenerated)
    {
        $this->dateGenerated = $dateGenerated;

        return $this;
    }

    /**
     * Get dateGenerated
     *
     * @return \DateTime 
     */
    public function getDateGenerated()
    {
        return $this->dateGenerated;
    }

    /**
     * Set company
     *
     * @param \Swan\CoreBundle\Entity\Company $company
     * @return FleetStat
     */
    public function setCompany(\Swan\CoreBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \Swan\CoreBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }
}
