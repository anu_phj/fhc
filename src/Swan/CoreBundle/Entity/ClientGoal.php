<?php

namespace Swan\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * ClientGoal
 */
class ClientGoal
{	

	/**
     * Validation required fields.
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('clientGoalType', new Assert\NotBlank(array('message' => 'Type Can not be blank')));
        
        $metadata->addPropertyConstraint('name', new Assert\NotBlank(array('message' => 'Name Can not be blank')));
        $metadata->addPropertyConstraint('value', new Assert\NotBlank(array('message' => 'Value Can not be blank')));
        $metadata->addPropertyConstraint('value', new Assert\Type(array(
			'type'    => 'numeric',
			'message' => 'Value should be a valid number',
        )));
        
    }

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \DateTime
     */
    private $createdOn;

    /**
     * @var float
     */
    private $value;

    /**
     * @var integer
     */
    private $year;

    /**
     * @var integer
     */
    private $month;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $clientGoalAsset;

    /**
     * @var \Swan\CoreBundle\Entity\User
     */
    private $user;

    /**
     * @var \Swan\CoreBundle\Entity\ClientGoalType
     */
    private $clientGoalType;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->clientGoalAsset = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ClientGoal
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createdOn
     *
     * @param \DateTime $createdOn
     * @return ClientGoal
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;

        return $this;
    }

    /**
     * Get createdOn
     *
     * @return \DateTime 
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * Set value
     *
     * @param float $value
     * @return ClientGoal
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return float 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set year
     *
     * @param integer $year
     * @return ClientGoal
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return integer 
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set month
     *
     * @param integer $month
     * @return ClientGoal
     */
    public function setMonth($month)
    {
        $this->month = $month;

        return $this;
    }

    /**
     * Get month
     *
     * @return integer 
     */
    public function getMonth()
    {
        return $this->month;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return ClientGoal
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Add clientGoalAsset
     *
     * @param \Swan\CoreBundle\Entity\ClientGoalAsset $clientGoalAsset
     * @return ClientGoal
     */
    public function addClientGoalAsset(\Swan\CoreBundle\Entity\ClientGoalAsset $clientGoalAsset)
    {
        $this->clientGoalAsset[] = $clientGoalAsset;

        return $this;
    }

    /**
     * Remove clientGoalAsset
     *
     * @param \Swan\CoreBundle\Entity\ClientGoalAsset $clientGoalAsset
     */
    public function removeClientGoalAsset(\Swan\CoreBundle\Entity\ClientGoalAsset $clientGoalAsset)
    {
        $this->clientGoalAsset->removeElement($clientGoalAsset);
    }

    /**
     * Get clientGoalAsset
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getClientGoalAsset()
    {
        return $this->clientGoalAsset;
    }

    /**
     * Set user
     *
     * @param \Swan\CoreBundle\Entity\User $user
     * @return ClientGoal
     */
    public function setUser(\Swan\CoreBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Swan\CoreBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set clientGoalType
     *
     * @param \Swan\CoreBundle\Entity\ClientGoalType $clientGoalType
     * @return ClientGoal
     */
    public function setClientGoalType(\Swan\CoreBundle\Entity\ClientGoalType $clientGoalType = null)
    {
        $this->clientGoalType = $clientGoalType;

        return $this;
    }

    /**
     * Get clientGoalType
     *
     * @return \Swan\CoreBundle\Entity\ClientGoalType 
     */
    public function getClientGoalType()
    {
        return $this->clientGoalType;
    }
}
