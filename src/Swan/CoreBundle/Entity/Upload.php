<?php

namespace Swan\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Upload
 */
class Upload
{
    /**
     * @Assert\File(maxSize="6000000")
     */
    private $file;

    public $temp;
    private $path;
    public $newFileName;
    public $customPath;
    
    /**
     * clean file name
     *
     * @param $string
     * @return filename
     */
    
    public function clean($string) 
    {
		$ext =  explode(".", $string);
		$ext = count($ext)>1 ? end($ext) : '';
		
		$string = str_replace(' ', '', str_replace('.'.$ext, '', $string)); // Replaces all spaces with hyphens.
		$string = preg_replace('/[^A-Za-z0-9\-_]/', '', $string); // Removes special chars.
		
		$ext = $ext!='' ? ".".$ext : '';
		
		return $string.$ext;
	}

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->getFile()) {
			
			$this->customPath = $this->customPath!='' ?  $this->customPath : 'driver';
			
            $this->path = $this->getFile()->guessClientExtension();
			$this->setOriginalFileName($this->clean($this->getFile()->getClientOriginalName()));
            $this->setFileType($this->getFile()->getClientMimeType());
            $this->setFileSize($this->getFile()->getClientSize());
            $this->setDateUploaded(new \DateTime);
        }
    }
    
    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function wbsUpload()
    {
        if (null === $this->getFile()) {
			return;
        }

        // check if we have an old image
        if (isset($this->temp)) {
            // delete the old image
            unlink($this->temp);
            // clear the temp image path
            $this->temp = null;
        }
		
		$this->newFileName = $this->id.'.'.$this->path;
		
		$this->getFile()->move(
			$this->getUploadRootDir(),
			$this->newFileName
		);

        $this->setFile(null);
    }
    
    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
        // check if we have an old image path
        if (is_file($this->getAbsolutePath())) {
            // store the old name to delete after the update
            $this->temp = $this->getAbsolutePath();
        } else {
            $this->path = 'initial';
        }
    }
    
    /**
     * @ORM\PreRemove()
     */
    public function storeFilenameForRemove()
    {
        $this->temp = $this->getAbsolutePath();
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if (isset($this->temp) && file_exists ($this->temp)) {
        
			if (@unlink($this->temp) === false) {
			
				throw new \RuntimeException('File could not be deleted.');
			}        
        }
    }

    public function getAbsolutePath()
    {
        return null === $this->path
            ? null
            : $this->getUploadRootDir().'/'.$this->id.'.'.$this->path;
    }
    
    protected function getUploadRootDir()
    {
        return $this->getUploadDir();
    }
    
    protected function getUploadDir()
    {
        return $this->customPath && $this->customPath!='' ? $this->customPath : 'uploads';
    }

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $originalFileName;

    /**
     * @var string
     */
    private $saveName;

    /**
     * @var string
     */
    private $fileSize;

    /**
     * @var string
     */
    private $fileType;

    /**
     * @var \DateTime
     */
    private $dateUploaded;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set originalFileName
     *
     * @param string $originalFileName
     * @return Upload
     */
    public function setOriginalFileName($originalFileName)
    {
        $this->originalFileName = $originalFileName;

        return $this;
    }

    /**
     * Get originalFileName
     *
     * @return string 
     */
    public function getOriginalFileName()
    {
        return $this->originalFileName;
    }

    /**
     * Set saveName
     *
     * @param string $saveName
     * @return Upload
     */
    public function setSaveName($saveName)
    {
        $this->saveName = $saveName;

        return $this;
    }

    /**
     * Get saveName
     *
     * @return string 
     */
    public function getSaveName()
    {
        return $this->saveName;
    }

    /**
     * Set fileSize
     *
     * @param string $fileSize
     * @return Upload
     */
    public function setFileSize($fileSize)
    {
        $this->fileSize = $fileSize;

        return $this;
    }

    /**
     * Get fileSize
     *
     * @return string 
     */
    public function getFileSize()
    {
        return $this->fileSize;
    }

    /**
     * Set fileType
     *
     * @param string $fileType
     * @return Upload
     */
    public function setFileType($fileType)
    {
        $this->fileType = $fileType;

        return $this;
    }

    /**
     * Get fileType
     *
     * @return string 
     */
    public function getFileType()
    {
        return $this->fileType;
    }

    /**
     * Set dateUploaded
     *
     * @param \DateTime $dateUploaded
     * @return Upload
     */
    public function setDateUploaded($dateUploaded)
    {
        $this->dateUploaded = $dateUploaded;

        return $this;
    }

    /**
     * Get dateUploaded
     *
     * @return \DateTime 
     */
    public function getDateUploaded()
    {
        return $this->dateUploaded;
    }
    /**
     * @var \Swan\CoreBundle\Entity\DocumentType
     */
    private $documentType;


    /**
     * Set documentType
     *
     * @param \Swan\CoreBundle\Entity\DocumentType $documentType
     * @return Upload
     */
    public function setDocumentType(\Swan\CoreBundle\Entity\DocumentType $documentType = null)
    {
        $this->documentType = $documentType;

        return $this;
    }

    /**
     * Get documentType
     *
     * @return \Swan\CoreBundle\Entity\DocumentType
     */
    public function getDocumentType()
    {
        return $this->documentType;
    }
    /**
     * @var \Swan\CoreBundle\Entity\User
     */
    private $user;

    /**
     * @var \Swan\CoreBundle\Entity\User
     */
    private $userUpload;


    /**
     * Set user
     *
     * @param \Swan\CoreBundle\Entity\User $user
     * @return Upload
     */
    public function setUser(\Swan\CoreBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Swan\CoreBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set userUpload
     *
     * @param \Swan\CoreBundle\Entity\User $userUpload
     * @return Upload
     */
    public function setUserUpload(\Swan\CoreBundle\Entity\User $userUpload = null)
    {
        $this->userUpload = $userUpload;

        return $this;
    }

    /**
     * Get userUpload
     *
     * @return \Swan\CoreBundle\Entity\User 
     */
    public function getUserUpload()
    {
        return $this->userUpload;
    }
}
