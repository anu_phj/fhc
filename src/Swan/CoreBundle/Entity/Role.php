<?php

namespace Swan\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Role
 */
class Role
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var boolean
     */
    private $status;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Role
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Role
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $user;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->user = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add user
     *
     * @param \Swan\CoreBundle\Entity\User $user
     * @return Role
     */
    public function addUser(\Swan\CoreBundle\Entity\User $user)
    {
        $this->user[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \Swan\CoreBundle\Entity\User $user
     */
    public function removeUser(\Swan\CoreBundle\Entity\User $user)
    {
        $this->user->removeElement($user);
    }

    /**
     * Get user
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUser()
    {
        return $this->user;
    }
    /**
     * @var string
     */
    private $slug;


    /**
     * Set slug
     *
     * @param string $slug
     * @return Role
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $rolePermission;


    /**
     * Add rolePermission
     *
     * @param \Swan\CoreBundle\Entity\RolePermission $rolePermission
     * @return Role
     */
    public function addRolePermission(\Swan\CoreBundle\Entity\RolePermission $rolePermission)
    {
        $this->rolePermission[] = $rolePermission;

        return $this;
    }

    /**
     * Remove rolePermission
     *
     * @param \Swan\CoreBundle\Entity\RolePermission $rolePermission
     */
    public function removeRolePermission(\Swan\CoreBundle\Entity\RolePermission $rolePermission)
    {
        $this->rolePermission->removeElement($rolePermission);
    }

    /**
     * Get rolePermission
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRolePermission()
    {
        return $this->rolePermission;
    }
}
