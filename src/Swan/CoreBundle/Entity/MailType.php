<?php

namespace Swan\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MailType
 */
class MailType
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return MailType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * @var string
     */
    private $slug;


    /**
     * Set slug
     *
     * @param string $slug
     * @return MailType
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $mailTemplate;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->mailTemplate = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add mailTemplate
     *
     * @param \Swan\CoreBundle\Entity\MailTemplate $mailTemplate
     * @return MailType
     */
    public function addMailTemplate(\Swan\CoreBundle\Entity\MailTemplate $mailTemplate)
    {
        $this->mailTemplate[] = $mailTemplate;

        return $this;
    }

    /**
     * Remove mailTemplate
     *
     * @param \Swan\CoreBundle\Entity\MailTemplate $mailTemplate
     */
    public function removeMailTemplate(\Swan\CoreBundle\Entity\MailTemplate $mailTemplate)
    {
        $this->mailTemplate->removeElement($mailTemplate);
    }

    /**
     * Get mailTemplate
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMailTemplate()
    {
        return $this->mailTemplate;
    }
}
