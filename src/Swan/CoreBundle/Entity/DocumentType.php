<?php

namespace Swan\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DocumentType
 */
class DocumentType
{
	public function __toString()
	{
		return $this->name;
	}
    
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return DocumentType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * @var \Swan\CoreBundle\Entity\Company
     */
    private $company;


    /**
     * Set company
     *
     * @param \Swan\CoreBundle\Entity\Company $company
     * @return DocumentType
     */
    public function setCompany(\Swan\CoreBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \Swan\CoreBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $wbsUpload;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->wbsUpload = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add wbsUpload
     *
     * @param \Swan\CoreBundle\Entity\Upload $wbsUpload
     * @return DocumentType
     */
    public function addWbsUpload(\Swan\CoreBundle\Entity\Upload $wbsUpload)
    {
        $this->wbsUpload[] = $wbsUpload;

        return $this;
    }

    /**
     * Remove wbsUpload
     *
     * @param \Swan\CoreBundle\Entity\Upload $wbsUpload
     */
    public function removeWbsUpload(\Swan\CoreBundle\Entity\Upload $wbsUpload)
    {
        $this->wbsUpload->removeElement($wbsUpload);
    }

    /**
     * Get wbsUpload
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getWbsUpload()
    {
        return $this->wbsUpload;
    }
}
