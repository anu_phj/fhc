<?php

namespace Swan\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EmailLog
 */
class EmailLog
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $companyId;

    /**
     * @var integer
     */
    private $vehicleId;

    /**
     * @var integer
     */
    private $driverId;

    /**
     * @var string
     */
    private $subject;

    /**
     * @var string
     */
    private $body;

    /**
     * @var \DateTime
     */
    private $sendDate;

    /**
     * @var integer
     */
    private $mailTypeId;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set companyId
     *
     * @param integer $companyId
     * @return EmailLog
     */
    public function setCompanyId($companyId)
    {
        $this->companyId = $companyId;

        return $this;
    }

    /**
     * Get companyId
     *
     * @return integer 
     */
    public function getCompanyId()
    {
        return $this->companyId;
    }

    /**
     * Set vehicleId
     *
     * @param integer $vehicleId
     * @return EmailLog
     */
    public function setVehicleId($vehicleId)
    {
        $this->vehicleId = $vehicleId;

        return $this;
    }

    /**
     * Get vehicleId
     *
     * @return integer 
     */
    public function getVehicleId()
    {
        return $this->vehicleId;
    }

    /**
     * Set driverId
     *
     * @param integer $driverId
     * @return EmailLog
     */
    public function setDriverId($driverId)
    {
        $this->driverId = $driverId;

        return $this;
    }

    /**
     * Get driverId
     *
     * @return integer 
     */
    public function getDriverId()
    {
        return $this->driverId;
    }

    /**
     * Set subject
     *
     * @param string $subject
     * @return EmailLog
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string 
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set body
     *
     * @param string $body
     * @return EmailLog
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string 
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set sendDate
     *
     * @param \DateTime $sendDate
     * @return EmailLog
     */
    public function setSendDate($sendDate)
    {
        $this->sendDate = $sendDate;

        return $this;
    }

    /**
     * Get sendDate
     *
     * @return \DateTime 
     */
    public function getSendDate()
    {
        return $this->sendDate;
    }

    /**
     * Set mailTypeId
     *
     * @param integer $mailTypeId
     * @return EmailLog
     */
    public function setMailTypeId($mailTypeId)
    {
        $this->mailTypeId = $mailTypeId;

        return $this;
    }

    /**
     * Get mailTypeId
     *
     * @return integer 
     */
    public function getMailTypeId()
    {
        return $this->mailTypeId;
    }
    /**
     * @var string
     */
    private $mailTo;

    /**
     * @var string
     */
    private $mailCc;

    /**
     * @var string
     */
    private $mailBcc;


    /**
     * Set mailTo
     *
     * @param string $mailTo
     * @return EmailLog
     */
    public function setMailTo($mailTo)
    {
        $this->mailTo = $mailTo;

        return $this;
    }

    /**
     * Get mailTo
     *
     * @return string 
     */
    public function getMailTo()
    {
        return $this->mailTo;
    }

    /**
     * Set mailCc
     *
     * @param string $mailCc
     * @return EmailLog
     */
    public function setMailCc($mailCc)
    {
        $this->mailCc = $mailCc;

        return $this;
    }

    /**
     * Get mailCc
     *
     * @return string 
     */
    public function getMailCc()
    {
        return $this->mailCc;
    }

    /**
     * Set mailBcc
     *
     * @param string $mailBcc
     * @return EmailLog
     */
    public function setMailBcc($mailBcc)
    {
        $this->mailBcc = $mailBcc;

        return $this;
    }

    /**
     * Get mailBcc
     *
     * @return string 
     */
    public function getMailBcc()
    {
        return $this->mailBcc;
    }
}
