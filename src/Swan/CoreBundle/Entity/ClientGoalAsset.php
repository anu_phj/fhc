<?php

namespace Swan\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ClientGoalAsset
 */
class ClientGoalAsset
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var float
     */
    private $value;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * @var \Swan\CoreBundle\Entity\ClientGoal
     */
    private $clientGoal;

    /**
     * @var \Swan\CoreBundle\Entity\Asset
     */
    private $asset;

    /**
     * @var \Swan\CoreBundle\Entity\User
     */
    private $user;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param float $value
     * @return ClientGoalAsset
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return float 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return ClientGoalAsset
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set clientGoal
     *
     * @param \Swan\CoreBundle\Entity\ClientGoal $clientGoal
     * @return ClientGoalAsset
     */
    public function setClientGoal(\Swan\CoreBundle\Entity\ClientGoal $clientGoal = null)
    {
        $this->clientGoal = $clientGoal;

        return $this;
    }

    /**
     * Get clientGoal
     *
     * @return \Swan\CoreBundle\Entity\ClientGoal 
     */
    public function getClientGoal()
    {
        return $this->clientGoal;
    }

    /**
     * Set asset
     *
     * @param \Swan\CoreBundle\Entity\Asset $asset
     * @return ClientGoalAsset
     */
    public function setAsset(\Swan\CoreBundle\Entity\Asset $asset = null)
    {
        $this->asset = $asset;

        return $this;
    }

    /**
     * Get asset
     *
     * @return \Swan\CoreBundle\Entity\Asset 
     */
    public function getAsset()
    {
        return $this->asset;
    }

    /**
     * Set user
     *
     * @param \Swan\CoreBundle\Entity\User $user
     * @return ClientGoalAsset
     */
    public function setUser(\Swan\CoreBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Swan\CoreBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
