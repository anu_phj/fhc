<?php

namespace Swan\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ClientAsset
 */
class ClientAsset
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var float
     */
    private $value;

    /**
     * @var \DateTime
     */
    private $fromDate;

    /**
     * @var \DateTime
     */
    private $updated;

    /**
     * @var \Swan\CoreBundle\Entity\Asset
     */
    private $asset;

    /**
     * @var \Swan\CoreBundle\Entity\User
     */
    private $user;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param float $value
     * @return ClientAsset
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return float 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set fromDate
     *
     * @param \DateTime $fromDate
     * @return ClientAsset
     */
    public function setFromDate($fromDate)
    {
        $this->fromDate = $fromDate;

        return $this;
    }

    /**
     * Get fromDate
     *
     * @return \DateTime 
     */
    public function getFromDate()
    {
        return $this->fromDate;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return ClientAsset
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set asset
     *
     * @param \Swan\CoreBundle\Entity\Asset $asset
     * @return ClientAsset
     */
    public function setAsset(\Swan\CoreBundle\Entity\Asset $asset = null)
    {
        $this->asset = $asset;

        return $this;
    }

    /**
     * Get asset
     *
     * @return \Swan\CoreBundle\Entity\Asset 
     */
    public function getAsset()
    {
        return $this->asset;
    }

    /**
     * Set user
     *
     * @param \Swan\CoreBundle\Entity\User $user
     * @return ClientAsset
     */
    public function setUser(\Swan\CoreBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Swan\CoreBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
