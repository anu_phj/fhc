<?php

namespace Swan\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Assumption
 */
class Assumption
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $question;

    /**
     * @var string
     */
    private $answer;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $userAssumption;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $asset;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->userAssumption = new \Doctrine\Common\Collections\ArrayCollection();
        $this->asset = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set question
     *
     * @param string $question
     * @return Assumption
     */
    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return string 
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set answer
     *
     * @param string $answer
     * @return Assumption
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;

        return $this;
    }

    /**
     * Get answer
     *
     * @return string 
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Add userAssumption
     *
     * @param \Swan\CoreBundle\Entity\UserAssumption $userAssumption
     * @return Assumption
     */
    public function addUserAssumption(\Swan\CoreBundle\Entity\UserAssumption $userAssumption)
    {
        $this->userAssumption[] = $userAssumption;

        return $this;
    }

    /**
     * Remove userAssumption
     *
     * @param \Swan\CoreBundle\Entity\UserAssumption $userAssumption
     */
    public function removeUserAssumption(\Swan\CoreBundle\Entity\UserAssumption $userAssumption)
    {
        $this->userAssumption->removeElement($userAssumption);
    }

    /**
     * Get userAssumption
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUserAssumption()
    {
        return $this->userAssumption;
    }

    /**
     * Add asset
     *
     * @param \Swan\CoreBundle\Entity\Asset $asset
     * @return Assumption
     */
    public function addAsset(\Swan\CoreBundle\Entity\Asset $asset)
    {
        $this->asset[] = $asset;

        return $this;
    }

    /**
     * Remove asset
     *
     * @param \Swan\CoreBundle\Entity\Asset $asset
     */
    public function removeAsset(\Swan\CoreBundle\Entity\Asset $asset)
    {
        $this->asset->removeElement($asset);
    }

    /**
     * Get asset
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAsset()
    {
        return $this->asset;
    }
}
