<?php

namespace Swan\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ClientNote
 */
class ClientNote
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $body;

    /**
     * @var \DateTime
     */
    private $dateCreated;

    /**
     * @var \Swan\CoreBundle\Entity\User
     */
    private $user;

    /**
     * @var \Swan\CoreBundle\Entity\User
     */
    private $userAddNote;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set body
     *
     * @param string $body
     * @return ClientNote
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string 
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     * @return ClientNote
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime 
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set user
     *
     * @param \Swan\CoreBundle\Entity\User $user
     * @return ClientNote
     */
    public function setUser(\Swan\CoreBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Swan\CoreBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set userAddNote
     *
     * @param \Swan\CoreBundle\Entity\User $userAddNote
     * @return ClientNote
     */
    public function setUserAddNote(\Swan\CoreBundle\Entity\User $userAddNote = null)
    {
        $this->userAddNote = $userAddNote;

        return $this;
    }

    /**
     * Get userAddNote
     *
     * @return \Swan\CoreBundle\Entity\User 
     */
    public function getUserAddNote()
    {
        return $this->userAddNote;
    }
}
