<?php

namespace Swan\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GoalReport
 */
class GoalReport
{
    
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $fileName;

    /**
     * @var string
     */
    private $customFileName;

    /**
     * @var \DateTime
     */
    private $dateGenerated;

    /**
     * @var \Swan\CoreBundle\Entity\User
     */
    private $user;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fileName
     *
     * @param string $fileName
     * @return GoalReport
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * Get fileName
     *
     * @return string 
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Set customFileName
     *
     * @param string $customFileName
     * @return GoalReport
     */
    public function setCustomFileName($customFileName)
    {
        $this->customFileName = $customFileName;

        return $this;
    }

    /**
     * Get customFileName
     *
     * @return string 
     */
    public function getCustomFileName()
    {
        return $this->customFileName;
    }

    /**
     * Set dateGenerated
     *
     * @param \DateTime $dateGenerated
     * @return GoalReport
     */
    public function setDateGenerated($dateGenerated)
    {
        $this->dateGenerated = $dateGenerated;

        return $this;
    }

    /**
     * Get dateGenerated
     *
     * @return \DateTime 
     */
    public function getDateGenerated()
    {
        return $this->dateGenerated;
    }

    /**
     * Set user
     *
     * @param \Swan\CoreBundle\Entity\User $user
     * @return GoalReport
     */
    public function setUser(\Swan\CoreBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Swan\CoreBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
