<?php

namespace Swan\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Asset
 */
class Asset
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var boolean
     */
    private $interestTypeMonthly;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $clientAsset;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $clientGoalAsset;

    /**
     * @var \Swan\CoreBundle\Entity\Assumption
     */
    private $assumption;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->clientAsset = new \Doctrine\Common\Collections\ArrayCollection();
        $this->clientGoalAsset = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Asset
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set interestTypeMonthly
     *
     * @param boolean $interestTypeMonthly
     * @return Asset
     */
    public function setInterestTypeMonthly($interestTypeMonthly)
    {
        $this->interestTypeMonthly = $interestTypeMonthly;

        return $this;
    }

    /**
     * Get interestTypeMonthly
     *
     * @return boolean 
     */
    public function getInterestTypeMonthly()
    {
        return $this->interestTypeMonthly;
    }

    /**
     * Add clientAsset
     *
     * @param \Swan\CoreBundle\Entity\ClientAsset $clientAsset
     * @return Asset
     */
    public function addClientAsset(\Swan\CoreBundle\Entity\ClientAsset $clientAsset)
    {
        $this->clientAsset[] = $clientAsset;

        return $this;
    }

    /**
     * Remove clientAsset
     *
     * @param \Swan\CoreBundle\Entity\ClientAsset $clientAsset
     */
    public function removeClientAsset(\Swan\CoreBundle\Entity\ClientAsset $clientAsset)
    {
        $this->clientAsset->removeElement($clientAsset);
    }

    /**
     * Get clientAsset
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getClientAsset()
    {
        return $this->clientAsset;
    }

    /**
     * Add clientGoalAsset
     *
     * @param \Swan\CoreBundle\Entity\ClientGoalAsset $clientGoalAsset
     * @return Asset
     */
    public function addClientGoalAsset(\Swan\CoreBundle\Entity\ClientGoalAsset $clientGoalAsset)
    {
        $this->clientGoalAsset[] = $clientGoalAsset;

        return $this;
    }

    /**
     * Remove clientGoalAsset
     *
     * @param \Swan\CoreBundle\Entity\ClientGoalAsset $clientGoalAsset
     */
    public function removeClientGoalAsset(\Swan\CoreBundle\Entity\ClientGoalAsset $clientGoalAsset)
    {
        $this->clientGoalAsset->removeElement($clientGoalAsset);
    }

    /**
     * Get clientGoalAsset
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getClientGoalAsset()
    {
        return $this->clientGoalAsset;
    }

    /**
     * Set assumption
     *
     * @param \Swan\CoreBundle\Entity\Assumption $assumption
     * @return Asset
     */
    public function setAssumption(\Swan\CoreBundle\Entity\Assumption $assumption = null)
    {
        $this->assumption = $assumption;

        return $this;
    }

    /**
     * Get assumption
     *
     * @return \Swan\CoreBundle\Entity\Assumption 
     */
    public function getAssumption()
    {
        return $this->assumption;
    }
}
