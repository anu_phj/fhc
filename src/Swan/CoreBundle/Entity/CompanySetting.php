<?php

namespace Swan\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * CompanySetting
 */
class CompanySetting
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $companyId;

    /**
     * @var boolean
     */
    private $allocationMethod;

    /**
     * @var \Swan\CoreBundle\Entity\Company
     */
    private $company;
    
    private $file;

    public $temp;
    private $path;
    public $newFileName;
    
    
    /**
     * clean file name
     *
     * @param $string
     * @return filename
     */
    
    public function clean($string) 
    {
		$ext =  explode(".", $string);
		$ext = count($ext)>1 ? end($ext) : '';
		
		$string = str_replace(' ', '', str_replace('.'.$ext, '', $string)); // Replaces all spaces with hyphens.
		$string = preg_replace('/[^A-Za-z0-9\-_]/', '', $string); // Removes special chars.
		
		$ext = $ext!='' ? ".".$ext : '';
		
		return $string.$ext;
	}

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->getFile()) {
			
            $this->path = $this->getFile()->guessClientExtension();
			$this->setLogo($this->clean($this->getFile()->getClientOriginalName()));
            //$this->setType($this->getFile()->getClientMimeType());           
        }
    }
    
    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->getFile()) {
			return;
        }

        // check if we have an old image
        if (isset($this->temp)) {
            // delete the old image
            unlink($this->temp);
            // clear the temp image path
            $this->temp = null;
        }
		
		$this->newFileName = $this->id.'.'.$this->path;
		
		$this->getFile()->move(
			$this->getUploadRootDir(),
			$this->newFileName
		);

        $this->setFile(null);
    }
    
    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
        // check if we have an old image path
        if (is_file($this->getAbsolutePath())) {
            // store the old name to delete after the update
            $this->temp = $this->getAbsolutePath();
        } else {
            $this->path = 'initial';
        }
    }
    
    /**
     * @ORM\PreRemove()
     */
    public function storeFilenameForRemove()
    {
        $this->temp = $this->getAbsolutePath();
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if (isset($this->temp) && file_exists ($this->temp)) {
        
			if (@unlink($this->temp) === false) {
			
				throw new \RuntimeException('File could not be deleted.');
			}        
        }
    }

    public function getAbsolutePath()
    {
        return null === $this->path
            ? null
            : $this->getUploadRootDir().'/'.$this->id.'.'.$this->path;
    }
    
    protected function getUploadRootDir()
    {
         return __DIR__.'/../../../../data/'.$this->getUploadDir();
    }
    
    protected function getUploadDir()
    {
        return 'uploads/company';
    } 
	
	
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set companyId
     *
     * @param integer $companyId
     * @return CompanySetting
     */
    public function setCompanyId($companyId)
    {
        $this->companyId = $companyId;

        return $this;
    }

    /**
     * Get companyId
     *
     * @return integer 
     */
    public function getCompanyId()
    {
        return $this->companyId;
    }

    /**
     * Set allocationMethod
     *
     * @param boolean $allocationMethod
     * @return CompanySetting
     */
    public function setAllocationMethod($allocationMethod)
    {
        $this->allocationMethod = $allocationMethod;

        return $this;
    }

    /**
     * Get allocationMethod
     *
     * @return boolean 
     */
    public function getAllocationMethod()
    {
        return $this->allocationMethod;
    }

    /**
     * Set company
     *
     * @param \Swan\CoreBundle\Entity\Company $company
     * @return CompanySetting
     */
    public function setCompany(\Swan\CoreBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \Swan\CoreBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }
    /**
     * @var string
     */
    private $companyHrEmail;

    /**
     * @var string
     */
    private $companyVehicleSystemEmail;

    /**
     * @var string
     */
    private $fromEmail;

    /**
     * @var string
     */
    private $fromName;


    /**
     * Set companyHrEmail
     *
     * @param string $companyHrEmail
     * @return CompanySetting
     */
    public function setCompanyHrEmail($companyHrEmail)
    {
        $this->companyHrEmail = $companyHrEmail;

        return $this;
    }

    /**
     * Get companyHrEmail
     *
     * @return string 
     */
    public function getCompanyHrEmail()
    {
        return $this->companyHrEmail;
    }

    /**
     * Set companyVehicleSystemEmail
     *
     * @param string $companyVehicleSystemEmail
     * @return CompanySetting
     */
    public function setCompanyVehicleSystemEmail($companyVehicleSystemEmail)
    {
        $this->companyVehicleSystemEmail = $companyVehicleSystemEmail;

        return $this;
    }

    /**
     * Get companyVehicleSystemEmail
     *
     * @return string 
     */
    public function getCompanyVehicleSystemEmail()
    {
        return $this->companyVehicleSystemEmail;
    }

    /**
     * Set fromEmail
     *
     * @param string $fromEmail
     * @return CompanySetting
     */
    public function setFromEmail($fromEmail)
    {
        $this->fromEmail = $fromEmail;

        return $this;
    }

    /**
     * Get fromEmail
     *
     * @return string 
     */
    public function getFromEmail()
    {
        return $this->fromEmail;
    }

    /**
     * Set fromName
     *
     * @param string $fromName
     * @return CompanySetting
     */
    public function setFromName($fromName)
    {
        $this->fromName = $fromName;

        return $this;
    }

    /**
     * Get fromName
     *
     * @return string 
     */
    public function getFromName()
    {
        return $this->fromName;
    }
    /**
     * @var string
     */
    private $fromEmailPassword;

    /**
     * @var string
     */
    private $smtpHost;

    /**
     * @var integer
     */
    private $smtpPort;

    /**
     * @var string
     */
    private $encryption;


    /**
     * Set fromEmailPassword
     *
     * @param string $fromEmailPassword
     * @return CompanySetting
     */
    public function setFromEmailPassword($fromEmailPassword)
    {
        $this->fromEmailPassword = $fromEmailPassword;

        return $this;
    }

    /**
     * Get fromEmailPassword
     *
     * @return string 
     */
    public function getFromEmailPassword()
    {
        return $this->fromEmailPassword;
    }

    /**
     * Set smtpHost
     *
     * @param string $smtpHost
     * @return CompanySetting
     */
    public function setSmtpHost($smtpHost)
    {
        $this->smtpHost = $smtpHost;

        return $this;
    }

    /**
     * Get smtpHost
     *
     * @return string 
     */
    public function getSmtpHost()
    {
        return $this->smtpHost;
    }

    /**
     * Set smtpPort
     *
     * @param integer $smtpPort
     * @return CompanySetting
     */
    public function setSmtpPort($smtpPort)
    {
        $this->smtpPort = $smtpPort;

        return $this;
    }

    /**
     * Get smtpPort
     *
     * @return integer 
     */
    public function getSmtpPort()
    {
        return $this->smtpPort;
    }

    /**
     * Set encryption
     *
     * @param string $encryption
     * @return CompanySetting
     */
    public function setEncryption($encryption)
    {
        $this->encryption = $encryption;

        return $this;
    }

    /**
     * Get encryption
     *
     * @return string 
     */
    public function getEncryption()
    {
        return $this->encryption;
    }
    /**
     * @var string
     */
    private $logo;


    /**
     * Set logo
     *
     * @param string $logo
     * @return CompanySetting
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return string 
     */
    public function getLogo()
    {
        return $this->logo;
    }
}
