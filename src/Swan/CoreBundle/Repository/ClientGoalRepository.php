<?php

namespace Swan\CoreBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * ClientGoalRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ClientGoalRepository extends EntityRepository
{
	/**
	 * Function to find all client goals
	 *
	 * @param $userId
	 *
	 * @return goals
	 */
	public function findGoals($userId)
	{
		$query = $this->getEntityManager()->createQueryBuilder();
		
		$query->select('cg', 'u', 'cga', 'cgt', 'a', 'ass', 'uas')
				->from('CoreBundle:ClientGoal', 'cg')
				->leftjoin('cg.user', 'u')
				->leftjoin('cg.clientGoalType', 'cgt')
				->leftjoin('cg.clientGoalAsset', 'cga')
				->leftjoin('cga.asset', 'a')
				->leftjoin('a.clientAsset', 'ca')
				->leftjoin('a.assumption', 'ass')
				->leftjoin('ass.userAssumption', 'uas')
				
				->where('u.id =:userId')->setParameter('userId', $userId)
				->andWhere('u.deleted =:deleted')->setParameter('deleted', false)
				->andwhere('u.isAdmin !=:isAdmin')->setParameter('isAdmin', 1)

				->orderBy('cg.id', 'DESC')
			;
		
		return $query->getQuery()->getResult();
	}
	
	/**
	 * Function to find all client goals
	 *
	 * @param $userId
	 *
	 * @return goals
	 */
	public function findUsedGoals()
	{
		$query = $this->getEntityManager()->createQueryBuilder();
		
		$query->select('cg.id')
				->from('CoreBundle:ClientGoal', 'cg')
				->leftjoin('cg.clientGoalAsset', 'cga')
				
				->where('cga.clientGoal IS NOT NULL')
				
			;
		
		return $query->getQuery()->getResult();
	}
	
}
