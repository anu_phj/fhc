<?php

namespace Swan\CoreBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * UserModuleActionRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class UserModuleActionRepository extends EntityRepository
{
	/**
	 * Function to delete all module actions for user
	 *
	 * @param $keysToDelete
	 *
	 * @return none
	 */
	public function deleteAllModuleActionsForUser($keysToDelete)
	{
		$query = $this->getEntityManager()->createQueryBuilder();
        
        if (!empty($keysToDelete)) {
			
			$q = $query->delete('CoreBundle:UserModuleAction', 'uma')
				->where('uma.id IN ('.implode(", ", $keysToDelete).')')
				->getQuery();
			
			$q->execute();
		}
	}

}
