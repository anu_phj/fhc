<?php

namespace Swan\CoreBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * ROleRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class RoleRepository extends EntityRepository
{
    /**
     * Function to find all roles
     *
     * @return roles
     */
    public function findAllRoles()
    {
        $query = $this->getEntityManager()->createQueryBuilder();
        
        $query->select('r')
                ->from('CoreBundle:Role', 'r')
				->where('r.status =:status')->setParameter('status', true)
            ;
                
        return $query->getQuery()->getResult();
    }

	public function findActiveRolesDetails()
	{
		$query = $this->getEntityManager()->createQueryBuilder();

		$query->select('r.name, r.slug, r.id')
			->from('CoreBundle:Role', 'r')
			->where('r.status =:status')->setParameter('status', true)
		;

		return $query->getQuery()->getArrayResult();
	}
}
