<?php

namespace Swan\CoreBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * ModuleActionRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ModuleActionRepository extends EntityRepository
{
    public function findAllModuleActions()
    {
        $query = $this->getEntityManager()->createQueryBuilder();
        
        $query->select('ma', 'm')
                ->from('CoreBundle:ModuleAction', 'ma')
	            ->leftjoin('ma.module', 'm')
            ;
                
        return $query->getQuery()->getResult();
    }
}
