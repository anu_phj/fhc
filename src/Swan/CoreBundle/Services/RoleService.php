<?php

namespace Swan\CoreBundle\Services;

use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Swan\CoreBundle\Entity\Role;

class RoleService
{
	protected $mr;
	private $container;

	public function __construct(ManagerRegistry $mr, Container $container)
	{
		$this->mr = $mr;
		$this->container = $container;
	}
	
    /**
     * Lists all Role entities.
     *
     * @return Role object result
     */     	
	public function findAllRoles()
	{
		$em = $this->mr->getManagerForClass(get_class(new Role()));
        
        return $em->getRepository('CoreBundle:Role')->findAllRoles();
	}

	public function findActiveRolesDetails()
	{
		$em = $this->mr->getManagerForClass(get_class(new Role()));

		return $em->getRepository('CoreBundle:Role')->findActiveRolesDetails();
	}
}