<?php

namespace Swan\CoreBundle\Services;

use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

use Swan\CoreBundle\Entity\EmailLog;

class MailService
{
    protected $mr;
    protected $container;
    
	public function __construct(ManagerRegistry $mr, Container $container)
	{
        $this->mr = $mr;
        $this->container = $container;
	}
	
    /**
     * validate Mail data 
     *
     * @param $dataArr
     *
     * @return bool
     */	
	private function validateMailData($dataArr)
	{
        $requiredParams = array('subject', 'body', 'to', 'companyId');
        $requiredParamsError = array('subject' => 'Mail subject', 'from' => 'Mail from email', 'body' => 'Mail body', 'to' => 'Mail to email', 'companyId' => 'company');
        
        foreach($requiredParams as $requiredParam) {
            
            if(!isset($dataArr[$requiredParam]) || empty($dataArr[$requiredParam])) {
            
                return $requiredParamsError[$requiredParam]." should not be blank";
            }
        }
        
        return $this->validateCompanySettingData($dataArr['companyId']);
	}
	
	/**
     * validate company setting table data 
     *
     * @param $companyId
     *
     * @return bool
     */ 
	public function validateCompanySettingData($companyId)
    {
        $company = $this->container->get('core_company_setting')->getCompanySettingByCompanyId($companyId);
        
        $requiredParams = array('getFromEmail', 'getFromName', 'getFromEmailPassword', 'getSmtpHost', 'getSmtpPort', 'getEncryption');
        $requiredParamsError = array('getFromEmail' => 'Mail from email', 'getFromName' => 'Mail from name', 'getFromEmailPassword' => 'Password', 'getSmtpHost' => 'Smtp host', 'getSmtpPort' => 'Smtp port', 'getEncryption' => 'Encryption');
        
        if (count($company)>0) {
            foreach($requiredParams as $requiredParam) {
                if ($company->$requiredParam()=='') return $requiredParamsError[$requiredParam]." should not be blank";
            }
            
            return true;
        }
        
        return false;
        
	}
	
	/**
     * send Mail using swiftmailer
     *
     * @param $dataArr
     *
     * @return bool
     */
    public function sendMail($dataArr)
    {
        //For check required fields for mail
        $isValidData = $this->validateMailData($dataArr);
        
        if ($isValidData !== true) {
            return $isValidData;
        }
        
        $company = $this->container->get('core_company_setting')->getCompanySettingByCompanyId($dataArr['companyId']);
        if (count($company)>0) {
            
            $message = \Swift_Message::newInstance()
                                    ->setSubject($dataArr['subject'])
                                    ->setFrom(array($company->getFromEmail() => $company->getFromName()))
                                    ->setReplyTo(array($company->getFromEmail() => $company->getFromName()))
                                    ->setContentType('text/html')
                                    ->setBody($dataArr['body']);

            $toRecipients = array_unique($dataArr['to']);
            
            if (count(array_filter($toRecipients))>0) {
                
                foreach($toRecipients as $toRecipient) {
                    $message->addTo(trim($toRecipient));
                }
                
                $this->addMailCc($dataArr, $message);
                
                $this->addMailBcc($dataArr, $message);
                
                $transport = $this->getMailerTransport($company);
                
                $mailer = \Swift_Mailer::newInstance($transport);
                
                try {
                
                    $mailer->send($message);
                    
                    $this->addMailLog($dataArr);
                    
                    return true;
                    
                } catch (\Exception $e) {
                
                    return 'Mail Error :: '.$e->getMessage();
                }
            }
        }
    }
    
    /**
     * create new swiftmailer transport
     *
     * @param $company
     *
     * @return object
     */    
    private function getMailerTransport($company) 
    {
        $transport = \Swift_SmtpTransport::newInstance($company->getSmtpHost(), $company->getSmtpPort())
                    ->setUsername($company->getFromEmail())
                    ->setPassword($company->getFromEmailPassword())
                    ->setEncryption(strtolower($company->getEncryption()))
                ;

        return $transport;
    }
    
    /**
     * add mail cc reciepts in mailer object
     *
     * @param $dataArr
     * @param $message
     *
     * @return object
     */        
    private function addMailCc($dataArr, $message) 
    {
        if ($this->isFieldExist($dataArr, 'cc')===true) {
            $ccReciepts = array_unique($dataArr['cc']);
            
            foreach($ccReciepts as $ccReciept) {
                $message->addCc(trim($ccReciept));
            }
        }
        
        return $message;
    }
    
    /**
     * add mail bcc reciepts in mailer object
     *
     * @param $dataArr
     * @param $message
     *
     * @return object
     */        
    private function addMailBcc($dataArr, $message) 
    {
        if ($this->isFieldExist($dataArr, 'bcc')===true) {
            $bccReciepts = array_unique($dataArr['bcc']);
            
            foreach($bccReciepts as $bccReciept) {
                $message->addBcc(trim($bccReciept));
            }
        }
        
        return $message;
    }
    
    /**
     * add mail log to db
     *
     * @param $dataArr
     *
     * @return null
     */            
    private function addMailLog($dataArr) 
    {
        $em = $this->mr->getManagerForClass(get_class(new EmailLog()));
        
        $emailLog = new EmailLog();
        $emailLog->setCompanyId($dataArr['companyId']);
        $emailLog->setVehicleId($dataArr['vehicleId']);
        $emailLog->setDriverId($dataArr['driverId']);
        $emailLog->setSubject($dataArr['subject']);
        $emailLog->setBody($dataArr['body']);
        $emailLog->setSendDate(new \DateTime());
        $emailLog->setMailTypeId($dataArr['mailTypeId']);
        $emailLog->setMailTo(implode(", ", $dataArr['to']));
        
        if($this->isFieldExist($dataArr, 'cc')===true) $emailLog->setMailCc(implode(", ", $dataArr['cc'])); 
        if($this->isFieldExist($dataArr, 'bcc')===true) $emailLog->setMailBcc(implode(", ", $dataArr['bcc'])); 
        
        $em->persist($emailLog);
        $em->flush();
    }
    
    /**
     * check if field exist in array
     *
     * @param $dataArr
     *
     * @return bool
     */                
    private function isFieldExist($dataArr, $fieldName) 
    {
        if (isset($dataArr[$fieldName]) && count(array_filter($dataArr[$fieldName]))>0) return true;
        
        return false;
    }
}