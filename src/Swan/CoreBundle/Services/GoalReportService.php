<?php

namespace Swan\CoreBundle\Services;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Swan\CoreBundle\Entity\GoalReport;

class GoalReportService
{
    protected $mr;
	private $container;

	public function __construct(ManagerRegistry $mr, Container $container)
	{
		$this->mr = $mr;
		$this->container = $container;
	}
	
    /**
     * Lists all Goal Reports
     *
     * @return goal report object result
     */     	
	public function findAllGoalReports($userId) 
	{
		$em = $this->mr->getManagerForClass(get_class(new GoalReport()));
		
		return $em->getRepository('CoreBundle:GoalReport')->findBy(array('user'=>$userId));
	}
	
	/**
     * get all Vehicle entities as array.
     *
     * @param Integer $companyId
     * @param boolean $showActive
     *
     * @return Vehicle object result
     */         
    public function getAllGoalReportsAsArray($userId) 
    {
        $em = $this->mr->getManagerForClass(get_class(new GoalReport()));
        
        $datatableService = $this->container->get('core_datatable');
        
        $result = $datatableService->initJsonArray();
        $goalReports = $this->findAllGoalReports($userId);
        
        if (!empty($goalReports)) { 
            
            foreach($goalReports as $goalReport) {
                
                $result['data'][] = $this->getGoalReportAsArray($goalReport);
            }
        } 
        
        return $result;
    }
    
    
    
    /**
     * get all vehicle data in array
     *
     * @param Object $vehicle
     * @param Object $costAllocationMaxLevel
     *
     * @return resultArray
     */
    public function getGoalReportAsArray($goalReport)
    {
        $resultArray = array(
                        "fileName" => $goalReport->getFileName(),
                        "fileCustomName" => $this->getCustomFileName($goalReport),
                        "dateGenerated" => $goalReport->getDateGenerated()->format('d-m-Y'),
						"actions" => $this->getDownloadLink($goalReport),
                    );
        		
        return $resultArray;
    }
    
    /**
     * get vehicle action buttons
     *
     * @param Object $vehicle
     *
     * @return String
     */        
    private function getCustomFileName($goalReport)
    { 
        $links =  '<span>'.$goalReport->getCustomFileName().'</span>&nbsp;&nbsp;<a href="javascript:void(0)" class="editReport" data="'.$goalReport->getCustomFileName().'" id="'.$this->container->get('router')->generate('_core_goal_report_edit', array('reportId' => $goalReport->getId())).'" class="editVehicle margin-right10" title="'.$this->container->get('translator')->trans('Edit').'"><i class="fa fa-pencil"></i></a>';
        
        return $links;
    }
	
	private function getDownloadLink($goalReport)
	{
		$link = '<a href="'.$this->container->get('router')->generate('_core_pdf_generate', array('userId' => $goalReport->getUser()->getId())).'" class="downloadUser" target="_blank" title="Download Report" id=""><i class="fa fa-download"></i></a>';
		
		return $link;
	}
    
}