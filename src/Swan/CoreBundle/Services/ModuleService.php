<?php

namespace Swan\CoreBundle\Services;

use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Swan\CoreBundle\Entity\Module;

class ModuleService
{
	protected $mr;
	private $container;

	public function __construct(ManagerRegistry $mr, Container $container)
	{
		$this->mr = $mr;
		$this->container = $container;
	}

	public function getAllModulesWithActions()
	{
		$em = $this->mr->getManagerForClass(get_class(new Module()));

		return $em->getRepository('CoreBundle:Module')->getAllModulesWithActions();
	}
}