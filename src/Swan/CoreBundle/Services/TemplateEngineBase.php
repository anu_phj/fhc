<?php

namespace Swan\CoreBundle\Services;

use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Swan\CoreBundle\Entity\Vehicle;
use Swan\CoreBundle\Entity\Driver;

class TemplateEngineBase
{
	protected $mr;
	protected $em;
	protected $container;
	protected $reservedVariables; 
	protected $vehicle; 
	protected $driver; 
	
	public function __construct(ManagerRegistry $mr, Container $container)
	{
		$this->mr = $mr;
		$this->container = $container;
		$this->reservedVariables = $this->getReservedVariables();
		$this->em = $this->mr->getManagerForClass(get_class(new Vehicle()));
	}

    /**
     * Get reserved Variables
     *
     * @return array
     */	
	protected function getReservedVariables() 
	{
        return array(
            '{{contact_person.name}}' => 'contact_person_name',
            '{{vehicle.licence_plate}}' => 'vehicle_license_plate',
            '{{vehicle.chassis_number}}' => 'vehicle_chassis_number',
            '{{vehicle.brand.name}}' => 'vehicle_brand_name',
            '{{vehicle.model}}' => 'vehicle_model',
            '{{vehicle.type}}' => 'vehicle_type',
            '{{vehicle.fiscal_value}}' => 'vehicle_fiscal_value',
            '{{vehicle.income_tax_addition.name}}' => 'vehicle_income_tax_addition_name',
            '{{driver.old.name}}' => 'driver_old_name',
            '{{driver.old.employeeId}}' => 'driver_old_employeeId',
            '{{driver.old.cost_allocation.name}}' => 'driver_old_cost_allocation_name',
            '{{driver.old.allocation.date_end}}' => 'driver_old_allocation_date_end',
            '{{allocation.date_start}}' => 'allocation_date_start',
            '{{diver.new.name}}' => 'driver_new_name',
            '{{diver.new.employeeId}}' => 'driver_new_employeeId',
            '{{diver.new.gender}}' => 'driver_new_gender',
            '{{diver.new.street}}' => 'driver_new_street',
            '{{diver.new.house_number}}' => 'driver_new_house_number',
            '{{diver.new.house_number_suffix}}' => 'driver_new_house_number_suffix',
            '{{diver.new.postal_code}}' => 'driver_new_postal_code',
            '{{diver.new.city}}' => 'driver_new_city',
            '{{diver.new.phone_number}}' => 'driver_new_phone_number',
            '{{diver.new.mobile_number}}' => 'driver_new_mobile_number',
            '{{diver.new.email}}' => 'driver_new_email',
            '{{diver.new.cost_allocation.name}}' => 'driver_new_cost_allocation_name'
        );
	}
	
    /**
     * Funciton to get vehicle object
     *
     * @param Integer $vehicleId 
     *
     * @return Vehicle
     */                                 
	protected function vehicle($vehicleId)
	{
		if (is_object($this->vehicle) && count($this->vehicle)>0) {
		
			if ($this->vehicle->getId()==$vehicleId) {
			
				return $this->vehicle;
			}
		}
		
		return $this->getEntity(new Vehicle(), $vehicleId);
	}
	
    /**
     * Funciton to get driver object
     *
     * @param Integer $driverId 
     *
     * @return Driver
     */                                 
	protected function driver($driverId)
	{
		if (is_object($this->driver) && count($this->driver)>0) {
		
			if ($this->driver->getId()==$driverId) {
			
				return $this->driver;
			}
		}
		
		return $this->getEntity(new Driver(), $driverId);
	}
	
    /**
     * Function to get entity
     *
     * @param Object $entity 
     * @param Integer $entityId 
     *
     * @return Object
     */                                 
	protected function getEntity($entity, $entityId)
	{
		$class = $this->em->getClassMetadata(get_class($entity))->getName();

		$entity = $this->em->getRepository($class)->find($entityId);
		if (count($entity)>0) {

			return $entity;
		}
	}
	
    /**
     * Function to get entity fields for vehicle/driver
     *
     * @param String $entityType
     * @param Integer $entityId 
     * @param String $field 
     *
     */                                 
	protected function getEntityFields($entityType, $entityId, $field)
	{
		if ($entityId >0) { 
		
			$entity = $this->getEntityObj($entityType, $entityId);
			
			if (!empty($entity)) {
			
				return $this->getFieldFromObj($entity, $field);
			}
		}
	}
	
    /**
     * Function to get field
     *
     * @param String $field 
     *
     */                                 
	protected function getFieldFromObj($entity, $field)
	{
		if (!empty($entity)) {
			
			if ($entity->$field()!='') {
			
				return $entity->$field();
			}
		}
	}
	
    /**
     * Function to get entity
     *
     * @param String $entityType
     * @param Integer $entityId 
     *
     */                                 
	protected function getEntityObj($entityType, $entityId)
	{
		switch ($entityType) {
			
			case 'vehicle': 
				$entity = $this->vehicle($entityId);
				break;
				
			case 'driver': 
				$entity = $this->driver($entityId);
				break;

			default:
				$entity = '';
				break;
		}
		
		return $entity;
	}
	
    /**
     * Function to get cost allocation of driver/vehicle
     */                                 
	protected function getCostAllocation($driverId = false, $vehicleId = false)
	{
		if ($driverId >0) {
		
			$parameter = $driverId;
			$entity = 'CoreBundle:DriverCostAllocation';
		} else {
		
			$parameter = array('vehicleId' => $vehicleId);
			$entity = 'CoreBundle:VehicleCostAllocation';
		} 
		
		$entityCostAllocation = $this->em->getRepository($entity)->findActiveCostAllocation($parameter);
		if (count($entityCostAllocation) >0) {
			
			$entityCostAllocation = reset($entityCostAllocation);
			
			if (count($entityCostAllocation->getCostAllocation()) >0) {
			
				return $entityCostAllocation->getCostAllocation();
			}
		}
	}
	
    /**
     * Funciton to get contact person
     */                                 
	protected function getContactPerson($params) 
	{
		$driverId = isset($params['driverId']) ? $params['driverId'] : 0;
		$vehicleId = isset($params['vehicleId']) ? $params['vehicleId'] : 0;
		
		$costAllocation = $this->getCostAllocation($driverId, $vehicleId);
		
		if (count($costAllocation)>0 && count($costAllocation->getContactPerson()) > 0) {
			
			$contactPerson = $costAllocation->getContactPerson();
			
			if (count($contactPerson) >0) {
				
				return $contactPerson;
			}
		}
	}
	
    /**
     * Function to get previous allocation
     */                                 
	protected function getPreviousAllocation($vehicleId, $allocation) 
	{
		$criteria = array('dateStart' => $allocation->getDateStart()->format('Y-m-d'));
		
		$previousAllocation = $this->em->getRepository('CoreBundle:Allocation')->findNearByAllocation($vehicleId, $criteria);
		if (count($previousAllocation) >0) {
			
			$previousAllocation = reset($previousAllocation);
			
			return $previousAllocation;
		}
	}
	
    /**
     * Function to get previous allocation driver
     */                                 
	protected function getPreviousAllocationDriver($vehicleId, $allocation) 
	{
		$allocation = $this->getPreviousAllocation($vehicleId, $allocation);
		if (count($allocation) >0) {
			
			if (count($allocation->getDriver()) >0) {
			
				return $allocation->getDriver();
			}
		}
	}
	
    /**
     * Function to get driver full name
     */                                 
	protected function getDriverFullName($driver) 
	{
		if (count($driver) >0) {
			
			return $driver->getInitials()." ".$driver->getFamilyNamePrefix()." ".$driver->getFamilyName();
		}
	}
	
	/**
     * Function to return cost allocation HTML for mailTemplate
     */
	public function returnHTMLForMergedCostAllocation($costAllocation)
    {
		if ($costAllocation != '') {
		
			$parentArr = explode("<!>", $costAllocation);
			
			$html = !empty($parentArr) ? implode(" > ",$parentArr):'';
			
			return $html;
		}
    }
    
    /**
     * Function to return pool or general use allocation name
     */
    protected function getPoolOrGeneralUseName($allocation)
    {
        $allocations = array('', 'generalUseAllocation', 'poolAllocation');
        if (count($allocation)>0) {
            
            return isset($allocations[$allocation->getType()]) ? $this->container->get('translator')->trans($allocations[$allocation->getType()]) : '';
        }
        
    }
}