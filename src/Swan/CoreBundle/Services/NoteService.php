<?php

namespace Swan\CoreBundle\Services;

use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\HttpFoundation\Response;
use Swan\CoreBundle\Entity\ClientNote;
use Symfony\Component\Form\FormFactoryInterface;
use Swan\CoreBundle\Form\ClientNoteType;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class NoteService
{
	protected $mr;
	private $container;
	protected $formFactory;

	public function __construct(ManagerRegistry $mr, Container $container, FormFactoryInterface $formFactory)
	{
		$this->mr = $mr;
		$this->container = $container;
		$this->formFactory = $formFactory;
	}
	
    /**
     * Lists all Note entities.
     *
     * @param Integer $userId The user entity id
     * @param Integer $entityId The entity id
     * @param string  $formType The Form Type
     *
     * @return notes
     */         
    public function findAllNotes($userId, $entityId, $formType) 
    {
        $em = $this->mr->getManagerForClass(get_class(new ClientNote()));
        
        return $em->getRepository('CoreBundle:ClientNote')->findAllNotesForUser($userId, $entityId, $formType);
    }
    
    /**
     * Creates form of all notes
     *
     * @param String $formType 
     * @param Object $entity 
     * @param String $url 
     *
     * @return \Symfony\Component\Form\Form The form
     */
    public function createNoteForm($formType, $entity, $url=false)
    {
        $form = $this->formFactory->create($this->getNoteFormType($formType), $entity, array(
            'action' => $url!='false' ? $url : '',
            'method' => 'POST',
        ));

        return $form;
    }
    
    /**
     * return form for note.
    */         
    public function getNoteFormType($formType)
    {
        switch ($formType) {
        
            case 'client':
                $formType = new ClientNoteType();
                break;
        }
        
        return $formType;
    }
    
    /**
     * Lists all Note entities.
     *
     * @param Integer $userId The user entity id
     * @param Integer $entityId The note entity id
     * @param string  $noteFormType The Note Form Type
     *
     * @return notesArray 
    */         
    public function getNotes($userId, $entityId, $noteFormType)
    {
        $notes = $this->findAllNotes($userId, $entityId, $noteFormType);

        $notesArray = $this->getNotesArray($notes, $noteFormType);
        
        return $notesArray;
    }
    
    /**
    * Function to get notes 
    */
    public function getNotesArray($notes, $noteFormType)
    {
        $infoContainer = $editNoteForm = array();
        
        if (count($notes)) {
            foreach($notes as $note)
            {
                $noteId = $note->getId();
                
                $url = $this->generateUrl('_core_note_edit', array('entity' =>$noteFormType, 'noteId' => $noteId));
                
                $editForm = $this->createNoteForm($this->getNoteFormType($noteFormType), $note, $url);
                $editNoteForm[$noteId] = $editForm->createView();
            }
            
            $infoContainer['editForm'] = $editNoteForm;
            $infoContainer['notes'] = $notes;
        }
        
        return $infoContainer;
    }
    
    /**
     * Generates a URL from the given parameters.
     *
     * @param string      $route         The name of the route
     * @param mixed       $parameters    An array of parameters
     * @param bool|string $referenceType The type of reference (one of the constants in UrlGeneratorInterface)
     *
     * @return string The generated URL
     *
     * @see UrlGeneratorInterface
    */
    protected function generateUrl($route, $parameters = array(), $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH)
    {
        return $this->container->get('router')->generate($route, $parameters, $referenceType);
    }
    
    /**
     * Get Entity name
     *
     * @param string $formType The FormType for entity
     * @param integer $entityId The entity id
     *
     * @return string The generated URL
     */
    public function getEntity($formType, $entityId=false)
    {
        $em = $this->mr->getManagerForClass(get_class(new ClientNote()));
        
        $entity = '';
        if ($formType == 'client') {
            if($entityId>0) {
                $entity = $em->getRepository('CoreBundle:ClientNote')->find($entityId);
            } else {
                $entity = new ClientNote();
            }
        }
        
        return $entity;
    }    
    
    /**
     * Function to save entityId for Note in database
     *
     * @param string      $entityType      The FormType for entity
     * @param integer     $entityId        The entity id for Note
     * @param object      $entity          The note entity
     *
     * @return the Note Entity
     *
    */
    public function setNoteEntity($entityType, $entityId, $entity)
    {   
        $em = $this->mr->getManagerForClass(get_class(new ClientNote()));
        
        if ($entityType == 'client') {
        
            $client = $em->getRepository('CoreBundle:User')->find($entityId);
            $entity->setUser($client);

        }
        
        return $entity;
    }
    
    /**
     * Function to Note Form Twig
     *
     * @param object      $form      return the form object
     * @param integer     $entityId  The entity id for Note
     * @param object      $entity    The note entity
     *
     */
    public function renderNoteFormTwigFile($entity, $entityId, $form)
    {
        
        if($entity=='client') {
            
            return $this->container->get('templating')->renderResponse('CoreBundle:ClientNote:new.html.twig', array(
                'clientId' => $entityId,
                'form' => $form->createView(),
            ));
        }
    }
    
    /**
     * Function to Note List Twig
     *
     * @param array       $infoContainer    return the array
     * @param integer     $entityId         The entity id for Note
     * @param object      $entity           The note entity
     *
     */
    public function renderNoteListTwigFile($entity, $entityId, $infoContainer=false)
    {
        if($entity=='client') {
            
            return $this->container->get('templating')->renderResponse('CoreBundle:ClientNote:noteInformation.html.twig', array(
                'clientId' => $entityId,
                'edit_form'  => isset($infoContainer['editForm']) ? $infoContainer['editForm'] : false,
                'notes' => isset($infoContainer['notes'])  ? $infoContainer['notes'] : false
            ));
        }
    }
    
    /**
     * Function to add Note in database
     *
     * @param object      $request         The request object
     * @param object      $form            The form object for Note
     * @param object      $entity          The note entity
     * @param string      $entityType      The FormType for entity
     * @param integer     $entityId        The entity id for Note
     *
     * @return the response message
     *
    */
    public function addNote($request, $form, $entity, $entityType, $entityId)
    {
        $em = $this->mr->getManagerForClass(get_class(new ClientNote()));
        
        $form->handleRequest($request);
        
        $infoArr = array();
        
        if ($form->isValid()) {

            $user = $this->container->get('security.context')->getToken()->getUser();
            $userInfo = $em->getRepository('CoreBundle:User')->find($user->getId());
            
            $entity->setDateCreated(new \DateTime);
            $entity->setUserAddNote($userInfo);
            
            $this->setNoteEntity($entityType, $entityId, $entity);
            
            $em->persist($entity);
            $em->flush();
            
            $infoArr['success'] = $this->container->get('translator')->trans('addSuccess');
        }
        
        $infoArr['error'] = $this->container->get('translator')->trans('someError');
        
        $response = new Response(json_encode($infoArr));
        $response->headers->set('Content-Type', 'application/json');
    
        return $response;
    }
    
    /**
     * Function to update Note in database
     *
     * @param object      $request         The request object
     * @param object      $form            The form object for Note
     * @param object      $entity          The note entity
     *
     * @return the response message
     *
    */
    public function editNote($request, $form, $entity)
    {
        $em = $this->mr->getManagerForClass(get_class(new ClientNote()));
        
        $form->handleRequest($request);
        
        $infoArr = array();
        
        if ($form->isValid()) {
                
            $em->persist($entity);
            $em->flush();
            
            $infoArr['success'] = $this->container->get('translator')->trans('updateSuccess');
        } else {
            $infoArr['error'] = $this->container->get('translator')->trans('someError');
        }
        
        $response = new Response(json_encode($infoArr));
        $response->headers->set('Content-Type', 'application/json');
    
        return $response;
    }
}