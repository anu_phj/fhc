<?php

namespace Swan\CoreBundle\Services;

use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Swan\CoreBundle\Entity\FleetStat;

class FleetStatService
{
	protected $mr;
	private $container;
	protected $wbsCore;

	public function __construct(ManagerRegistry $mr, Container $container, $wbsCore)
	{
		$this->mr = $mr;
		$this->container = $container;
		$this->wbsCore = $wbsCore;
	}

	/**
	 * Get fleet stat by date
	 *
	 * @param $companyId
	 * @param null $date
	 * @return array
	 */
	public function getFleetStatByDate($companyId, $date=NULL)
	{
		$em = $this->mr->getManagerForClass(get_class(new FleetStat()));

		$date = $this->wbsCore->validDate($date);

		if(empty($date)) {

			$date = date('Y-m-d');
		}

		$totalActiveVehicle = $em->getRepository('CoreBundle:Vehicle')->countActiveVehicles($companyId);

		$totalActiveVehicleLease = $em->getRepository('CoreBundle:Vehicle')->countVehicleContractByDateAndContractType($companyId, $date, 'LeaseContract');
		$totalActiveVehicleLeaseLimited = $em->getRepository('CoreBundle:Vehicle')->countVehicleContractByDateAndContractType($companyId, $date, 'LeaseContractLimited');
		$totalActiveVehicleRent = $em->getRepository('CoreBundle:Vehicle')->countVehicleContractByDateAndContractType($companyId, $date, 'OnRentContract');
		$totalActiveVehicleFuel = $em->getRepository('CoreBundle:Vehicle')->countVehicleContractByDateAndContractType($companyId, $date, 'FuelContract');

		$ownByCompanyCriteria = array('ownershipType' => 'Ownership');
		$totalActiveVehicleOwnedByCompany = $em->getRepository('CoreBundle:Vehicle')->countActiveVehicles($companyId, $ownByCompanyCriteria);

		return array(
			$date => array(
				'totalActiveVehicle' => $totalActiveVehicle,
				'totalActiveVehicleLease' => $totalActiveVehicleLease,
				'totalActiveVehicleLeaseLimited' => $totalActiveVehicleLeaseLimited,
				'totalActiveVehicleRent' => $totalActiveVehicleRent,
				'totalActiveVehicleFuel' => $totalActiveVehicleFuel,
				'totalActiveVehicleOwnedByCompany' => $totalActiveVehicleOwnedByCompany
			)
		);
	}

	/**
	 * Lists Fleet Stat entities
	 *
	 * @param $companyId
	 * @param null $startDate
	 * @param null $endDate
	 * @return mixed
	 */
	public function findFleetStatByDates($companyId, $startDate=NULL, $endDate=NULL)
	{
		$em = $this->mr->getManagerForClass(get_class(new FleetStat()));

		$startDate = $this->wbsCore->validDate($startDate);
		$endDate = $this->wbsCore->validDate($endDate);

		$validateDatesResp = $this->_validateDates($startDate, $endDate);

		if($validateDatesResp['status']=='error') {

			return $validateDatesResp['message'];
		}

		if(empty($startDate)) {

			$startDate = date('Y-m-d');
		}

		if(empty($endDate)) {

			$endDate = $startDate;
		}

        $fleetStats = $em->getRepository('CoreBundle:FleetStat')->findFleetStatByDates($companyId, $startDate, $endDate);

		return $this->processFleetStatForGraph($fleetStats);
	}
	
	/**
	 * Function that return content of fleet stat entity 
	 * in array
	 *
	 * @param $fleetStats
	 * @return array
	 */
	public function processFleetStatForGraph($fleetStats)
	{
		$result = array();
		if(!empty($fleetStats)) {

			foreach($fleetStats as $fleetStat) {

				$result[$fleetStat->getDateGenerated()->format('Y-m-d')] = array(
					'totalActiveVehicle' => $fleetStat->getTotalActiveVehicle(),
					'totalActiveVehicleLease' => $fleetStat->getTotalActiveVehicleLease() + $fleetStat->getTotalActiveVehicleLeaseLimited(),
					'totalActiveVehicleRent' => $fleetStat->getTotalActiveVehicleRent(),
					'totalActiveVehicleFuel' => $fleetStat->getTotalActiveVehicleFuel(),
					'totalActiveVehicleOwnedByCompany' => $fleetStat->getTotalActiveVehicleOwnedByCompany()
				);
			}
		}

		return $result;
	}

	/**
	 * @param $startDate
	 * @param $endDate
	 * @return array
	 */
	private function _validateDates($startDate, $endDate)
	{
		$response = array('status' => 'success');
		if(!empty($startDate) && !empty($endDate) && (strtotime($startDate) > strtotime($endDate))) {

			$response['status'] = 'error';
			$response['message'] = $this->container->get('translator')->trans('startDateGreaterThanEndDate');
		}elseif(empty($startDate) && !empty($endDate)){

			$response['status'] = 'error';
			$response['message'] = $this->container->get('translator')->trans('invalidDates');
		}

		return $response;
	}
}