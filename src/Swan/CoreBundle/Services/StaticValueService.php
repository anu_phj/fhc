<?php

namespace Swan\CoreBundle\Services;

use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Response;

use Swan\CoreBundle\Entity\Brand;
use Swan\CoreBundle\Entity\Vehicle;

class StaticValueService
{
	protected $mr;
	private $container;
	protected $formFactory;
	protected $wbsCore;
    protected $staticValueEntity;
    
	public function __construct(ManagerRegistry $mr, Container $container, FormFactoryInterface $formFactory, $wbsCore, $staticValueEntity)
	{
		$this->mr = $mr;
		$this->container = $container;
		$this->formFactory = $formFactory;
		$this->wbsCore = $wbsCore;
		$this->staticValueEntity = $staticValueEntity;
	}

	/**
	 * Creates form of all static values
	 *
	 * @param String $formType 
	 * @param Object $entity 
	 * @param String $url 
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	public function createStaticValueForm($formType, $entity, $url=false)
	{
		$form = $this->formFactory->create($this->staticValueEntity->getFormType($formType), $entity, array(
			'action' => $url!='false' ? $url : '',
			'method' => 'POST',
		));

		return $form;
	}
	
	/**
	 * Add and edit static values
	 *
	 * @param Integer $formType
	 * @param Object $form 
	 * @param Object $request  Request
	 * @param Object $entity 
	 *
	 * @return Response
	 */
	public function addEditStaticValues($formType, $form, $request, $entity)
	{
		$infoArr = array();
		
		$form->handleRequest($request);

		$errors = $this->wbsCore->validate($entity);

		if ($form->isValid() && count($errors)<=0) {

			$em = $this->mr->getManagerForClass(get_class(new Brand()));
			
			if ($formType == 'supplier' || $formType == 'documentType') {
			
				$this->wbsCore->setCompany($entity);
			}
			
			$em->persist($entity);
			$em->flush();
			
			$infoArr['success'] = $this->container->get('translator')->trans('addSuccess');
		} else {

			$infoArr['error'] = $errors[0]->getMessage();
		}

		$response = new Response(json_encode($infoArr));
		$response->headers->set('Content-Type', 'application/json');

		return $response;
	}
	
    /**
     * Gets used brand on other entites
     *
     * @return entity
     */
    public function getUsedStaticValues($entityName) 
    {
        $infoArr = array();
        
        $entities = $this->staticValueEntity->getUsedStaticValuesEntities($entityName);
        
        if (count($entities)>0) {
        
            foreach($entities as $entity) {
                
                $infoArr[] = $this->staticValueEntity->getFkEntityValue($entity, $entityName);
            }
        }
        
        return $infoArr;
    }
    
}