<?php

namespace Swan\CoreBundle\Services;

use Doctrine\Common\Persistence\ManagerRegistry;
use Swan\CoreBundle\Entity\ClientAsset;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

use Swan\CoreBundle\Entity\User;
use Swan\CoreBundle\Entity\Role;
use Swan\CoreBundle\Entity\RolePermission;
use Swan\CoreBundle\Entity\UserModuleAction;
use Swan\CoreBundle\Entity\UserAssumption;
use Swan\CoreBundle\Entity\Company;

class UserService
{
	protected $mr;
	private $container;
	protected $userAccess;
	protected $wbsCore;


	public function __construct(ManagerRegistry $mr, Container $container, $userAccess, $wbsCore)
	{
		$this->mr = $mr;
		$this->container = $container;
		$this->userAccess = $userAccess;
		$this->wbsCore = $wbsCore;
	}
	
	/**
	 * Get all users
	 * @param $companyId
	 *
	 * @return user
	 */
	public function getAllUsers($companyId, $userType)
	{
		$em	=	$this->mr->getManagerForClass(get_class(new User()));
		
		$type = $this->getUserTypeForDb($userType);
		
		$users = $em->getRepository('CoreBundle:User')->findAllUsers($companyId, $type);
		
		return $users;
	}
	
	public function getUserTypeForDb($userTypeFromUrl)
	{
		$userType = NULL;
		if(!empty($userTypeFromUrl)) {
			
			switch($userTypeFromUrl) {
				
				case 'dealer':
					$userType = 1;
				break;
				case 'staff':
					$userType = 2;
				break;
				case 'client':
					$userType = 3;
				break;
			}
		}
		
		return $userType;
	}
	
	/**
	 * Assign a company to the newly added user
	 *
	 */
	public function setUserCompany($request, $user)
	{
		if ($user->getId()>0) {
			
			$em	= $this->mr->getManagerForClass(get_class(new User()));

			$this->userAccess->addUserAccess($request, $user);
			
			$company = $this->wbsCore->getEntity('company', $this->container->get('session')->get('company'));
			
			$user->addCompany($company);
			
			$em->persist($user);
			$em->flush();
		}
	}
	
	/**
	* set user roles user entity
	*
	* @param user object
	* @param array $roles
	*
	* @return none
	*/
	public function setUserRole($user, $roles)
	{ 
		
		if(!empty($roles)) {
		
			$em = $this->mr->getManagerForClass(get_class(new Role()));
			
			$rolesSlug = array();
			
			foreach($roles as $role) {
				
				$objRole = $em->getRepository('CoreBundle:Role')->find($role);
				
				if (count($objRole)>0) {		
				
					$rolesSlug[] = $objRole->getSlug();
				
					$user->addRole($objRole);
				}
			}
			
			$this->setUserIsAdmin($user, $rolesSlug); 
		}
	}
	
	/**
	* delete user roles
	*
	* @param $user object entity 	
	*
	* @return none
	*/
	public function deleteUserRoles($user)
	{
		$userRoles= $user->getRole();
		
		if(!empty($userRoles)) {
		
			foreach($userRoles as $userRole) {
			
				$user->removeRole($userRole);
			
			}
		}
	}
	
	/**
	* get user roles
	*
	* @param $userId 	
	*
	* @return array of user roles
	*/
	public function getUserRoles($userId)
	{
		$em = $this->mr->getManagerForClass(get_class(new User()));
		
		$user = $em->getRepository('CoreBundle:User')->find($userId);
		
		$userRoles = array();
		
		if(!empty($user)) {
		
			$roles = $user->getRole();

			if(!empty($roles)) {
				
				foreach($roles as $role) {
				
					$userRoles[] = $role->getSlug();
				}
			}
		}
		
		return $userRoles;
	}
	
	/**
	* update user is admin field on behalf of role
	*
	* @param $user object entity 	
	*
	* @param $roles 	
	*
	* @return none
	*/
	public function setUserIsAdmin($user, $roles) {
		
		if(!empty($roles)) {
		
			if (in_array('admin', $roles)) {
				
				$user->setIsAdmin(true);
			}else{
			
				$user->setIsAdmin(false);
			}
		}
	}

	/**
	 * Removes old User Assumptions
	 *
	 */
	public function removeUserAssumptions($userAssumptions)
	{
		if(!empty($userAssumptions)) {

			foreach($userAssumptions as $userAssumption) {

				$notFlush = true;
				$this->wbsCore->removeEntity(new UserAssumption(), $userAssumption->getId(), $notFlush);
			}
		}
	}

	/**
	 * Removes old User Assets
	 *
	 */
	public function removeUserAssets($userAssets)
	{
		if(!empty($userAssets)) {

			foreach($userAssets as $userAsset) {

				$notFlush = true;
				$this->wbsCore->removeEntity(new ClientAsset(), $userAsset->getId(), $notFlush);
			}
		}
	}
	
	public function setRolePermissions($user)
	{
		$em = $this->mr->getManagerForClass(get_class(new User()));
		
		$userId = $user->getId();
		$roleId = count($user->getRole())>0 ? $user->getRole()[0]->getId() : 0;
		
		$rolePermissions = $em->getRepository('CoreBundle:RolePermission')->findBy(array('role'=>$roleId));
		
		if(count($rolePermissions)>0) {
			foreach($rolePermissions as $rolePermission) {
				
				$userModuleAction = new UserModuleAction();
				$userModuleAction->setUser($user);
				$userModuleAction->setModuleAction($rolePermission->getModuleAction());
				
				$em->persist($userModuleAction);
			}
			
			$em->flush();
		}
	}
	
	public function setDealerCompany($dealerCompany, $user)
	{
		if (!empty($dealerCompany)) {
			
			$em = $this->mr->getManagerForClass(get_class(new User()));
			$newCompany = new Company();
			$newCompany->setName($dealerCompany);
			$newCompany->setArnCode($dealerCompany);
			
			$em->persist($newCompany);
			$em->flush();
			
			if (!$em->isOpen()) {
			    $em = $this->mr->resetManager();
			}
			
			if ($user->getId()>0) {
				$company = $this->wbsCore->getEntity('company', $newCompany->getId());
				$user->addCompany($company);
				$em->persist($user);
				
				$user = $em->getRepository("CoreBundle:User")->findBy(array('isAdmin'=> true));
				if(count($user)>0) {
					$user = reset($user);
					$user->addCompany($company);
					$em->persist($user);
				}
				
				$em->flush();
			} 
		}
	}
	
	public function updateDealerCompany($dealerCompany)
	{
		$em = $this->mr->getManagerForClass(get_class(new User()));
		if (!empty($dealerCompany)) {
			
			$companyInSession = $this->container->get('website.twig.extension')->getCompanyNameFromId($this->container->get('session')->get('company'));
			
			if ($companyInSession != $dealerCompany) {
				$company = $this->wbsCore->getEntity('company', $this->container->get('session')->get('company'));
				$company->setName($dealerCompany);
				$em->persist($company);
				
				$em->flush();
			}
		}
	}
	
}