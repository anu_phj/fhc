<?php

namespace Swan\CoreBundle\Services;

use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Swan\CoreBundle\Entity\Upload;

class UploadService
{
	protected $mr;
	private $container;

	public function __construct(ManagerRegistry $mr, Container $container)
	{
		$this->mr = $mr;
		$this->container = $container;
	}
	
    /**
     * Get uploads dir
     *
     * @param String $entityType
     * @param Integer $entityId
     *
     * @return upload path
     */
	public function getUploadDir($entityType, $entityId)
	{
		switch ($entityType) {
		
		    case 'client':
					$uploadDir = '/user/'.$entityId;
					break;
					
			default:
					$uploadDir = '';
					break;
			
		}
		
		return $uploadDir;
	}
	
    /**
     * Create folder if doesnot exist
     *
     * @param String $uploadFolder
     *
     * @return client upload path
     */
	public function createFolder($uploadFolder)
	{
		if ($uploadFolder!='' && !file_exists ($uploadFolder) ) {
		
			if (@mkdir($uploadFolder, 0755) === false) {
			
				throw new \RuntimeException('The directory could not be created.');
			}		
		}
		
		return true;
	}
	
    /**
     * Get uploads path
     *
     * @param String $entityType
     * @param Integer $entityId
     *
     * @return upload path
     */
	public function getUploadPath($entityType, $entityId) 
	{
		$uploadFolder = $this->container->getParameter('_core_uploadDir').$this->getUploadDir($entityType, $entityId);
		
		$this->createFolder($uploadFolder);
		
		return $uploadFolder;
	}
	
	/**
	 * Get all client uploads
	 * @param String $entityType
	 * @param Integer $entityId
	 *
	 * @return uploads
	 */
	public function findAllUploads($entityType, $entityId)
	{
		$em = $this->mr->getManagerForClass(get_class(new Upload()));
		
		switch ($entityType) {
		    case 'client':
					$uploads = $em->getRepository('CoreBundle:Upload')->findAllClientUploads($entityId);
					break;
			default:
					$uploads = '';
					break;
		}
		
		return $uploads;
	}
	
	
	/**
	 * Download uploaded file
	 * @param String $fileName
	 * @param String $filePath
	 *
	 * @return uploads
	 */
	public function download($fileName, $filePath)
	{
		if ($fileName!='' && $filePath!='') {

			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename=' . basename($fileName));
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($filePath));

			if (@ob_clean() === false) {
				
				throw new \RuntimeException('The file does not exist.');
			}

			flush();
			readfile($filePath);
		}
	}
	
	/**
	 * Download file
	 *
	 * @param Integer $uploadId
	 * @param String $entityType
	 * @param Integer $entityId
	 *
	 * @return uploads
	 */
	public function downloadFile($uploadId, $entityType, $entityId)
	{
		$em = $this->mr->getManagerForClass(get_class(new Upload()));

		$upload = $em->getRepository('CoreBundle:Upload')->find($uploadId);
		if (count($upload) >0) {	
		
			$filePath = $this->getUploadPath($entityType, $entityId)."/".$upload->getSaveName();

			if (file_exists($filePath)) {

				$this->download($upload->clean($upload->getOriginalFileName()), $filePath);
			} else {
			
				$this->container->get('session')->getFlashBag()->add('warning', $this->container->get('translator')->trans('file does not exist'));
			}
		}
		
		return false;
	}
	
	/**
	 * Add uploads to client
	 * @param String $entityType
	 * @param Object $entity
	 * @param Upload $upload
	 *
	 * @return uploads
	 */
	public function addUpload($entityType, $entity, $upload)
	{
		$em = $this->mr->getManagerForClass(get_class(new Upload()));
	
		$this->setUploadEntity($entityType, $entity, $upload);
		
		$upload->setSaveName($upload->clean($upload->getFile()->getClientOriginalName()));
		
		$em->persist($upload);
		$em->flush();
		
		$this->updateSaveName($upload);
	}
	
	/**
	 * Set entity to be uploaded
	 * @param String $entityType
	 * @param Object $entity
	 * @param Upload $upload
	 *
	 * @return uploads
	 */
	public function setUploadEntity($entityType, $entity, $upload)
	{
		switch ($entityType) {
		
		    case 'client':
					$upload->setUser($entity);
					break;
					
			default:
					break;
			
		}
		
		return $upload;
	}
	
	/**
	 * Update saved name in table
	 * @param Upload $upload
	 *
	 * @return uploads
	 */
	public function updateSaveName($upload)
	{
		$em = $this->mr->getManagerForClass(get_class(new Upload()));
	
		//UPDATE FILE NAME
		$upload->setSaveName($upload->newFileName);
		$em->persist($upload);
		$em->flush();
	}
	
	/**
	 * Delete Upload
	 *
	 * @param Request $request
	 * @param String $entityType
	 * @param Integer $entityId
	 * @param Integer $uploadId
	 *
	 * @return uploads
	 */
	public function deleteUpload($request, $entityType, $entityId, $uploadId)
	{
		$em = $this->mr->getManagerForClass(get_class(new Upload()));
	
		if ($request->getMethod() == "POST") {
		
			$upload = $em->getRepository('CoreBundle:Upload')->find($uploadId);
			if (count($upload) >0) {
			
				$upload->temp = $this->getUploadPath($entityType, $entityId)."/".$upload->getSaveName();
				
				$em->remove($upload);
				$em->flush();
				
				$this->container->get('session')->getFlashBag()->add('success', $this->container->get('translator')->trans('removeSuccess'));
			}
		}
	}
	
}
	