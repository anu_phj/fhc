<?php

/**
 * This serives will check acess of user
 */
namespace Swan\CoreBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Doctrine\Common\Persistence\ManagerRegistry;
use Swan\CoreBundle\Entity\User;
use Swan\CoreBundle\Entity\CostAllocation;

class UserAccess
{
	private $container;
	protected $mr;
	private $routeInfoArr;
	private $requestVars;
	
    public function __construct(Container $container, ManagerRegistry $mr)
    {
	    $this->container 	= $container;
	    $this->mr = $mr;
	    $this->routeInfoArr =  array(
            'clientId' => array(
                'entity' => 'user',
            )
        );
    }   
    
    /**
     * For add user access
     *
     * @param $request request object
     * @param $user user entity object
     *
     * @return false|null
     */    
    public function addUserAccess($request, $user) 
    {
        $costAllocationPermissions = $this->getCostAllocationPermissions($request->get('ids'), $request->get('costAllocations'));
    
        if(!empty($costAllocationPermissions)) {
        
            foreach($costAllocationPermissions as $costAllocationId => $values) {
            
                $costAllocation = $this->mr->getRepository('CoreBundle:CostAllocation')->find($costAllocationId);
                
                $this->addAcess($user, $costAllocation, $values);
            }
        }
    }
    
    private function  getCostAllocationPermissions($ids, $costAllocationsData) {

        $costAllocationPermissions = array();
		
        $permissions = $this->getPermissions($ids);
		
		if(!empty($costAllocationsData)) {
        
            foreach($costAllocationsData as $costAllocation) {
            
                if(isset($permissions[$costAllocation])) {
                
                    $costAllocationPermissions[$costAllocation] = $permissions[$costAllocation];
                }else{
                
                    $costAllocationPermissions[$costAllocation] =   array();
                }
            }
        }
		
		return $costAllocationPermissions;
    }
    
    private function getPermissions($ids) {
		$permissions = array();
		
        if(!empty($ids)) {
        
            foreach($ids as $id) {
            
                $val = explode("-",$id);
                $value = $val[0];
                $permissions[$value][] = $val[1];
            }
        }
		
		return $permissions;
    }
    
    /**
	 * @param $entity entity object
	 * @param $permissions array of permissions
	 * @return none
	 */
    public function addAcess($user, $entity, $permissions = array()) 
    { 
		$aclProvider =  $this->container->get('security.acl.provider');
		
		$objectIdentity = ObjectIdentity::fromDomainObject($entity);

		$securityIdentity = UserSecurityIdentity::fromAccount($user);
		
		try {
				$acl = $aclProvider->findAcl($objectIdentity);
				$aces = $acl->getObjectAces();
				
				foreach($aces as $index=>$ace)
				{
					if($ace->getSecurityIdentity() == $securityIdentity)
					{
					$acl->deleteObjectAce($index);
					}
				}
				
				$aclProvider->updateAcl($acl);
			
		} catch (\Exception $e) {					
				$acl = $aclProvider->createAcl($objectIdentity);
		}
		
		if(!empty($permissions)) {			 			
			$mask = $this->createMask($permissions);
			$acl->insertObjectAce($securityIdentity, $mask);
			$aclProvider->updateAcl($acl);
		}
	}
	
	 /**
	 * create mask 
	 * @param $permissions array	 
	 * @return generated mask	
	 */
	private function createMask($permissions) {
		
		$builder = new MaskBuilder();
		
		if(in_array("view", $permissions)) {
		
			$builder->add('view');
		}
		
		if(in_array("edit", $permissions)) {
		
			$builder->add('edit');
		}
		
		if(in_array("delete", $permissions)) {
		
			$builder->add('delete');
		}
		
		if(in_array("undelete", $permissions)) {
		
			$builder->add('undelete');
		}
			
		$mask = $builder->get();
		
		return $mask;
	}
	
	/**
	 * check Access
	 * @param $acess (eg edit,view)
	 * @param $entity object	 
	 * @return true or false
	 *
	 */
    public function checkAcess($acess, $entity) 
    {
		$authorizationChecker = $this->container->get('security.authorization_checker');		 
		
		if (false === $authorizationChecker->isGranted($acess, $entity)) {
        
            return false;
        }
        
        return true;
	}
	
	/**
	 * check Access
	 * @param $class object entity class	 
	 * @return array of permissions	 
	 */
	public function getAcess($user) 
 	{		
		$em	=	$this->mr->getManagerForClass(get_class(new User()));
			
		$userAccess = $em->getRepository('CoreBundle:User')->findUserAccess($user->getEmail());
		
		$access = array();
		
		if(!empty($userAccess)) {
			foreach($userAccess as $userAcces) {
				$objectIdentifier = $userAcces['object_identifier'];
				
				switch($userAcces['mask']) {
					case '5':
					$access[$objectIdentifier][] = 'view';
					$access[$objectIdentifier][] = 'edit';
					break;
					case '4':					
					$access[$objectIdentifier][] = 'edit';
					break;
					case '1':					
					$access[$objectIdentifier][] = 'view';
					break;
					default:
					$access[$objectIdentifier][]	="";
					break;
				}
			}
		}
		
		return $access;
	}
	
    /**
     * get user cost places
     *
     * @return array of cost places  
     */
    public function getLoggedInUserCostPlaces() 
    {
        $infoArr = array();
        $securityContext = $this->container->get('security.context')->getToken();
        $user = $securityContext->getUser();
        
        if ($user->getIsAdmin()!=true) {
        
            $userAccess = $this->getAcess($user);
            if (count($userAccess)>0) {
            
                foreach($userAccess as $costAllocationId => $userAcces) {
                    $infoArr[] = $costAllocationId; 
                }
            }
        }
        
        if ($user->getIsAdmin()!=true && count($infoArr)<=0) {
            $infoArr = array(0);
        }
        
        return $infoArr;
    }
    
    /**
     * get user cost places
     *
     * @return array of cost places  
     */
    public function getLoggedInUserParentChildCostPlaces() 
    {
        $costPlaces = $this->getLoggedInUserCostPlaces();
        $finalArr = $costPlaces;
        if (count($costPlaces)>0) {
        
            foreach($costPlaces as $costPlaceId) {
            
                $costPlaces = $this->container->get('CostAllocation')->costAllocationParentTree($costPlaceId);
                
                $finalArr = array_merge($finalArr, $costPlaces);
            }
            
            $finalArr = array_unique($finalArr);  
            sort($finalArr);
        }
        
        return $finalArr;
    }
    
    /**
     * get Route parameter name
     *
     * @return string
     */    
    public function getRouteParamName() 
    {
        $entityParameters= '';
        if(in_array("clientId", array_keys($this->requestVars))) {
        
            $entityParameters = 'clientId';
        } else if (in_array("entityId", array_keys($this->requestVars))) {
        
            $entityParam = $this->requestVars['entity']."Id";
            $this->requestVars[$entityParam] = $this->requestVars['entityId'];
            
            $entityParameters = $entityParam;
        }
      
        return $entityParameters;
    }
    
    /**
     * get entity by route name
     *
     * @return string
     */    
    public function getEntityFromRoute() 
    {
        $entity = false;
        
        $entityParameter = $this->getRouteParamName();

        if ($entityParameter!='') {
        
            $entityName = $this->routeInfoArr[$entityParameter]['entity'];

            $entity = $this->container->get('wbs_core')->getEntity($entityName, $this->requestVars[$entityParameter]);

	        if($entity->getCompany() && count($entity->getCompany())>0) {

                if ($this->container->get('session')->get('company')!=$entity->getCompany()->getId()) {
                    
                    return 'InvalidCompany';
                }
            }
        }
        
        return $entity;
    }
    
    /**
     * check if User is customer then mentioned Route allows to user.
     *
     * @return bool
     */
    public function checkCustomerRouteAccess($request) 
    {
        /*$customerRoutes = array('_core_access_denied', '_core_homepage', '_core_calculator_form','_core_note_list','_core_asset_name', '_core_goal_report_list');
        
        if(in_array($request->get('_route'), $customerRoutes)) {
            return true;
        }*/
        
        return true;
    }
    
    /**
     * check User Route Access
     *
     * @return bool
     */
    public function checkRouteUserAccess($request, $user) 
    {
        if ($user->getIsAdmin()!=1) {
            return $this->checkCustomerRouteAccess($request);
        }
        
        $requestParams = $request->attributes->all();
        if (count($requestParams)>0) {
            $this->requestVars = $requestParams['_route_params'];
            
            $entity = $this->getEntityFromRoute();
            
            $entityParameter = $this->getRouteParamName();
            if (count($entity)>0 && $entityParameter!='') { 
                
                
                if ($entity==='InvalidCompany') {
                    return false;
                }
                
                if ($user->getIsAdmin()==1) {
                    return true;
                }
                
                $costAllocation = $this->getCostAllocationFKName($entityParameter);
                if ($entity->$costAllocation()) {
                    
                    $costAllocations = $entity->$costAllocation();
                    
                    return $this->checkAccessWithCostAllocations($costAllocations);
                    
                } 
                
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * get cost allocation relation name
     *
     * @return string
     */    
    public function getCostAllocationFKName($entityParameter) 
    {
        return $this->routeInfoArr[$entityParameter] && $this->routeInfoArr[$entityParameter]['costAllocation'] ? $this->routeInfoArr[$entityParameter]['costAllocation'] : false;
    }
    
    /**
     * check user access
     *
     * @return bool
     */
    public function checkAccessWithCostAllocations($entityCostAllocations){
    
        if (count($entityCostAllocations)>0) {
            foreach($entityCostAllocations as $entityCostAllocation) {
                if ($this->checkAcess('EDIT', $entityCostAllocation->getCostAllocation())===true) {
                
                    return true;
                }
            }
            
        }
        return false;
    }
}