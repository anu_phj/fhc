<?php

namespace Swan\CoreBundle\Services\form;

class NoteForm
{
	public  function getFormFields($builder, $formType)
    {
        $class = $formType=='vehicleNote' ? 'vehicleNoteBody' : 'noteBody';
        $builder
            ->add('body','textarea', array(
				'label' => 'body',
                'attr'=>array(
                    'class'=> $class,
                    'rows' => '5',
                    'cols'=>'40',
                )
        ));
        
        return $builder;
    }	
}