<?php

namespace Swan\CoreBundle\Services;

use Doctrine\Common\Persistence\ManagerRegistry;
use Swan\CoreBundle\Entity\ClientAsset;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

use Swan\CoreBundle\Entity\User;
use Swan\CoreBundle\Entity\ClientGoal;

class ClientGoalService
{
	protected $mr;
	private $container;


	public function __construct(ManagerRegistry $mr, Container $container)
	{
		$this->mr = $mr;
		$this->container = $container;
	}
	
	/**
	 * Get all users
	 * @param $companyId
	 *
	 * @return user
	 */
	public function getRateAsPerUserAssumption($userId, $assumptionId)
	{
		$em	=	$this->mr->getManagerForClass(get_class(new User()));
		
		$userAssumption = $em->getRepository('CoreBundle:UserAssumption')->findBy(array('user' => $userId, 'assumption' => $assumptionId));
		
		if(count($userAssumption)>0) {
			
			$userAssumption = reset($userAssumption);
			return $userAssumption->getAnswer();
		}
		
		return '';
	}
	
	public function getFutureValue($principal, $rate, $period, $isMonthly)
	{
		if($isMonthly) {
			
			$futureValue = $period > 0 ? round($principal * pow(1 + ($rate/100)/12, $period), 2) : $principal;
		} else {
		
			$futureValue = $period > 0 ? round($principal * pow(1 + ($rate/100), $period), 2) : $principal;
		}

		return $futureValue;
	}
	
	public function calculateMonths($goalCreatedAt, $goalTargetYear, $goalMonth)
	{
		$date1 = $goalCreatedAt->format('Y-m-d');
		$date2 = '28-'.$goalMonth.'-'.$goalTargetYear;
		$date2 = date("Y-m-t", strtotime($date2));

		$months = $this->returnNumberOfMonths($date1, $date2);
		
		return $this->returnNumberOfMonths($date1, $date2);
	}
	
	public function returnNumberOfMonths($date1, $date2)
	{
		$date1 = date("Ymd", strtotime($date1));
		$date2 = date("Ymd", strtotime($date2));
		
		$dateObj1 = date_create($date1);
		$dateObj2 = date_create($date2);
		
		if($date1 > $date2) {
			
			$diff = date_diff($dateObj2,$dateObj1);
		} else {
			
			$diff = date_diff($dateObj1,$dateObj2);
		}
		
		$months = $diff->format('%y')*12 + $diff->format('%m');
		
		return $months;
	}
	
	public function calculateFutureValueForMonthlyInterest($principal, $rateOfInterest, $years, $numberOfQuartersInYear=12)
	{
		$period = 12 * $years;
		$rate = $rateOfInterest/100;
		$valueInPow = 1+($rate/$numberOfQuartersInYear);
		
		$sumValue = 0;
		
		for($i=$period; $i>=1; $i--) {
			$monthlyPeriod = $i/$period;
			$powerValue = $numberOfQuartersInYear * $monthlyPeriod;
			$finalAmount = $principal*(pow($valueInPow, $powerValue));
			$sumValue = $sumValue + $finalAmount;
		}

		return $sumValue;
	}
	
}