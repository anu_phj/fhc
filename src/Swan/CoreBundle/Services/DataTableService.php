<?php
/**
 * This serives will return settings value
 */
namespace Swan\CoreBundle\Services;

class DataTableService
{
   /**
	* Function to get datatables ordering and paging
	*
	* @param Object $request
	* @param Array $criteria
	* @param Array $aColumns
	*
	* @return criteria
	*/
    public function dataTableSort($request, $criteria, $aColumns=false) 
    {
        if ( isset($request['start']) && $request['length'] != -1 ) {
		
			$criteria['pagging']['length'] = intval($request['length']);
			$criteria['pagging']['start'] = intval($request['start']);
		}
	    
		if ( isset($request['order']) && count($request['order']) ) {
		
			for ( $i=0, $ien=count($request['order']) ; $i<$ien ; $i++ ) {
				// Convert the column index into the column data property
				$columnIdx = intval($request['order'][$i]['column']);
				$requestColumn = $request['columns'][$columnIdx];
				
				if ($requestColumn['orderable'] == 'true' && isset($aColumns[$columnIdx])) {
					
					if ($aColumns[$columnIdx]!='') {
                        $criteria['order'][] = array(
                                        'col' => $aColumns[$columnIdx], 
                                        'order' => $request['order'][$i]['dir']
                                    ); 
                    }
				} 
			}
		}

		return $criteria; 
    } 
    
    /**
     * return initialized array for json
     *
     * @return mixed
     */
    public function initJsonArray()
    {
        return array('display'=>0, 'count'=>0, 'data'=>'');
    }
    
    /**
     * return response for datatable
     *
     * @param Array $result
     *
     * @return array|mixed
     */
    public function returnDataTableResponse($result)
    {
        $response = array();
        
        $response['data'] = $result['data'];
        $response['recordsFiltered'] = $result['count'];
        $response['recordsTotal'] = $result['display'];         
        
        return $response;
    }
    
}