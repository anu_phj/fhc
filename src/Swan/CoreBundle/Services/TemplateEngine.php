<?php

namespace Swan\CoreBundle\Services;

use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

use Swan\CoreBundle\Services\TemplateEngineBase;
use Swan\CoreBundle\Entity\Allocation;

class TemplateEngine extends TemplateEngineBase
{
	protected $companyId;
	
	public $allocation; 

	public function __construct(ManagerRegistry $mr, Container $container)
	{
        $this->companyId = NULL;
        parent::__construct($mr, $container);
	}
	
    /**
     * Main Function for replace text variables to actual values.
     *
     * @param String $text 
     * @param Array $params
     *
     * @return string
     */                             	
	public function finalTemplateResult($text, $params)
    {
        if ($text!='') {
            
            $existFunctions = array();
            foreach($this->reservedVariables as $variable => $functionName) {
                
                if ($this->checkValidFunctionVar($variable, $functionName)) { 
                    
                    if (strpos($text, $variable) !== false) {
                        $existFunctions[$variable] = $functionName;
                    }
                }
            }
            
            return trim($this->replaceTemplateText($text, $existFunctions, $params));
        }
    }
    
    /**
     * Function for replace text variables to actual values.
     *
     * @param $text 
     * @param $existFunctions 
     * @param $params
     *
     * @return string
     */                                 
    private function replaceTemplateText($text, $existFunctions, $params){
        
        foreach($existFunctions as $var => $existFunction) {
			
			if (method_exists($this, $existFunction)) {
			
				$text = str_replace($var,$this->$existFunction($params), $text);
			}
        }
        
        return $text;
    }
    
    /**
     * check conditon for valid functions and vars
     *
     * @param $variable 
     * @param $functionName
     *
     * @return bool
     */                             
    private function checkValidFunctionVar($variable, $functionName)
    {
        if ($variable!='' && $functionName!='' && method_exists($this, $functionName)) return true;
        
        return false;
    }
    
    /**
     * Function to get contact person name
     */                                 
	private function contact_person_name($params)
	{
		return $this->getFieldFromObj($this->getContactPerson($params), 'getName');
	}
	
    /**
     * Function to get vehicle license plate
     */                                 
	private function vehicle_license_plate($params)
	{
		if (isset($params['vehicleId'])) {
		
			return $this->getEntityFields('vehicle', $params['vehicleId'], 'getLicensePlate');
		}
	}
	
    /**
     * Function to get vehicle chassis number
     */                                 
	private function vehicle_chassis_number($params)
	{
		if (isset($params['vehicleId'])) {
	
			return $this->getEntityFields('vehicle', $params['vehicleId'], 'getChassisNumber');
		}
	}
	
    /**
     * Function to get vehicle brand name
     */                                 
	private function vehicle_brand_name($params)
	{
		if (isset($params['vehicleId'])) {

			$brand = $this->getEntityFields('vehicle', $params['vehicleId'], 'getBrand');
			
			return $this->getFieldFromObj($brand, 'getName');
		}
	}
	
    /**
     * Function to get vehicle model
     */                                 
	private function vehicle_model($params)
	{
		if (isset($params['vehicleId'])) {
	
			return $this->getEntityFields('vehicle', $params['vehicleId'], 'getModel');
		}
	}
	
    /**
     * Function to get vehicle type
     */                                 
	private function vehicle_type($params)
	{
		if (isset($params['vehicleId'])) {
	
			return $this->getEntityFields('vehicle', $params['vehicleId'], 'getType');
		}
	}
	
    /**
     * Function to get vehicle fiscal value
     */                                 
	private function vehicle_fiscal_value($params)
	{
		if (isset($params['vehicleId'])) {
	
			$fiscalValue = $this->getEntityFields('vehicle', $params['vehicleId'], 'getFiscalValue');
		
			return '&euro;'.number_format($fiscalValue, 2, ',', '.');
		}
	}
	
    /**
     * Function to get vehicle income tax addition
     */                                 
	private function vehicle_income_tax_addition_name($params)
	{
		if (isset($params['vehicleId'])) {
	
			$incomeTaxAddition = $this->getEntityFields('vehicle', $params['vehicleId'], 'getIncomeTaxAddition');
			
			return $this->getFieldFromObj($incomeTaxAddition, 'getName');
		}
	}
	
    /**
     * Function to get old driver name according to allocation
     */                                 
	private function driver_old_name($params)
	{
		if (count($this->allocation) >0 && isset($params['vehicleId'])) {
			
			if(count($this->getPreviousAllocationDriver($params['vehicleId'], $this->allocation)) > 0) {
			
                return $this->getDriverFullName($this->getPreviousAllocationDriver($params['vehicleId'], $this->allocation));
			} else {
			
                return $this->getPoolOrGeneralUseName($this->getPreviousAllocation($params['vehicleId'], $this->allocation));
			}
		}
	}
	
    /**
     * Function to get old driver employeeId according to allocation
     */                                 
	private function driver_old_employeeId($params)
	{
		if (count($this->allocation) >0 && isset($params['vehicleId'])) {
			
			return $this->getFieldFromObj($this->getPreviousAllocationDriver($params['vehicleId'], $this->allocation), 'getEmployeeId');
		}
	}
	
    /**
     * Function to get old driver cost allocation
     */                                 
	private function driver_old_cost_allocation_name($params)
	{
		$costAllocation=false;
		
		if (count($this->allocation) >0 && isset($params['vehicleId'])) {
			
			$driver = $this->getPreviousAllocationDriver($params['vehicleId'], $this->allocation);
			if (count($driver) >0) {
				
				$costAllocation = $this->getFieldFromObj($this->getCostAllocation($driver->getId(), false), 'getLevelNames');
			} else if ($this->getPoolOrGeneralUseName($this->getPreviousAllocation($params['vehicleId'], $this->allocation))!='') {
				
				$costAllocation = $this->getFieldFromObj($this->getCostAllocation(false, $params['vehicleId']), 'getLevelNames');
			}
			
			$html = $this->returnHTMLForMergedCostAllocation($costAllocation);
		
			return $html;
		}
	}
	
    /**
     * Function to get old allocation end date
     */                                 
    private function driver_old_allocation_date_end($params)
    {
        if (count($this->allocation) >0 && isset($params['vehicleId'])) {
            
            $allocation = $this->getPreviousAllocation($params['vehicleId'], $this->allocation);
            if (count($allocation) >0) {
            
                $dateEnd = $this->getFieldFromObj($allocation, 'getDateEnd');
                
                return $dateEnd!='' ? $dateEnd->format('d-m-Y') : '';
            }
        }
    }
	
    /**
     * Function to get allocation date start
     */                                 
    private function allocation_date_start($params)
    {
		if (count($this->allocation) >0 && count($params) >0) {
			
			return $this->allocation->getDateStart()!='' ? $this->allocation->getDateStart()->format('d-m-Y') : '';
		}
    }
    
    /**
     * Function to get new driver name
     */                                 
    private function driver_new_name($params)
    {
        if (isset($params['driverId'])) {
		
			$driver = $this->driver($params['driverId']);
			if (count($driver) > 0) {
				
				return $this->getDriverFullName($driver);
			}
		} else {
		
            return $this->getPoolOrGeneralUseName($this->allocation);
		}
    }
    
    /**
     * Function to get driver employeeId
     */                                 
    private function driver_new_employeeId($params)
    {
		if (isset($params['driverId'])) {
    
			return $this->getEntityFields('driver', $params['driverId'], 'getEmployeeId');
		}
    }
    
    /**
     * Function to get new driver gender
     */                                 
    private function driver_new_gender($params)
    {
		if (isset($params['driverId'])) {
    
			$gender = $this->getEntityFields('driver', $params['driverId'], 'getGender');
		
			if ($gender!='') {
			
				return $gender == 'female' ? 'Vrouw' : 'Man';
			}
		}
    }
    
    /**
     * Function to get new driver street
     */                                 
    private function driver_new_street($params)
    {
		if (isset($params['driverId'])) {
    
			return $this->getEntityFields('driver', $params['driverId'], 'getStreet');
		}
    }
    
    /**
     * Function to get new driver housenumber
     */                                 
    private function driver_new_house_number($params)
    {
		if (isset($params['driverId'])) {
    
			return $this->getEntityFields('driver', $params['driverId'], 'getHouseNumber');
		}
    }
    
    /**
     * Function to get new driver housenumber suffix
     */                                 
    private function driver_new_house_number_suffix($params)
    {
		if (isset($params['driverId'])) {
    
			return $this->getEntityFields('driver', $params['driverId'], 'getHouseNumberSuffix');
		}
    }
    
    /**
     * Function to get new driver postal
     */                                 
    private function driver_new_postal_code($params)
    {
		if (isset($params['driverId'])) {
    
			return $this->getEntityFields('driver', $params['driverId'], 'getPostalCode');
		}
    }
    
    /**
     * Function to get new driver city
     */                                 
    private function driver_new_city($params)
    {
		if (isset($params['driverId'])) {
    
			return $this->getEntityFields('driver', $params['driverId'], 'getCity');
		}
    }
    
    /**
     * Function to get new driver phone number
     */                                 
    private function driver_new_phone_number($params)
    {
		if (isset($params['driverId'])) {
    
			return $this->getEntityFields('driver', $params['driverId'], 'getPhoneNumber');
		}
    }
    
    /**
     * Function to get new driver mobile number
     */                                 
    private function driver_new_mobile_number($params)
    {
		if (isset($params['driverId'])) {
    
			return $this->getEntityFields('driver', $params['driverId'], 'getMobileNumber');
		}
    }

    /**
     * Function to get new driver email
     */                                 
    private function driver_new_email($params)
    {
		if (isset($params['driverId'])) {
    
			return $this->getEntityFields('driver', $params['driverId'], 'getEmail');
		}
    }
    
    /**
     * Function to get new driver cost allocation at time of new cost allocation start
     */                                 
    private function driver_new_cost_allocation_name($params)
    {
		$costAllocation = false;
		
		if (isset($params['driverId'])) {
			
			$costAllocation = $this->getFieldFromObj($this->getCostAllocation($params['driverId']), 'getLevelNames');
			
		} else if ($this->getPoolOrGeneralUseName($this->allocation)!='') {
				
			$costAllocation = $this->getFieldFromObj($this->getCostAllocation(false, $params['vehicleId']), 'getLevelNames');
		}
		
		$html = $this->returnHTMLForMergedCostAllocation($costAllocation);
		
		return $html;
    }
    
}