<?php

namespace Swan\CoreBundle\Services;

use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Swan\CoreBundle\Entity\CompanySetting;


class CompanySettingService
{
	protected $mr;
	private $container;

	public function __construct(ManagerRegistry $mr, Container $container)
	{
		$this->mr = $mr;
		$this->container = $container;
	}
	
	/**
     * get record from company setting by companyId
     *
     * @param companyId $companyId
     *
     * @return company setting entity object
     */     	
	public function getCompanySettingByCompanyId($companyId) 
	{
		$em = $this->mr->getManagerForClass(get_class(new CompanySetting()));
		
		$companySetting = new CompanySetting();
		
		$arrCompanySettingData = $em->getRepository('CoreBundle:CompanySetting')->findByCompanyId($companyId);
		
		if(!empty($arrCompanySettingData)) {
			
			$arrCompanySettingData = reset($arrCompanySettingData);
			$companySetting = $em->getRepository('CoreBundle:CompanySetting')->find($arrCompanySettingData->getId());
		}
		
		return $companySetting;
	}

	public function listCompanySettingEmails($companyId)
	{
		$listEmails = array();
		$companySetting = $this->getCompanySettingByCompanyId($companyId);

		if($companySetting->getCompanyHrEmail() != '') {

			$listEmails['companyHrEmail~'.$companySetting->getCompanyHrEmail()] = $this->container->get('translator')->trans('companyHrEmail').' <'.$companySetting->getCompanyHrEmail().'>';
		}

		if($companySetting->getCompanyVehicleSystemEmail() != '') {

			$listEmails['companyVehicleSystemEmail~'.$companySetting->getCompanyVehicleSystemEmail()] = $this->container->get('translator')->trans('companyVehicleSystemEmail').' <'.$companySetting->getCompanyVehicleSystemEmail().'>';
		}

		return $listEmails;
	}
	
	/**
	 * Update logo name in table
	 *
	 * @param companySetting $companySetting
	 *
	 * @return none
	 */
	public function updateLogoName($companySetting)
	{
		$em = $this->mr->getManagerForClass(get_class(new CompanySetting()));
	
		//UPDATE FILE NAME
		$companySetting->setLogo($companySetting->newFileName);
		$em->persist($companySetting);
		$em->flush();
	} 
}
	