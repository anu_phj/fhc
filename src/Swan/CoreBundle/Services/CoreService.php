<?php

namespace Swan\CoreBundle\Services;

use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Swan\CoreBundle\Entity\Company;
use Swan\CoreBundle\Entity\Vehicle;
use Swan\CoreBundle\Entity\Driver;
use Swan\CoreBundle\Entity\LeaseContract;
use Swan\CoreBundle\Entity\LeaseContractLimited;
use Symfony\Component\Form\FormInterface;
use Swan\CoreBundle\Entity\User;
use Swan\CoreBundle\Form\LeaseContractType;
use Swan\CoreBundle\Form\LeaseContractLimitedType;
use Swan\CoreBundle\Form\VehicleImportType;
use Swan\CoreBundle\Entity\CostAllocation;
use Swan\CoreBundle\Entity\CostType;
use Swan\CoreBundle\Entity\FuelContract;
use Swan\CoreBundle\Form\FuelContractType;
use Swan\CoreBundle\Entity\OnRentContract;
use Swan\CoreBundle\Form\OnRentContractType;

class CoreService
{
	protected $mr;
	private $container;
	protected $formFactory;
	protected $wbsEntities;

	public function __construct(ManagerRegistry $mr, Container $container, FormFactoryInterface $formFactory)
	{
		$this->mr = $mr;
		$this->container = $container;
		$this->formFactory = $formFactory;
		$this->wbsEntities = array(
            'company' => array(
                'entity' => new Company(),
                'entityName' => 'CoreBundle:Company',
            ),
			'user' => array(
				'entity' => new User(),
				'entityName' => 'CoreBundle:User',
			)
        );
	}
	
    /**
     * validate all form required fields of entity.
     *
     * @param Entity $entity The entity
     *
     * @return driver edit form
     */                    	
	public function validate($entity, $group=false) 
	{
		$validator = $this->container->get('validator');
		
		if ($group!== false) {
		
			$errors = $validator->validate($entity, $group);
		} else {
		
			$errors = $validator->validate($entity);
		}
		
		return $errors;
	}
	
    /**
     * validate all required fields of cost allocation.
     *
     * @param Object the request object
     *
     * @return array
     */                     
    public function costAllocationValidate($request) 
    {
        $errors = array();
        if ($request->get('costAllocationId')<=0) {
            $errors[] = $this->container->get('translator')->trans('pleaseSelectCostAllocation');
        } 
        if ($request->get('dateActive')=='')  {
            $errors[] = $this->container->get('translator')->trans('pleaseSelectStartDateForCostAllocation');
        }
        
        return $errors;
    }	

    /**
     * set company entity.
     *
     * @param Entity $entity The entity
     *
     * @return entity
     */                    	
	public function setCompany($entity) 
	{
		$companyId = $this->container->get('session')->get('company');
		if ($companyId>0 && count($entity)>0) {
		
			$em	=	$this->mr->getManagerForClass(get_class(new Company()));
			
		    $company = $em->getRepository('CoreBundle:Company')->find($companyId);
		    if (count($company)>0) {
		        $entity->setCompany($company);
		    }
		}
		
		return $entity;
	}
	
    /**
     * set vehicle entity.
     *
     * @param Entity $entity The entity
     *
     * @return entity
     */                     
    public function setVehicle($entity, $vehicleId) 
    {
        if (count($entity)>0 && $vehicleId>0) {
        
            $vehicle = $this->getEntity('vehicle', $vehicleId);
            
            if (count($vehicle)>0) {
                $entity->setVehicle($vehicle);
            }
        }
        
        return $entity;
    }	

	/**
	 * Creates an array of form errors
	 *
	 * @param \Symfony\Component\Form\Form $form
	 * @return array Form fields errors
	 */
	public function getFormErrors(\Symfony\Component\Form\Form $form)
	{
		$errors = array();

		foreach ($form->getErrors() as $key => $error) {
			$errors[] = $error->getMessage();
		}

		foreach ($form->all() as $child) {
			if (!$child->isValid()) {
				$errors[$child->getName()] = $this->getFormErrors($child);
			}
		}

		return $errors;
	}

	/**
	 * Set delete flag true
	 *
	 * @param Request $request
	 * @param Object $entity
	 * @param Integer $entityId
	 *
	 * @return Object Entity
	 */
	public function softDeleteRecord($request, $entity, $entityId)
	{
		$em	= $this->mr->getManagerForClass(get_class(new Company()));
		
		$class = $em->getClassMetadata(get_class($entity))->getName();
	
		if ($request->getMethod() == 'POST') {
		
			$entity = $em->getRepository($class)->find($entityId);
			if (count($entity) >0) {
			
				$entity->setDeleted(true);
				
				$em->persist($entity);
				$em->flush();
				
				$this->container->get('session')->getFlashBag()->add('success', $this->container->get('translator')->trans('removeSuccess'));
			}
		}
		
		return false;
	}
	
	/**
	 * Hard delete (Remove entity)
	 *
	 * @param Request $request
	 * @param Object $entity
	 * @param Integer $entityId
	 *
	 * @return Object Entity
	 */
	public function hardDeleteRecord($request, $entity, $entityId, $hideFlash = false)
	{
		if ($request->getMethod() == 'POST') {

			if($this->removeEntity($entity, $entityId) && $hideFlash===true) {

				$this->container->get('session')->getFlashBag()->add('success', $this->container->get('translator')->trans('removeSuccess'));
			}
		}

		return false;
	}

	/**
	 * Checks if start date is smaller or equal to end date
	 *
	 * @param $startDate
	 * @param $endDate
	 * @return bool
	 */
	public function compareDates($startDate, $endDate)
	{
		if(strtotime($startDate) < strtotime($endDate)) {

			return true;
		}

		return false;
	}
	
    /**
     * Get Entity name
     *
     * @param string $entityName The entityType for entity
     * @param integer $entityId The entity id
     *
     * @return string The generated URL
     */
    public function getEntity($entityName, $entityId=0)
    {
        if ($entityId>0) {

            $doctrineEntity = $this->_getEntityValue($entityName, 'entityName');
            if ($doctrineEntity!==false) {
            
                return $this->getEntityObj($doctrineEntity, $entityId);
            }
        } else {

            return  $this->_getEntityValue($entityName, 'entity');
        }
    }

	/**
	 * Get value for entiy or entity name
	 *
	 * @param $entityName
	 * @param $type
	 * @return bool
	 */
	private function _getEntityValue($entityName, $type)
	{
		return isset($this->wbsEntities[$entityName][$type]) ? $this->wbsEntities[$entityName][$type] : false;
	}
    
    /**
     * Get Entity name
     *
     * @param $doctrineEntity The entityType for entity
     * @param $entityId The entity id
     *
     * @return string The generated URL
     */
    public function getEntityObj($doctrineEntity, $entityId)
    {
        $em = $this->mr->getManagerForClass(get_class(new Company()));

        $entity = $em->getRepository($doctrineEntity)->find($entityId);
        if (!$entity) {
            throw new NotFoundHttpException('Unable to find entity');
        }
        
        return $entity;
    }
    
    /**
     * Get FormType name
     *
     * @param string $entityName The entityType for entity
     *
     * @return object formType
     */
    public function getFormTypeName($entityName)
    {

        $entityTypeArr = array(
			'lease' => new LeaseContractType(),
			'vehicleImport' => new VehicleImportType(),
			'leasecontractlimited' => new LeaseContractLimitedType(),
			'fuel' => new FuelContractType(),
			'onrent' => new OnRentContractType(),
		);
		        
        return isset($entityTypeArr[$entityName]) ? $entityTypeArr[$entityName] : new LeaseContractType();
    }    
    
    /**
     * Creates form of all entities
     *
     * @param String $formType 
     * @param Object $entity 
     * @param String $url 
     *
     * @return \Symfony\Component\Form\Form The form
     */
    public function createEntityForm($formType, $entity, $url='', $submitBtnLabel='Save', $otherInfo=false)
    {
        $submitBtnLabel = $this->container->get('translator')->trans($submitBtnLabel);
        
        $formOtherData = array(
            'action' => $url!='' ? $url : '',
            'method' => 'POST',
        );
        
        if ($otherInfo !== false) {
        
			$formOtherData['otherInfo'] = $otherInfo;
        }
        
        $form = $this->formFactory->create($this->getFormTypeName($formType), $entity, $formOtherData);
        
        $form->add('submit', 'submit', array(
                                'label' => $submitBtnLabel,
                                'attr' => array('title'=> $submitBtnLabel,),
                                )
                );

        return $form;
    }    
    
    /**
     * validate all form required fields of entity.
     *
     * @param Entity $entity The entity
     *
     * @return entity
     */
    public function setUser($entity, $userId)
    {
	    $em =   $this->mr->getManagerForClass(get_class(new User()));

        if (!empty($userId) && count($entity)>0) {

            $user = $em->getRepository('CoreBundle:User')->find($userId);

            if (count($user)>0) {

                $entity->setUser($user);
            }
        }

        return $entity;
    }

	/**
	 * Provides date in required format by using entity
	 *
	 * @param $entity
	 * @param $fieldName
	 * @param $defaultValue
	 * @param $format
	 * @return mixed
	 */
	public function getDateFieldValue($entity, $fieldName, $defaultValue, $format)
	{
		$date = $defaultValue;
		$fieldName = 'get'.ucfirst($fieldName);

		if($entity->$fieldName()!='') {

			$date = $entity->$fieldName()->format($format);
		}

		return $date;
	}

	/**
	 * Saves the entity
	 *
	 * @param $entity
	 * @param bool $returnValue
	 * @return bool
	 */
	public function saveRecord($entity, $returnValue = false, $flushRecord = true)
	{
		$em	= $this->mr->getManagerForClass(get_class(new Company()));

		try {

			$em->persist($entity);
			if($flushRecord===true) {
				$em->flush();
			}

			$returnValue = (!empty($returnValue)) ? $entity : true;
		} catch(\Doctrine\ORM\ORMException $e) {
			//TODO Log records that are not updated while executing script
		} catch(\Exception $e) {
			//TODO Log records that are not updated while executing script
		}

		return $returnValue;
	}

	/**
	 * Converts messages array to string
	 *
	 * @param $headMessage
	 * @param $messages
	 * @return string
	 */
	public function messagesArrayToHtml($headMessage, $messages)
	{
		$messagesHtml = '<span>'.$headMessage.'</span>';

		if(!empty($messages)) {

			$messagesHtml .= '<ul class="listMessages">';
			foreach($messages as $message) {

				$messagesHtml .= '<li>'.$message.'</li>';
			}

			$messagesHtml .= '</ul>';
		}

		return $messagesHtml;
	}

	/**
	 * Remove entity
	 *
	 * @param Object $entity
	 * @param Integer $entityId
	 *
	 * @return Object Entity
	 */
	public function removeEntity($entity, $entityId, $notFlush = null)
	{
		$em	= $this->mr->getManagerForClass(get_class(new Company()));

		$class = $em->getClassMetadata(get_class($entity))->getName();

		$entity = $em->getRepository($class)->find($entityId);
		if (count($entity)>0) {

			$em->remove($entity);

			if(empty($notFlush)) {

				$em->flush();
			}

			return true;
		}

		return false;
	}

	/**
	 * Convert excel content to array format
	 *
	 * @param string $inputFile Path to excel file, set whether excel first row are headers
	 * @param $inputFile
	 * @param $headingRow
	 * @param $dataStartRow
	 * @param $columnKeys
	 * @return array
	 */
	public function excelToArray($inputFile, $headingRow = 1, $dataStartRow = 2, $columnKeys = false)
	{
		//Get worksheet and built array with first row as header
		$objWorksheet = $this->getWorksheetObject($inputFile);

		$highestRow = $objWorksheet->getHighestRow();
		$highestColumn = $objWorksheet->getHighestColumn();
		$headingsArray = $objWorksheet->rangeToArray('A'.$headingRow.':'.$highestColumn.$headingRow, null, true, true, true);

		$headingsArray = reset($headingsArray);

		$r = -1;
		$namedDataArray = array();
		for($row = $dataStartRow; $row <= $highestRow; ++$row) {

			$dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row, null, true, true, true);
			if($this->_validateExcelRow($dataRow, $row)) {

				++$r;
				foreach($headingsArray as $columnKey => $columnHeading) {

					$namedDataArrayColumnKeys = (!empty($columnKeys)) ? $columnKey : $columnHeading;
					$namedDataArray[$r][$namedDataArrayColumnKeys] = $dataRow[$row][$columnKey];
				}
			}
		}
		
		return $namedDataArray;
	}

	private function _validateExcelRow($dataRow, $row) {

		if((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '') ||
			(isset($dataRow[$row]['B'])) && ($dataRow[$row]['B'] > '') ||
			(isset($dataRow[$row]['C'])) && ($dataRow[$row]['C'] > '')) {

			return true;
		}

		return false;
	}

	/**
	 * @param $inputFile
	 * @return \PHPExcel_Worksheet
	 * @throws \PHPExcel_Reader_Exception
	 */
	private function getWorksheetObject($inputFile)
	{
		$inputFileType = \PHPExcel_IOFactory::identify($inputFile); //Identify the type of $inputFile
		$objReader = \PHPExcel_IOFactory::createReader($inputFileType);
		$objReader->setReadDataOnly(true); //Set read type to read cell data only
		$objPHPExcel = $objReader->load($inputFile);

		//Get worksheet and built array with first row as header
		return $objPHPExcel->getActiveSheet();
	}

	/**
	 * Convert csv to array format
	 *
	 * @param string $inputFile Path to excel file, set whether excel first row are headers
	 * @param $inputFile	 
	 * @param $dataStartRow	 
	 * @return array
	 */
	public function csvToArray($inputFile, $dataStartRow = 2)
	{
		$namedDataArray = array();
		
		if (($handle = fopen($inputFile, "r")) !== FALSE) {
		
			$row = 0;			
			
			while (($data = fgetcsv($handle, 0, ";")) !== FALSE) {
				
				if($row >= $dataStartRow) {
				
					foreach($data as $key=>$val) {
						
						$alpha = $this->num2alpha($key);
						$namedDataArray[$row][$alpha] = $val;						
					}
				}
				
				$row++; 
			}
			
			fclose($handle);
		}
		
		return $namedDataArray;
	}
	
	/**
	 * convert num2alpha
	 *
	 * @param $number
	 * @return string alpha
	 */
	public function num2alpha($number) {
		$r = '';
		for ($i = 1; $number >= 0 && $i < 10; $i++) {
			$r = chr(0x41 + ($number % pow(26, $i) / pow(26, $i - 1))) . $r;
			$number -= pow(26, $i);
		} 
		
		return $r;
	}	
	
    /**
     * get cost allocation array
     *
     * @param array $updateRecord
     
     * @return array
     */
    public function getCostAllocationResult($updateRecord)
    {
        $result = array();
        if(isset($updateRecord['status']) && $updateRecord['status']=='success') {
            
            $breadcrumbLinks = $this->getCostAllocationBreadcrumbLinks($updateRecord);

            $result[] = array("result" => "Success", "msg" => $updateRecord['message'], 'breadcrumbLinks' => $breadcrumbLinks);
        } else {

            $result[] = array("result" => "error", "msg" => $updateRecord['message']);
        }
        
        return $result;
    }
    
    /**
     * get cost allocation breadcrumbLinks
     *
     * @param array $updateRecord
     
     * @return array
     */
    public function getCostAllocationBreadcrumbLinks($updateRecord)
    {
	    if (isset($updateRecord['entity']) && $updateRecord['entity']=='vehicle') {
		    $breadcrumbLinks = $this->container->get('CostAllocation')->getEntityCostAllocationList($updateRecord['vehicleId'], 'vehicle');
	    } else if (isset($updateRecord['entity']) && $updateRecord['entity']=='importDriver'){
		    $breadcrumbLinks = $this->container->get('ImportDrivers')->getDriverCostAllocationList($updateRecord['id']);
	    } else {
		    $breadcrumbLinks = $this->container->get('ImportVehicles')->getVehicleCostAllocationList($updateRecord['id']);
	    }

        return $breadcrumbLinks;
    }
    
    /**
     * get message label
     *
     * @param String $message
     
     * @return String
     */
    public function getMessageLabel($message) 
    {
        if ($message=='new') {
            $label = 'addSuccess';
        } elseif ($message=='edit') {
            $label = 'updateSuccess';
        } else {
            $label = 'addSuccess';
        }
        
        return $label;
    }

	/**
	 * Removes File
	 *
	 * @param $fileName
	 */
	public function removeFile($fileName)
	{
		if(file_exists($fileName)) {

			unlink($fileName);
		}
	}

	/**
	 * Get date value in required format
	 *
	 * @param $value
	 * @return \DateTime|string
	 */
	public function getDateValue($value)
	{
		return (!empty($value)) ? new \DateTime($value) : NULL;
	}
	
    /**
     * validate unique cost allocation.
     *
     * @param Object the request object
     *
     * @return array $errors
     */                     
    public function validateUniqueCostAllocation($request) 
    {
        $errors = array(); 
        $data = $request->get('swan_corebundle_costallocation');
        $parentId = trim($request->request->get('parentId'));
        $costAllocationId = $request->get('costAllocationId');
        $companyId = $this->container->get('session')->get('company'); 
        
        $em	= $this->mr->getManagerForClass(get_class(new CostAllocation()));
        
        $costAllocation = $em->getRepository('CoreBundle:CostAllocation')->checkIfRecordExists($companyId, $data['name'], $parentId, $costAllocationId);  
        
        if (!empty($costAllocation)) {
        
            $errors[] = $this->container->get('translator')->trans('costAllocationAlreadyExists');
        } 
        
        return $errors;
    }
    
    /**
     * Prepares processed data response
     *
     * @param $entityNotUpdated
     * @param $uploadArr
     * @return array
     */
    public function fileDataResponseFinal($entityNotUpdated, $uploadArr = array()) 
    {
        $response = array();
        if (count($uploadArr)!=count(array_filter($uploadArr))) {
            
            $response['status'] = 'success';
            $response['message'] = count(array_filter($uploadArr))  ." of ". count($uploadArr)." ".$this->container->get('translator')->trans('recordsImportedSuccessfully');
        } elseif($entityNotUpdated) {

            $response['status'] = 'error';
            $response['message'] = $this->container->get('translator')->trans('unableToImportDataCompletely');
        } else {

            $response['status'] = 'success';
            $response['message'] = $this->container->get('translator')->trans('recordsImportedSuccessfully');
        }

        return $response;
    }
    
    /**
     * For check form valid or not
     *
     * @param $form
     * @param $errors
     * @param $costAllocationErrors
     *
     * @return bool
     */
    public function formIsValid($form, $errors, $costAllocationErrors) 
    {
        if ($form->isValid() && count($errors)<=0 && count($costAllocationErrors)<=0) return true;
        
        return false;
    }
	
	/**
     * validate file type
     *
     * @param $fileType file type
     * @return true or false
     */    
     public function validateFileType($fileType) {
     
		$allowedFileMimeTypes = array('xls','xlsx','csv');
		
		if (in_array($fileType, $allowedFileMimeTypes)) {
			
			return true;
		}
		
		return false;		
     }
     
    /**
     * Check if uploaded file is mathced with selected supplier
     *
     * @param $fileData
     * @param $mappingProfileId
     *
     * @return array
     */
    public function matchUploadedFile($fileData, $mappingProfileId, $importType=false)
    {
        if (count($fileData)>0 && $mappingProfileId>0) {

            $em = $this->mr->getManagerForClass(get_class(new Company()));
            
            $mappingProfileEntity = $importType=='driver' ? 'CoreBundle:MappingProfile' : 'CoreBundle:SupplierImportMappingProfile';
            
            $mappingProfile = $em->getRepository($mappingProfileEntity)->find($mappingProfileId);
            $key = array_search(max($fileData), $fileData);


	        if($this->_validateFieldsCount($mappingProfile->getTotalColumn(), $fileData[$key], $importType)) {

		        return true;
	        }
            
            return false;
        }
        
        return true;
    }

	/**
	 * Compare counts of file columns with mapping profile columns
	 *
	 * @param $mappingProfileColumns
	 * @param $fileColumns
	 * @param $importType
	 * @return bool
	 */
	private function _validateFieldsCount($mappingProfileColumns, $fileColumns, $importType)
	{
		if($importType == 'driver') {

			return $this->_validateDriverFieldsCount($mappingProfileColumns, $fileColumns);
		} else {

			if ($mappingProfileColumns==count($fileColumns)) {

				return true;
			}
		}

		return false;
	}

	/**
	 * Compare counts of Driver import file columns with mapping profile columns
	 *
	 * @param $mappingProfileColumns
	 * @param $fileColumns
	 * @return bool
	 */
	private function _validateDriverFieldsCount($mappingProfileColumns, $fileColumns)
	{
		if (count($fileColumns) >= $mappingProfileColumns) {

			return true;
		}

		return false;
	}
    
    /**
     * get costType for a company
     *
     *
     * @return array
     */
    public function getAllCostTypes()
    {
        $em = $this->mr->getManagerForClass(get_class(new CostType()));
        
        $costTypes = $em->getRepository('CoreBundle:CostType')->findAll();  
        
        return $costTypes;
    }
    
    /**
     * set CostType entity.
     *
     * @param Entity $entity The entity
     *
     * @return entity
     */                     
    public function setCostType($entity, $costTypeId, $oldCostTypeId = 0) 
    {
        if ($costTypeId>0 && count($entity)>0 && $costTypeId!=$oldCostTypeId) {
        
            $em =   $this->mr->getManagerForClass(get_class(new CostType()));
            
            $costType = $em->getRepository('CoreBundle:CostType')->find($costTypeId);
            if (count($costType)>0) {
                $entity->setCostType($costType);
            }
        }
        
        return $entity;
    }

	/**
	 * Calculates the number of days difference between dates
	 *
	 * @param $startDate
	 * @param $endDate
	 * @return float|int
	 */
	public function daysDifference($startDate, $endDate)
	{
		$date1 = date_create($startDate);
		$date2 = date_create($endDate);

		$diff = date_diff($date1,$date2);

		return $diff->days + 1;
	}

	/**
	 * Updated import entity with updated columns
	 *
	 * @param $entity
	 * @return mixed
	 */
	public function importEntityUpdatedColumns($entity)
	{
		$em = $this->mr->getManagerForClass(get_class(new Company()));

		$uow = $em->getUnitOfWork();
		$uow->computeChangeSets();
		$changeSet = $uow->getEntityChangeSet($entity);
		$changeSet = $this->_checkboxesFieldUpdates($changeSet);

		$existingUpdatedColumnsArray = array();
		$existingUpdatedColumns = $entity->getUpdatedColumns();
		if(!empty($existingUpdatedColumns)) {

			$existingUpdatedColumnsArray = unserialize($existingUpdatedColumns);
		}

		if(!empty($changeSet)) {

			foreach($changeSet as $changeFieldKey=>$changeField) {

				$existingUpdatedColumnsArray[$changeFieldKey] = $this->getFieldName($changeFieldKey);
			}
		}

		unset($existingUpdatedColumnsArray['vehicleId']);
		unset($existingUpdatedColumnsArray['driverId']);

		$updatedColumns = '';
		if(!empty($existingUpdatedColumnsArray)) {

			$updatedColumns = serialize($existingUpdatedColumnsArray);
		}

		return $updatedColumns;
	}

    /**
     * check if checkboxes fields are changed
     *
     * @param $changeSet
     *
     * @return mixed
     */	
	private function _checkboxesFieldUpdates($changeSet)
	{
		$fields = array('hasCarKit', 'hasAdvertisement', 'hasNavigation');

		foreach($fields as $field) {

			if(empty($changeSet[$field][1])) {

				unset($changeSet[$field]);
			}
		}

		return $changeSet;
	}

	/**
	 * Get Field name to show
	 *
	 * @param $columnKey
	 * @return string
	 */
	public function getFieldName($columnKey)
	{
		$fields = array(
			//Vehicle Fields
			'licensePlate' => $this->container->get('translator')->trans('licensePlate'),
			'brand' => $this->container->get('translator')->trans('brand'),
			'model' => $this->container->get('translator')->trans('model'),
			'type' => $this->container->get('translator')->trans('type'),
			'fuelType' => $this->container->get('translator')->trans('fuelType'),
			'co2' => $this->container->get('translator')->trans('co2'),
			'incomeTaxAddition' => $this->container->get('translator')->trans('incomeTaxAddition'),
			'fiscalValue' => $this->container->get('translator')->trans('fiscalValue'),
			'energyLabel' => $this->container->get('translator')->trans('energyLabel'),
			'apkExpiryDate' => $this->container->get('translator')->trans('apkExpiryDate'),
			'contractNumber' => $this->container->get('translator')->trans('contractNumber'),
			'chassisNumber' => $this->container->get('translator')->trans('chassisNumber'),
			'vehicleType' => $this->container->get('translator')->trans('vehicleType'),
			'dateFirstAdmittance' => $this->container->get('translator')->trans('dateFirstAdmittance'),
			'incomeTaxAdditionValidTill' => $this->container->get('translator')->trans('incomeTaxAdditionValidTill'),
			'dateLastMaintenance' => $this->container->get('translator')->trans('dateLastMaintenance'),
			'setupType' => $this->container->get('translator')->trans('setupType'),
			'hasAdvertisement' => $this->container->get('translator')->trans('hasAdvertisement'),
			'hasCarKit' => $this->container->get('translator')->trans('hasCarKit'),
			'hasNavigation' => $this->container->get('translator')->trans('hasNavigation'),
			'fuelTankSize' => $this->container->get('translator')->trans('fuelTankSize'),
			'availableForApplication' => $this->container->get('translator')->trans('availableForApplication'),
			'forGeneralUse' => $this->container->get('translator')->trans('forGeneralUse'),
			'status' => $this->container->get('translator')->trans('status'),
			//Driver Fields
			'employeeId' => $this->container->get('translator')->trans('employee id'),
			'bsn' => $this->container->get('translator')->trans('bsn'),
			'familyName' => $this->container->get('translator')->trans('family name'),
			'firstName' => $this->container->get('translator')->trans('first name'),
			'familyNamePrefix' => $this->container->get('translator')->trans('family name prefix'),
			'initials' => $this->container->get('translator')->trans('initials'),
			'gender' => $this->container->get('translator')->trans('gender'),
			'dateInService' => $this->container->get('translator')->trans('date in service'),
			'dateOutOfService' => $this->container->get('translator')->trans('date out of service'),
			'dateEligibleForVehicle' => $this->container->get('translator')->trans('date eligible for vehicle'),
			'dateIneligibleForVehicle' => $this->container->get('translator')->trans('date ineligible for vehicle'),
			'employeeRole' => $this->container->get('translator')->trans('employee role'),
			'street' => $this->container->get('translator')->trans('street'),
			'houseNumber' => $this->container->get('translator')->trans('house number'),
			'houseNumberSuffix' => $this->container->get('translator')->trans('house number suffix'),
			'postalCode' => $this->container->get('translator')->trans('postal code'),
			'city' => $this->container->get('translator')->trans('city'),
			'phoneNumber' => $this->container->get('translator')->trans('phone number'),
			'mobileNumber' => $this->container->get('translator')->trans('mobile number'),
			'email' => $this->container->get('translator')->trans('email'),
			'costAllocationId' => $this->container->get('translator')->trans('Cost Place'),
			'country' => $this->container->get('translator')->trans('country'),
			'leaseCategory' => $this->container->get('translator')->trans('Lease category'),
			'mutationDate' => $this->container->get('translator')->trans('mutationDate'),
		);

		return isset($fields[$columnKey]) ? $fields[$columnKey] : '';
	}

    /**
     * Get formated date
     *
     * @param $date
     *
     * @return date
     */	
	public function formedDate($date) 
	{
        if ($date!='' && $date!='0000-00-00') {
            return $date->format('d-m-Y');
        }
	}
	
    /**
     * Get formated value
     *
     * @param $value
     *
     * @return String
     */ 
    public function getBinaryValueToStringValue($value) 
    {
        if ($value==1 || $value==true) {
            $stringValue = $this->container->get('translator')->trans('yes');
        } else {
            $stringValue = $this->container->get('translator')->trans('no');
        }
        
        return $stringValue;
    }	
    
     /**
     * get entity methods
     *
     * @param Object $entity
     *
     * @return array
     */     
    public function getClassMethods($entity)
    {
        $em = $this->mr->getManagerForClass(get_class(new Company()));
            
        $className = $em->getClassMetadata(get_class($entity))->getName();
        
        $serviceMethods = get_class_methods($className);
        
        return $serviceMethods;
    }

	/**
	 * Prepare entity details with Existing entity details
	 *
	 * @param $listFields
	 * @param $entityDetails
	 * @param $existingEntity
	 * @return mixed
	 */
	public function processEntityDataWithExistingEntity($listFields, $entityDetails, $existingEntity)
	{
		$dateFormatFields = array('getValueInDateFormat', 'getValueInDateFormatCsv', 'getValueInDateFormatExcel', 'getIncomeTaxAdditionValidTill');
		$dateFields = array('dateFirstAdmittance', 'incomeTaxAdditionValidTill', 'apkExpiryDate', 'dateLastMaintenance', 'dateInService', 'dateOutOfService', 'dateEligibleForVehicle', 'dateIneligibleForVehicle');

		foreach($listFields as $listField) {

			$fieldName = 'get'.ucfirst($listField);
			$fieldValue = $existingEntity->$fieldName();

			$entityDetails = $this->_prepareEntityDetailKeys($entityDetails, $listField, $dateFields, $fieldValue);

			if(isset($entityDetails[$listField]) && empty($entityDetails[$listField]['value'])) {

				if(!empty($fieldValue) && in_array($entityDetails[$listField]['type'], $dateFormatFields)) {

					$fieldValue = $fieldValue->format('Y-m-d');
				}

				$entityDetails[$listField]['value'] = $fieldValue;
			}
		}

		return $entityDetails;
	}

	/**
	 * Prepare entity details with keys if field value is not empty
	 *
	 * @param $entityDetails
	 * @param $listField
	 * @param $dateFields
	 * @param $fieldValue
	 * @return mixed
	 */
	private function _prepareEntityDetailKeys($entityDetails, $listField, $dateFields, $fieldValue)
	{
		if(!isset($entityDetails[$listField]) && !empty($fieldValue)) {

			$type = '';
			if(in_array($listField, $dateFields)) {

				$type = 'getValueInDateFormatExcel';
			}

			$entityDetails[$listField] = array(
				'type' => $type,
				'value' => '',
			);
		}

		return $entityDetails;
	}

	/**
	 * ist import table fields for getting values from existing entity
	 *
	 * @param $entity
	 * @param $entityType
	 * @param $ignoreFields
	 * @return array
	 */
	public function listImportTableFields($entity, $entityType, $ignoreFields)
	{
		$em	= $this->mr->getManagerForClass(get_class($entity));

		$listFields = $em->getClassMetadata('CoreBundle:'.$entityType)->getFieldNames();
		$relationalFields = $em->getClassMetadata('CoreBundle:'.$entityType)->getAssociationNames();
		$listFields = array_merge($listFields, $relationalFields);

		if(!empty($listFields)) {

			foreach($listFields as $listFieldKey=>$listField) {

				if(in_array($listField, $ignoreFields)) {

					unset($listFields[$listFieldKey]);
				}
			}
		}

		return $listFields;
	}
    
    /**
     * get entity foreign table field data
     *
     * @param Object $entity
     * @param String $attrName
     *
     * @return string
     */        
    public function getEntityAttributeName($entity, $attrName, $fieldName = 'name')
    {
        $attrName = 'get'.$attrName;
        $fieldName = 'get'.ucfirst($fieldName);
        return $entity->$attrName() ? $entity->$attrName()->$fieldName() : '';
    }
    
    /**
     * get valid date
     *
     * @param $date
     * @param $format
     *
     * @return date
     */            
    public function validDate($date, $format='Y-m-d')
    {
        $finalDate = '';
        if (!empty($date)) {
            if (is_object($date) || is_array($date)) {
            
                $finalDate = $date->format($format);
            } else {
            
                $finalDate = date($format, strtotime($date));
            }
        }
        
        return $finalDate;
    }
    
    /**
     * get date betweens
     *
     * @param $from
     * @param $to
     * @param $date
     *
     * @return bool
     */    
    public function dateIsBetween($from, $to, $date = 'now') 
    {
        $date = is_int($date) ? $date : strtotime($date); // convert non timestamps
        $from = is_int($from) ? $from : strtotime($from); // ..
        $to = is_int($to) ? $to : strtotime($to);         // ..
        
        return ($date >= $from) && ($date <= $to); 
    }
}