<?php

namespace Swan\CoreBundle\Services;

use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

use Swan\CoreBundle\Entity\Brand;
use Swan\CoreBundle\Form\BrandType;
use Swan\CoreBundle\Entity\Supplier;
use Swan\CoreBundle\Form\SupplierType;
use Swan\CoreBundle\Entity\VehicleType;
use Swan\CoreBundle\Form\VehicleTypeType;
use Swan\CoreBundle\Entity\IncomeTaxAddition;
use Swan\CoreBundle\Form\IncomeTaxAdditionType;
use Swan\CoreBundle\Entity\EnergyLabel;
use Swan\CoreBundle\Form\EnergyLabelType;
use Swan\CoreBundle\Entity\DocumentType;
use Swan\CoreBundle\Form\DocumentTypeType;
use Swan\CoreBundle\Entity\FuelType;
use Swan\CoreBundle\Form\FuelTypeType;
use Swan\CoreBundle\Entity\Country;
use Swan\CoreBundle\Form\CountryType;
use Swan\CoreBundle\Entity\LeaseCategory;
use Swan\CoreBundle\Form\LeaseCategoryType;
use Swan\CoreBundle\Entity\ContractState;
use Swan\CoreBundle\Form\ContractStateType;
use Swan\CoreBundle\Entity\CalculationType;
use Swan\CoreBundle\Form\CalculationTypeType;
use Swan\CoreBundle\Entity\CostType;
use Swan\CoreBundle\Form\CostTypeType;
use Swan\CoreBundle\Entity\JournalCode;
use Swan\CoreBundle\Form\JournalCodeType;
use Swan\CoreBundle\Entity\OrderStatus;
use Swan\CoreBundle\Form\OrderStatusType;
use Swan\CoreBundle\Entity\OwnershipType;
use Swan\CoreBundle\Form\OwnershipTypeType;

class StaticValueEntityService
{
	protected $mr;
	private $container;
    private $staticValueEntities;
    
	public function __construct(ManagerRegistry $mr, Container $container)
	{
		$this->mr = $mr;
		$this->container = $container;
		
		$this->staticValueEntities = array(
			'brand' => array(
				'entity' => new Brand(),
				'formType' => new BrandType()
			),
			'supplier' => array(
				'entity' => new Supplier(),
				'formType' => new SupplierType($this->mr->getManagerForClass(get_class(new Supplier())),
					$this->container->get('session')->get('company')
				)
			),
			'vehicleType' => array(
				'entity' => new VehicleType(),
				'formType' => new VehicleTypeType()
			),
			'incomeTaxAddition' => array(
				'entity' => new IncomeTaxAddition(),
				'formType' => new IncomeTaxAdditionType()
			),
			'energyLabel' => array(
				'entity' => new EnergyLabel(),
				'formType' => new EnergyLabelType()
			),
			'documentType' => array(
				'entity' => new DocumentType(),
				'formType' => new DocumentTypeType($this->mr->getManagerForClass(get_class(new DocumentType())),
					$this->container->get('session')->get('company')
				)
			),
			'fuelType' => array(
				'entity' => new FuelType(),
				'formType' => new FuelTypeType()
			),
			'country' => array(
				'entity' => new Country(),
				'formType' => new CountryType()
			),
			'leaseCategory' => array(
				'entity' => new LeaseCategory(),
				'formType' => new LeaseCategoryType()
			),
			'contractState' => array(
				'entity' => new ContractState(),
				'formType' => new ContractStateType()
			),
			'calculationType' => array(
				'entity' => new CalculationType(),
				'formType' => new CalculationTypeType()
			),
			'costType' => array(
				'entity' => new CostType(),
				'formType' => new CostTypeType()
			),
			'journalCode' => array(
				'entity' => new JournalCode(),
				'formType' => new JournalCodeType($this->mr->getManagerForClass(get_class(new JournalCode())),
					$this->container->get('session')->get('company')
				)
			),
			'orderStatus' => array(
				'entity' => new OrderStatus(),
				'formType' => new OrderStatusType($this->mr->getManagerForClass(get_class(new OrderStatus())),
					$this->container->get('session')->get('company')
				)
			),
			'ownershipType' => array(
				'entity' => new OwnershipType(),
				'formType' => new OwnershipTypeType()
			),
			
		);
	}
	
	/**
	 * Gets form Type based upon form type
	 *
	 * @param String $formType 
	 *
	 * @return form Type
	 */
	public function getFormType($formType) 
	{
		return isset($this->staticValueEntities[$formType]['formType']) ? $this->staticValueEntities[$formType]['formType'] : false;
	}
	
	/**
	 * Gets entity based upon form type
	 *
	 * @param String $formType
	 *
	 * @return entity
	 */
	public function getEntity($formType) 
	{
		return isset($this->staticValueEntities[$formType]['entity']) ? $this->staticValueEntities[$formType]['entity'] : false;
	}
	
	/**
	 * Gets entity values based upon form type
	 *
	 * @param String $formType
	 * @param Integer $staticValueId 
	 *
	 * @return entity
	 */
	public function getEditEntity($formType, $staticValueId) 
	{
		$em = $this->mr->getManagerForClass(get_class(new Brand()));
		
		$class = $em->getClassMetadata(get_class($this->getEntity($formType)))->getName();
		
		$entity = $em->getRepository($class)->find($staticValueId);
		
		return $entity;
	}
	
    /**
     * Gets entity values based upon entity
     *
     * @param String $entityName
     *
     * @return entity
     */
    public function getFkEntityValue($entityObj, $entityName) 
    {
        switch ($entityName) {

            case 'brand':
                    $entity = $entityObj->getBrand()->getId();
                    break;
                        
            case 'vehicleType':
                    $entity = $entityObj->getVehicleType()->getId();
                    break;
                        
            case 'incomeTaxAddition':
                    $entity = $entityObj->getIncomeTaxAddition()->getId();
                    break;
                        
            case 'energyLabel':
                    $entity = $entityObj->getEnergyLabel()->getId();
                    break;
                        
            case 'fuelType':
                    $entity = $entityObj->getFuelType()->getId();
                    break;
                    
            case 'documentType':
                    $entity = $entityObj->getDocumentType()->getId();
                    break;
                        
            default:
                $entity = $entityObj->getId();
                break;
        }

        return $entity;
    }
    
    /**
     * Gets static values entities
     *
     * @param String $entityName
     *
     * @return entities
     */
    public function getUsedStaticValuesEntities($entityName) 
    {
        $em = $this->mr->getManagerForClass(get_class(new Brand()));
    
        switch ($entityName) {
            
            case 'supplier':
                    $entities = $em->getRepository('CoreBundle:Supplier')->findAllUsedSupliers($this->container->get('session')->get('company'));
                    break;
                    
            case 'documentType':
                    $entities = $em->getRepository('CoreBundle:Upload')->findAllDocumentTypeByUploads();
                    break;
            
            case 'country':
                    $entities = $em->getRepository('CoreBundle:Country')->findAllUsedCountries();
                    break;
            
            case 'leaseCategory':
                    $entities = $em->getRepository('CoreBundle:LeaseCategory')->findAllUsedLeaseCategories();
                    break;
            
            case 'contractState':
                    $entities = $em->getRepository('CoreBundle:ContractState')->findAllUsedContractStates();
                    break;
                    
            case 'calculationType':
                    $entities = $em->getRepository('CoreBundle:CalculationType')->findAllUsedCalculationTypes();
                    break;
                    
            case 'costType':
                    $entities = $em->getRepository('CoreBundle:CostType')->findAllUsedCostTypes();
                    break;
                    
            case 'journalCode':
                    $entities = $em->getRepository('CoreBundle:JournalCode')->findAllUsedJournalCode();
                    break;
                    
            case 'orderStatus':
                    $entities = $em->getRepository('CoreBundle:OrderStatus')->findAllUsedOrderStatus($this->container->get('session')->get('company'));
                    break;
            
            case 'ownershipType':
                    $entities = $em->getRepository('CoreBundle:OwnershipType')->findAllUsedOwnershipType();
                    break;  
          
            default:
                    $entities = $em->getRepository('CoreBundle:Vehicle')->findAllVehicleByEntity($entityName);
                    break;
        }
        
        return $entities;
    }
	
}