<?php

namespace Swan\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Swan\CoreBundle\Services\form\VehicleForm;
use Swan\CoreBundle\Form\EventListener\MoneyFormatListener;

class VehicleImportType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $vehicleFrm = new VehicleForm();
        $vehicleFrm->getFormFields($builder);
	    $builder->addEventSubscriber(new MoneyFormatListener());
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Swan\CoreBundle\Entity\VehicleImport'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'swan_corebundle_vehicleimport';
    }
}
