<?php

namespace Swan\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Swan\CoreBundle\Services\form\NoteForm;

class VehicleNoteType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $noteFrm = new NoteForm(); 
        $noteFrm->getFormFields($builder, 'vehicleNote');
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Swan\CoreBundle\Entity\VehicleNote'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'swan_corebundle_vehicle_note';
    }
}
