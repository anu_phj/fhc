<?php

namespace Swan\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{
	private $type;

	public function __construct($userType, $type = NULL)
	{
		$this->type = $type;
		$this->userType = $userType;
	}

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
	        ->add('name', 'text', array('label'=> 'First Name'))
	        ->add('lastName', 'text', array('label'=> 'Last Name', 'required'=>false))
	        ->add('dob', 'datePicker', array('label'=> 'DOB', 'attr' => array('placeholder' => 'dd-mm-yyyy')))
	        ->add('gender', 'choice', array('choices'=>array('male'=>'Male', 'female'=>'Female'),
		        'empty_value'   =>  'Select',
		        'label' => 'Gender'
	        ));
			
			if($this->userType == 'client') {
				$builder->add('occupation', 'choice', array('choices'=>array(
						'Business'=>'Business',
						'Service'=>'Service',
						'Housewife'=>'Housewife',
						'Student'=>'Student',
						'Retired'=>'Retired'
					),
					'empty_value'   =>  'Select',
					'label' => 'Occupation'
				));
			}
			
	        $builder->add('maritalStatus', 'choice', array('choices'=>array(
			        'Single'=>'Single',
			        'Married'=>'Married'
		        ),
		        'empty_value'   =>  'Select',
		        'label' => 'Marital Status'
	        ))
	        ->add('pincode', 'text', array('label'=> 'Pin Code', 'required'=>false))
	        ->add('email', 'text', array('label'=> 'Email'))
	        ->add('mobile', 'text', array('label'=> 'Mobile'))
        ;

	    if($this->type=='edit') {

		    $builder
			    ->add('address', 'textarea', array(
				        'label'=> 'Address', 'required'=>false,
					    'attr'=>array(
						    'rows' => '5',
						    'cols'=>'40',
					    )
				    )
			    )
			    ->add('country', 'entity', array('class' => 'CoreBundle:Country', 'empty_value'=> 'Select', 'label'=>'country', 'required'=>false))
			    ->add('state', 'choice', array('choices'=>array(),
				    'empty_value'   =>  'Select',
				    'label' => 'State',
				    'required'=>false
			    ))
			    ->add('city', 'choice', array('choices'=>array(),
				    'empty_value'   =>  'Select',
				    'label' => 'City',
				    'required'=>false
			    ))
		    ;
	    }

	    $builder->add('password', 'repeated', array(
			    'type' => 'password',
			    'invalid_message' => 'passwordMismatch',
			    'required' => true,
			    'first_options' => array('label' => 'Password'),
			    'second_options' => array('label' => 'Confirm Password'),
		    )
	    );
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Swan\CoreBundle\Entity\User',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'swan_corebundle_user';
    }
}