<?php
namespace Swan\CoreBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class MoneyFormatListener implements EventSubscriberInterface
{

    public static function getSubscribedEvents()
    {
        // Tells the dispatcher that you want to listen on the form.pre_submit_data
        // event and that the onPreSubmitData method should be called.
       
        return array(FormEvents::PRE_SUBMIT => 'onPreSubmitData');
    }
	
	public function onPreSubmitData(FormEvent $event)
    {
		$data = $event->getData();
        $form = $event->getForm();
        
        foreach($data as $key => $value) {
        
			if ($value!='' && $form[$key]->getConfig()->getType()->getName() == 'money') {
				
				$value = str_replace(".","",$value);
				$finalValue = str_replace(",",".",$value);

				$choppedNumber	=	round($finalValue, 2);
				$formatValue = str_replace(".", ",", $choppedNumber);
				
				$data[$key] = $formatValue;
				$event->setData($data);
			}
        }
    }
}