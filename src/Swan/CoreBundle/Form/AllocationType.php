<?php

namespace Swan\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormInterface;

class AllocationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateStart', 'datePicker', array('label'=> 'dateStart', 'attr' => array('placeholder' => 'dd-mm-yyyy')))
            ->add('dateEnd', 'datePicker', array('label'=> 'dateEnd', 'attr' => array('placeholder' => 'dd-mm-yyyy'), 'required' => false))
            ->add('privateUse', 'checkbox', array('label'=> 'privateUse', 'required' => false))
            ->add('fixedPersonalContribution', 'money', array('label'=> 'fixedPersonalContribution', 'grouping' => true))
            ->add('budgetOverdraw', 'money', array('label'=> 'budgetOverdraw', 'grouping' => true))
            ->add('eb1', 'money', array('label'=> 'eb1', 'grouping' => true))
            ->add('eb2', 'money', array('label'=> 'eb2', 'grouping' => true))
	        ->add('type', 'hidden', array('label'=> 'type'))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
	    $resolver->setDefaults(array(

		    'validation_groups' => function (FormInterface $form) {

			    $data = $form->getData();

			    if (in_array($data->getType(), array(1, 2))) {

				    return array('inGeneralOrPool');
			    }

			    return array('Default');
		    },
		    'data_class' => 'Swan\CoreBundle\Entity\Allocation'
	    ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'swan_corebundle_allocation';
    }
}
