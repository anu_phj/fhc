<?php

namespace Swan\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class UploadType extends AbstractType
{
	protected $companyId;
	
	public function __construct($companyId) {
	
		$this->companyId = $companyId;
	}
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		$companyId = $this->companyId;

        $builder
			->add('documentType', 'entity', array(
					'class' => 'CoreBundle:DocumentType', 
					'query_builder' => function(EntityRepository $er) use ($options, $companyId)
					{
						$query = $er->createQueryBuilder('d')
								->where('d.company = :companyId')->setParameter('companyId', $companyId)
						;
						
						return $query;
					},									
					'label'=> 'Document Type', 'empty_value' => 'select',
					'attr' => array(
						'class' => 'form-control selectWidth'
					)
			))
            ->add('file', 'file', array('label'=> 'Upload'));
            
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Swan\CoreBundle\Entity\Upload'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'swan_corebundle_upload';
    }
}
