<?php

namespace Swan\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DriverType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('employeeId', 'text', array('label'=> 'employee id'))
            ->add('bsn', 'text', array('label'=> 'Bsn', 'required'=>false))
            ->add('familyName', 'text', array('label'=> 'family name'))
            ->add('firstName', 'text', array('label'=> 'first name','required'=>false))
            ->add('familyNamePrefix', 'text', array('label'=> 'family name prefix','required'=>false))
            ->add('initials', 'text', array('required'=>false, 'label' => 'initials'))
            ->add('gender', 'choice', array('choices'=>array('male'=>'Male', 'female'=>'Female'),
                                                'required'  =>false,
                                                'empty_value'   =>  'select',
                                                'label' => 'gender'
                                        ))
            ->add('status', 'choice', array('choices'=>array('0'=>'In service', '1'=>'Out of service'),
                                                'required'  =>false,
                                                'empty_value'   =>  'select',
                                                'label' => 'status'
                                        ))
            ->add('dateInService', 'datePicker', array('label'=> 'date in service', 'required'=>false, 'attr' => array('placeholder' => 'dd-mm-yyyy')))
            ->add('dateOutOfService', 'datePicker', array('required'  =>false, 'label'=> 'date out of service', 'attr' => array('placeholder' => 'dd-mm-yyyy')))
            ->add('dateEligibleForVehicle', 'datePicker', array('label'=> 'date eligible for vehicle', 'required'=>false, 'attr' => array('placeholder' => 'dd-mm-yyyy')))
            ->add('dateIneligibleForVehicle', 'datePicker', array('required'  =>false, 'label'=> 'date ineligible for vehicle', 'attr' => array('placeholder' => 'dd-mm-yyyy')))
            ->add('employeeRole', 'text', array('label'=> 'employee role', 'required'=>false))
            ->add('leaseCategory', 'entity', array('class' => 'CoreBundle:LeaseCategory', 'empty_value'=> 'select', 'label'=>'Lease category', 'required'=>false))
            ->add('street', 'text', array('label'=> 'street', 'required'=>false))
            ->add('houseNumber', 'text', array('label'=> 'House number', 'required'=>false))
            ->add('houseNumberSuffix', 'text', array('label'=> 'House number suffix', 'required'=>false))
            ->add('postalCode', 'text', array('label'=> 'Postal code', 'required'=>false))
            ->add('city', 'text',array('label'=> 'city', 'required'=>false))
            ->add('country', 'entity', array('class' => 'CoreBundle:Country', 'empty_value'=> 'select', 'label'=>'country', 'required'=>false))
            ->add('phoneNumber', 'text', array('label'=> 'phone number', 'required'=>false))
            ->add('mobileNumber', 'text', array('label'=> 'mobile number', 'required'=>false))
            ->add('email', 'text', array('label'=> 'Email', 'required'=>true))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Swan\CoreBundle\Entity\Driver'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'swan_corebundle_driver';
    }
}
