<?php

namespace Swan\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class LeaseContractType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('contractNumber', 'text', array('label'=> 'contractNumber'))
            ->add('startDate', 'datePicker', array('label'=> 'startDate', 'attr' => array('placeholder' => 'dd-mm-yyyy')))
            ->add('duration', 'integer', array('label'=> 'duration'))
            ->add('yearlyKm', 'integer', array('label'=> 'yearlyKm'))
            ->add('catalogPrice', 'money', array('label'=> 'catalogPrice', 'grouping' => true))
            ->add('additionalInvestment', 'money', array('label'=> 'additionalInvestment', 'grouping' => true))
            ->add('bpm', 'money', array('label'=> 'bpm', 'grouping' => true))
            ->add('deliveryCosts', 'money', array('label'=> 'deliveryCosts', 'grouping' => true))
            ->add('discount', 'money', array('label'=> 'discount', 'grouping' => true))
            ->add('salvageValue', 'money', array('label'=> 'salvageValue', 'grouping' => true))
            ->add('monthlyDepreciation', 'money', array('label'=> 'monthlyDepreciation', 'grouping' => true))
            ->add('interest', 'text', array('label'=> 'interest'))
            ->add('hsb', 'money', array('label'=> 'hsb', 'grouping' => true))
            ->add('insurance', 'money', array('label'=> 'insurance', 'grouping' => true))
            ->add('maintenance', 'money', array('label'=> 'maintenance', 'grouping' => true))
            ->add('tires', 'money', array('label'=> 'tires', 'grouping' => true))
            ->add('winterTires', 'money', array('label'=> 'winterTires', 'grouping' => true))
            ->add('emergencySupport', 'money', array('label'=> 'emergencySupport', 'grouping' => true))
            ->add('replacementVehicle', 'money', array('label'=> 'replacementVehicle', 'grouping' => true))
            ->add('administrationCosts', 'money', array('label'=> 'administrationCosts', 'grouping' => true))
            ->add('managementFee', 'money', array('label'=> 'managementFee', 'grouping' => true))
            ->add('otherCosts', 'money', array('label'=> 'otherCosts', 'grouping' => true))
            ->add('fuelCard', 'money', array('label'=> 'fuelCard', 'grouping' => true))
            ->add('costFuelTerm', 'money', array('label'=> 'costFuelTerm', 'grouping' => true))
            ->add('costLeaseTerm', 'money', array('label'=> 'costLeaseTerm', 'grouping' => true))
            ->add('monthlyInterest', 'money', array('label'=> 'monthlyInterest', 'grouping' => true))
            ->add('dateTurnIn', 'datePicker', array('label'=> 'dateTurnIn', 'required' => false, 'attr' => array('placeholder' => 'dd-mm-yyyy')))
            ->add('supplier', 'entity', array(
                    'label'=> 'supplier', 
                    'class' => 'CoreBundle:Supplier', 
                    'query_builder' => function(EntityRepository $er) use ($options)
                    {
                        $query = $er->createQueryBuilder('s')
                                ->where('s.company = :companyId')->setParameter('companyId', $options['otherInfo']['companyId'])
                        ;
                        
                        return $query;
                    },                                  
                    'empty_value'=> 'select'
                ))
            ->add('contractState', 'entity', array('label'=> 'contractState', 'class' => 'CoreBundle:ContractState', 'empty_value'=> 'select'))
            ->add('calculationType', 'entity', array('label'=> 'calculationType', 'class' => 'CoreBundle:CalculationType', 'empty_value'=> 'select'))
            ->add('costAllocation', 'entity', array(
                    'label'=> 'costAllocation', 
                    'class' => 'CoreBundle:CostAllocation', 
                    'query_builder' => function(EntityRepository $er) use ($options)
                    {
                        $query = $er->createQueryBuilder('c')
                                ->where('c.parentId IS NULL')
                        ;
                        
                        return $query;
                    },                                  
                    'empty_value'=> 'select'
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Swan\CoreBundle\Entity\LeaseContract',
            'otherInfo' => false
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'swan_corebundle_leasecontract';
    }
}
