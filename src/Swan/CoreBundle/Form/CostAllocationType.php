<?php

namespace Swan\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class CostAllocationType extends AbstractType
{
	protected $companyId;
	
	public function __construct($companyId) {
	
		$this->companyId = $companyId;
	}

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		$companyId = $this->companyId;
    		
        $builder
            ->add('name', 'text', array('label'=> 'Name'))
            ->add('description', 'textarea', array('label'=> 'Description'))
            ->add('department', 'text', array('label'=> 'department', 'required'=> false))
            ->add('contactPerson', 'entity', 
					array('class' => 'CoreBundle:ContactPerson', 
						'label'=> 'contactPerson', 
						'empty_value'=> 'select', 
						'required'=> false,
						'query_builder' => function(EntityRepository $er) use ($options, $companyId)
						{
							$query = $er->createQueryBuilder('c')
									->where('c.company = :companyId')->setParameter('companyId', $companyId)
							;
							
							return $query;
						},									
					)
            )
            ->add('inactive', 'checkbox', array('label'=> 'inactive', 'required' => false))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Swan\CoreBundle\Entity\CostAllocation'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'swan_corebundle_costallocation';
    }
}
