<?php

namespace Swan\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Validator\Constraints\NotBlank;

class VehicleOrderType extends AbstractType
{
	protected $companyId;
	
	public function __construct($companyId) {
	
		$this->companyId = $companyId;
	}

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		$companyId = $this->companyId;
    
        $builder
            ->add('dealer', 'text', array('label'=> 'dealer'))
            ->add('contractNumber', 'text', array('label'=> 'contractNumber'))
            
            ->add('orderStatus', 'entity', array('label'=> 'orderStatus',
					'class' => 'CoreBundle:OrderStatus', 
					'query_builder' => function(EntityRepository $er) use ($options, $companyId)
					{
						$query = $er->createQueryBuilder('os')
								->where('os.company = :companyId')->setParameter('companyId', $companyId)
						;
						
						return $query;
					},									
					'empty_value' => 'select',
					'attr' => array(
						'class' => 'form-control'
					)
            ))
            
            ->add('supplier', 'entity', array('label'=> 'supplier', 'class' => 'CoreBundle:Supplier',
					'query_builder' => function(EntityRepository $er) use ($options, $companyId)
					{
						$query = $er->createQueryBuilder('s')
								->where('s.company = :companyId')->setParameter('companyId', $companyId)
						;
						
						return $query;
					},									
					'empty_value' => 'select',
					'attr' => array(
						'class' => 'form-control'
					)
            ))
            
            ->add('notes', 'textarea', array('label'=> 'Notes', 'required' => false))
            ->add('deliveryDate', 'datePicker', array('label'=> 'deliveryDate', 'attr' => array('placeholder' => 'dd-mm-yyyy')))
            ->add('replacementVehicle', 'hidden', array('label'=> 'replacementVehicle', 'mapped'=>false ))
            ->add('newVehicle', 'hidden', array('label'=> 'newVehicle', 'mapped'=>false, 'required' => true,
												 'constraints' => array(
													new NotBlank(array('message' => 'newVehicleNotBlank')),
												),
            ))
            ->add('driver', 'hidden', array('label'=> 'Driver', 'required' => false,'mapped'=>false))
            ->add('costAllocation', 'hidden', array('label'=> 'costAllocation', 'required' => false,'mapped'=>false))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Swan\CoreBundle\Entity\VehicleOrder'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'swan_corebundle_vehicleorder';
    }
}
