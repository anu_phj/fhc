<?php

namespace Swan\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Swan\CoreBundle\Services\form\ContractForm;


class FuelContractType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $contractFrm = new ContractForm(); 
        $contractFrm->getFormFields($builder, $options, 'fuelContract');
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Swan\CoreBundle\Entity\FuelContract',
            'otherInfo' => false
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'swan_corebundle_fuelcontract';
    }
}
