<?php

namespace Swan\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class JournalCodeType extends AbstractType
{
	protected $em;
	protected $companyId;

	public function __construct($em, $companyId)
	{
        $this->em = $em;
        $this->companyId = $companyId;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
			->add('company', 'entity', array(
						'class' => 'CoreBundle:Company', 
						'data' => $this->em->getReference("CoreBundle:Company", $this->companyId)
					)
				)
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Swan\CoreBundle\Entity\JournalCode'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'swan_corebundle_journalcode';
    }
}
