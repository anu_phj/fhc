<?php

namespace Swan\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class AllocationMutationEmailType extends AbstractType
{
	private $sendMailTo;
	private $fieldValues;

	public function __construct($sendMailTo, $fieldValues){
		$this->sendMailTo = $sendMailTo;
		$this->fieldValues = $fieldValues;
	}

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add("sendMailTo", "choice", array(
				'label' => 'sendMailTo',
				'choices' => $this->sendMailTo,
				'data' => (!empty($this->fieldValues['sendMailTo'])) ? $this->fieldValues['sendMailTo'] : array(),
				'multiple' => true,
				'expanded' => true,
				'empty_value'	=>	false
			))
			->add('sendOthers', 'text', array('label'=> 'others','required'=>false, 'attr' => array('value' => $this->fieldValues['sendOthers'])))
			->add('subject', 'text', array('label'=> 'subject', 'attr' => array('value' => $this->fieldValues['subject'])))
			->add('body','textarea', array(
				'label' => 'body',
				'data' => $this->fieldValues['body'],
				'attr'=>array(
					'rows' => '5',
					'cols'=>'40'
				)
			))
			->add('submit', 'submit', array('label' => $this->fieldValues['submitBtnLabel']))
			->getForm();
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'corebundle_allocation_mutation_email';
	}
}
