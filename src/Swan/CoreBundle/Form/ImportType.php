<?php

namespace Swan\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ImportType extends AbstractType
{
	private $mappingProfile;
	private $journalDate;

	public function __construct($mappingProfile, $journalDate = NULL){
		$this->mappingProfile = $mappingProfile;
		$this->journalDate = $journalDate;
	}

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('file', 'file', array('mapped' => false, 'label'=> 'File'))
			->add("mappingProfileId", "choice", array(
				'label' => 'Select Mapping Profile',
				'choices' => $this->mappingProfile,
				'empty_value'	=>	false
			))
			->add('submit', 'submit', array('label' => 'Next'))
			->getForm();

		if(!empty($this->journalDate)) {

			$builder->add('journalDate', 'datePicker', array('label'=> 'journalDate', 'attr' => array('placeholder' => 'dd-mm-yyyy', 'value' => $this->journalDate, 'readonly' => true)));
		} else {

			$builder->add('ignoreEmptyFields', 'checkbox', array('label'=> 'ignoreEmptyFields', 'required' => false));
		}
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'corebundle_import';
	}
}
