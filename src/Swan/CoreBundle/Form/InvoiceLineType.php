<?php

namespace Swan\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class InvoiceLineType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('invoiceCostAllocation')
            ->add('invoiceNo')
            ->add('invoiceDate')
            ->add('invoiceLineStartDate')
            ->add('invoiceLineEndDate')
            ->add('amountExVat')
            ->add('amountIncVat')
            ->add('vatPercentage')
            ->add('journalDate')
            ->add('originalDescription')
            ->add('status')
            ->add('checkedByUserId')
            ->add('journalEntry')
            ->add('file')
            //->add('company')
            //->add('costAllocation')
            //->add('costType')
            //->add('vehicle')
            //->add('driver')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Swan\CoreBundle\Entity\InvoiceLine'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'swan_corebundle_invoiceline';
    }
}
