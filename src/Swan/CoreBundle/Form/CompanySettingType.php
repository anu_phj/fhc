<?php

namespace Swan\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CompanySettingType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder                        
			->add('allocationMethod', 'choice', array('choices'=>array( 1 =>'Allocate current active cost place', 2 =>'Allocate proportionally'),
									'required'  =>true,
									'empty_value'   =>  'select',
									'label' => 'allocationMethod'
							))
	        ->add('companyHrEmail', 'text', array('label'=> 'companyHrEmail', 'required'=>true))
	        ->add('companyVehicleSystemEmail', 'text', array('label'=> 'companyVehicleSystemEmail', 'required'=>true))
	        ->add('fromEmail', 'text', array('label'=> 'fromEmail', 'required'=>true))
	        ->add('fromEmailPassword', 'password', array('label'=> 'password', 'required'=>true))
	        ->add('fromName', 'text', array('label'=> 'fromName','required'=>true))
	        ->add('encryption', 'text', array('label'=> 'encryption', 'required'=>true))
            ->add('smtpHost', 'text', array('label'=> 'smtpHost','required'=>true))
            ->add('smtpPort', 'text', array('label'=> 'smtpPort', 'required'=>true))
            ->add('file', 'file', array('label'=> 'uploadLogo', 'required'=>false));
            
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Swan\CoreBundle\Entity\CompanySetting'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'swan_corebundle_companysetting';
    }
}
