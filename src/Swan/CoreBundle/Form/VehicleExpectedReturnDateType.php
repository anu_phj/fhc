<?php

namespace Swan\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Swan\CoreBundle\Services\form\VehicleForm;
use Symfony\Component\Form\FormInterface;

class VehicleExpectedReturnDateType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		
		$builder->add('expectedReturnDate', 'datePicker', array('label'=> 'expectedReturnDate', 'required'=>false, 'attr' => array('placeholder' => 'dd-mm-yyyy')));
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
		$resolver->setDefaults(array(
			
			'validation_groups' => function (FormInterface $form) {
			
				$data = $form->getData();

				if ($data->getStatus() == 2) {

					return array('order');
				}

				return array('Default');
			},
		));        
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'swan_corebundle_vehicle';
    }
}
