<?php

namespace Swan\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ClientGoalType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
	        ->add('clientGoalType', 'entity', 
					array('class' => 'CoreBundle:ClientGoalType', 
						'label'=> 'Type', 
						'empty_value'=> 'Select',
					)
            )
	        ->add('name', 'text', array('label'=> 'Name'))
	        ->add('value', 'text', array('label'=> 'Value Today'))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Swan\CoreBundle\Entity\ClientGoal'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'swan_corebundle_client_goal';
    }
}