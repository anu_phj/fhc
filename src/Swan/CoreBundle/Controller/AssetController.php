<?php

namespace Swan\CoreBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Swan\CoreBundle\Entity\Asset;


/**
 * User controller.
 *
 */
class AssetController extends Controller
{
	/**
	 * Agenda remove Action
	 *
	 * @param Integer userId
	 * @param Integer goalId
	 *
	 * @return
	 */
	public function getAssetsAction(Request $request)
	{
		$assets = $request->request->get('assets');
		$assetValue = $request->request->get('assetValue');
		
		$assetsArr = explode(",", $assets);
		$assetsValueArr = explode(",", $assetValue);
		
		$response = array('status'=>'success', 'asset' => $assetsArr);
		
		if (in_array('0', $assetsArr) || $this->emptyElementExists($assetsValueArr) !== false) {
			$response = array('status'=>'error', 'value' => 'Please select a value');
		}
		
		$response = new Response(json_encode($response));
		$response->headers->set('Content-Type', 'application/json');

		return $response;
	}
	
	private function emptyElementExists($arr) {
		
		return array_search("", $arr) !== false;
	}
}