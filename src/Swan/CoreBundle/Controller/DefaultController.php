<?php

namespace Swan\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * For access Denied messsage
     *
     * @return access denied 
     */
    public function accessDeniedAction() 
    {
    
        return $this->render('CoreBundle:Default:accessDenied.html.twig');
    }

    /**
     * For displaying the dashboard
     *
     * @param Request $request The request object
     *
     * @return object vehiclesWithoutContract
     */
    public function indexAction(Request $request)
    {
	    $missingVehiclesInImport = '';
        
        $vehiclesWithoutActiveCostAllocation = '';
        
		$vehiclesContractToBeExpire = '';

	    $vehiclesWithNearbyApkExpiry = '';
	    
	    $vehiclesWithPoolAllocations = '';
	    
	    $fleetStatDataForGraph = '';
	    
	    $fleetStatData = '';
	    
        return $this->render('CoreBundle:Default:index.html.twig', array(
			'vehiclesWithoutContract' => NULL,
			'missingVehiclesInImport' => $missingVehiclesInImport,
			'vehiclesWithoutActiveCostAllocation' => $vehiclesWithoutActiveCostAllocation,
			'vehiclesContractToBeExpire' => $vehiclesContractToBeExpire,
	        'vehiclesWithNearbyApkExpiry' => $vehiclesWithNearbyApkExpiry,
	        'vehiclesWithPoolAllocations' => $vehiclesWithPoolAllocations,
	        'fleetStatDataForGraph' => $fleetStatDataForGraph,
	        'fleetStatData' => $fleetStatData,
        ));
    }

    /**
     * Lists all Vehicle that are without contract.
     *
     * @param Request $request The request object
     *
     * @return vehicle list
     */                    
    public function listVehiclesWithoutContractAction(Request $request)
    {
        $vehiclesWithoutContract = $this->get('core_vehicle')->findAllVehicleWithoutContract($request->getSession()->get('company'));
        
        return $this->render('CoreBundle:Default:vehiclesWithoutContractList.html.twig', array(
            'vehiclesWithoutContract' => $vehiclesWithoutContract['data'],
        ));
    }
    
    /**
     * Lists all Vehicle that are without contract.
     *
     * @param Request $request The request object
     *
     * @return vehicle list
     */                    
    public function listMissingVehiclesInImportAction(Request $request)
    {
        $missingVehiclesInImport = $this->get('core_vehicle')->getAllMissingVehiclesInImport($request->getSession()->get('company'));
        
        return $this->render('CoreBundle:Default:missingVehiclesInImportList.html.twig', array(
            'missingVehiclesInImport' => $missingVehiclesInImport,
        ));
    }
    
    /**
     * Lists all vehicles that do not have any active cost allocation.
     *
     * @param Request $request The request object
     *
     * @return vehicle with inactiveCostAllocation list
     */                    
    public function listVehiclesWithoutActiveCostAllocationAction(Request $request)
    {
        $vehiclesWithoutActiveCostAllocation = $this->get('core_vehicle')->getVehiclesWithoutActiveCostAllocation($request->getSession()->get('company'));
        
        return $this->render('CoreBundle:Default:vehiclesWithoutActiveCostAllocationList.html.twig', array(
            'vehiclesWithoutActiveCostAllocation' => $vehiclesWithoutActiveCostAllocation,
        ));
    }
    
    /**
     * Lists all vehicles whose contract are going to expire
     *
     * @param Request $request The request object
     *
     * @return vehicle
     */                    
    public function listVehiclesContractToBeExpireAction(Request $request)
    {
        return $this->_listVehiclesContractToBeExpire('admin', $request);
    }
    
    /**
     * Lists all vehicles whose contract are going to expire for customer
     *
     * @param Request $request The request object
     *
     * @return vehicle
     */                    
    public function listCustomerVehiclesContractToBeExpireAction(Request $request)
    {
       return $this->_listVehiclesContractToBeExpire('customer', $request);
    }    
    
    
    /**
     * Lists all vehicles whose contract are going to expire
     *
     * @param $portal
     * @param Request $request The request object
     *
     * @return vehicle
     */                    
    private function _listVehiclesContractToBeExpire($portal, Request $request)
    {
		$showActive = $portal=='admin' ? false : true;
        $vehiclesContractToBeExpire = $this->get('core_vehicle')->getVehiclesContractToBeExpire($request->getSession()->get('company'), '', $showActive);
        
        $twigFile = $portal=='admin' ? 'vehiclesContractToBeExpireList.html.twig' : 'customerVehiclesContractToBeExpireList.html.twig';
        
        return $this->render('CoreBundle:Default:'.$twigFile, array(
            'vehiclesContractToBeExpire' => $vehiclesContractToBeExpire,
        ));
    }    
    
    
    /**
     * Deletes a vehicle that do not have any active cost allocation
     *
     * @param Request $request The request object
     * @param Integer $vehicleId The vehicle id
     *
     * @return vehicle with inactiveCostAllocation list
     */                         
    public function deleteVehiclesWithoutActiveCostAllocationAction(Request $request, $vehicleId)
    {
		$this->get('wbs_core')->softDeleteRecord($request, new Vehicle(), $vehicleId);

        return $this->redirect($this->generateUrl('_core_vehicle_without_active_costAllocation'));
    }

	/**
	 * Lists all vehicles with nearby apk date expiry.
	 *
     * @param Request $request The request object
     *
	 * @return vehicles
	 */
	public function listVehiclesWithNearbyApkExpiryAction(Request $request)
	{
		return $this->_listVehiclesWithNearbyApkExpiry('admin', $request);
	}
 
	/**
	 * Lists all vehicles with nearby apk date expiry for customer.
	 *
     * @param Request $request The request object
     *
	 * @return vehicles
	 */
	public function listCustomerVehiclesWithNearbyApkExpiryAction(Request $request)
	{ 
		return $this->_listVehiclesWithNearbyApkExpiry('customer', $request);
	}	
	
	/**
     * Lists all vehicles with nearby apk date expiry.
     *
     * @param $portal
     * @param Request $request The request object
     *
     * @return vehicles
     */
    private function _listVehiclesWithNearbyApkExpiry($portal, Request $request)
    {
		$showActive = $portal=='admin' ? false : true;
        $vehicles = $this->get('core_vehicle')->getVehiclesWithNearbyApkExpiry($request->getSession()->get('company'), $showActive);
        
        $twigFile = $portal=='admin' ? 'listVehiclesWithNearbyApkExpiryDate.html.twig' : 'listCustomerVehiclesWithNearbyApkExpiryDate.html.twig';
        
        return $this->render('CoreBundle:Default:'.$twigFile, array(
            'vehiclesWithNearbyApkExpiryDate' => $vehicles,
        ));
    }
    
    /**
     * For displaying the dashboard
     *
     * @param Request $request The request object
     *
     * @return object vehiclesWithoutContract
     */
    public function customerDashboardAction(Request $request)
    {
    
		$vehicles = $this->get('core_vehicle')->findAllVehicles($request->getSession()->get('company'),true);
		
		$drivers = $this->get('core_driver')->findAllCustomerDrivers($request->getSession()->get('company'));
		
		$vehicleOrders = $this->get('core_order')->findAllOrders($request->getSession()->get('company'));
        
		$vehiclesContractToBeExpire = $this->get('core_vehicle')->getVehiclesContractToBeExpire($request->getSession()->get('company'), true);

	    $vehiclesWithNearbyApkExpiry = $this->get('core_vehicle')->getVehiclesWithNearbyApkExpiry($request->getSession()->get('company'));
		
        return $this->render('CoreBundle:Default:customerDashboard.html.twig', array(
			'vehicles' => $vehicles,
			'drivers' => $drivers,
			'vehicleOrders' => $vehicleOrders,
			'vehiclesContractToBeExpire' => $vehiclesContractToBeExpire,
	        'vehiclesWithNearbyApkExpiry' => $vehiclesWithNearbyApkExpiry
        ));
    }
    
    public function updateJounalCodeCostAllocationAction(Request $request)
    {
		$em = $this->getDoctrine()->getManager();
		
		$company = $em->getRepository('CoreBundle:Company')->find($request->getSession()->get('company'));

		$costTypeArr = array(
			7 => array(1,6),
			16 => array(1,6),
			4 => array(1,6),
			11 => array(1,6),
			12 => array(1,6),
			6 => array(2,7),
			15 => array(2,7),
			10 => array(2,7),
			3 => array(2,7),
			9 => array(3,8),
			18 => array(3,8),
			17 => array(4,9),
			8 => array(4,9),
			5 => array(5,10),
			2 => array(5,10),
			1 => array(5,10),
			14 => array(5,10),
			13 => array(5,10),
			19 => array(5,10),
		);
		
		
		
		foreach($costTypeArr as $k => $values) {
		    
			$costType = $em->getRepository('CoreBundle:CostType')->find($k);
		    
		    
		    $costAllocations = $em->getRepository('CoreBundle:CostAllocation')->findCostAllocationForJournalCode('1');
		    if (count($costAllocations) >0) {
		    
                echo "<br/>++++++++++++++++1++++++++++++++++++++<br/>";
				foreach($costAllocations as $costAllocation) {
				    
				    $journalCode = $em->getRepository('CoreBundle:JournalCode')->find($values[0]);
				    
				    $costTypeCostAllocation = new CostTypeCostAllocationJournalCode();
				    
				    $mappedJournalCode = $em->getRepository('CoreBundle:CostTypeCostAllocationJournalCode')->findBy(
                        array(
                            'company' => $company,
                            'costAllocation' => $costAllocation,
                            'costType' => $costType,
                            'journalCode' => $journalCode
                        )                   
                    );
                    
                    if(empty($mappedJournalCode)) {
                    
                        echo $costAllocation->getId().'++'.$costType->getId().'++'.$journalCode->getId().'<br>';
				    
                        $costTypeCostAllocation->setCompany($company);
                        $costTypeCostAllocation->setCostAllocation($costAllocation);
                        $costTypeCostAllocation->setCostType($costType);
                        $costTypeCostAllocation->setJournalCode($journalCode);	
                        
                        $em->persist($costTypeCostAllocation);
                    }
				}
			}
			
			echo "<br/>++++++++++++++++2++++++++++++++++++++<br/>";
			
		    $costAllocations2 = $em->getRepository('CoreBundle:CostAllocation')->findCostAllocationForJournalCode('2');
		    if (count($costAllocations2) >0) {
		    
				foreach($costAllocations2 as $costAllocation2) {
				    
				    $journalCode = $em->getRepository('CoreBundle:JournalCode')->find($values[1]);
				    
				    $costTypeCostAllocation = new CostTypeCostAllocationJournalCode();
				    
				    
				    $mappedJournalCode = $em->getRepository('CoreBundle:CostTypeCostAllocationJournalCode')->findBy(
                        array(
                            'company' => $company,
                            'costAllocation' => $costAllocation2,
                            'costType' => $costType,
                            'journalCode' => $journalCode
                        )				    
				    );
				    
				    if(empty($mappedJournalCode)) {
				    
                        echo $costAllocation2->getId().'++'.$costType->getId().'++'.$journalCode->getId().'<br>';
				    
                        $costTypeCostAllocation->setCompany($company);
                        $costTypeCostAllocation->setCostAllocation($costAllocation2);
                        $costTypeCostAllocation->setCostType($costType);
                        $costTypeCostAllocation->setJournalCode($journalCode);  
                        
                        $em->persist($costTypeCostAllocation);
				    }
				}
			}
			
		}
		
		$em->flush();
        die('All values updated');
    }
}
