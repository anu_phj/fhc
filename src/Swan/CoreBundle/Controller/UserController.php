<?php

namespace Swan\CoreBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Swan\CoreBundle\Entity\User;
use Swan\CoreBundle\Entity\UserAssumption;
use Swan\CoreBundle\Entity\Assumption;
use Swan\CoreBundle\Entity\ClientFamilyDetail;
use Swan\CoreBundle\Form\UserType;
use Swan\CoreBundle\Entity\ClientAsset;
use Swan\CoreBundle\Form\ClientGoalType;
use Swan\CoreBundle\Entity\ClientGoal;
use Swan\CoreBundle\Entity\ClientGoalAsset;
use Swan\CoreBundle\Entity\RolePermission;
use Swan\CoreBundle\Entity\UserModuleAction;

/**
 * User controller.
 *
 */
class UserController extends Controller
{

    /**
     * Lists all User entities.
     *
     * @return user list
     */     
    public function listAction($userType)
    {
        $users = $this->get('User')->getAllUsers($this->container->get('session')->get('company'), $userType);

        return $this->render('CoreBundle:User:list.html.twig', array(
            'users' => $users,
        ));
    }

	/**
	 * Delete action.
	 *
	 * @param $userId
	 * @param Request $request
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse
	 */
	public function deleteAction($userId, $userType, Request $request)
	{
		$this->get('wbs_core')->softDeleteRecord($request, new User(), $userId);
		
		return $this->redirect($this->generateUrl('_core_user_list', array('userType'=>$userType)));
	}

	/**
	 * inactive user action
	 *
	 * @param $userId
	 * @param $status
	 * @param Request $request
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse
	 */
	public function inactiveAction($userId, $userType, $status, Request $request)
	{
		$em = $this->getDoctrine()->getManager();
		
		if ($request->getMethod() == 'POST') {
			
			$user = $em->getRepository('CoreBundle:User')->find($userId);
			if (count($user) >0) {
			
				if ($status == 1) {
				
					$user->setInactive(true);
				} else {
				
					$user->setInactive(false);
				}
				
				$em->persist($user);
				$em->flush();
				
				$this->get('session')->getFlashBag()->add('success', 'Lead updated successfully');
			}
		}
		
		return $this->redirect($this->generateUrl('_core_user_list', array('userType'=>$userType)));
	}
	
    /**
     * Displays a form to add a User entity.
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction($userType, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $user = new User();

        $form = $this->createCustomForm($user, $userType);

        $roles = $this->get('core_role')->findAllRoles();
		$customError = false;
		
        if ($request->getMethod() == 'POST') {
			
			$dealerCompany = $request->get('dealer_company');
            $form->handleRequest($request);
			
			$validator = $this->get('validator');
            $errors = $validator->validate($user);
			
			if($userType == 'client' && $form->getData()->getOccupation()=='') {
				
				$customError = $this->get('translator')->trans('occupationNotBlank');
			}
			
			$customError = $this->getCustomErrorForDealer($dealerCompany, $userType);
			
            if ($form->isValid() && count($errors)<=0 && $customError===false) {
				
				$userTypeForDb = $this->get('User')->getUserTypeForDb($userType);
	            $user->setType($userTypeForDb);
                $user->setInactive(false);
                $user->setDeleted(false);
                $user->setIsAdmin(false);
	            $user->setCreated(new \DateTime());
                $user->setPassword(sha1($user->getPassword()));
                
				//set user role
				$this->get('User')->setUserRole($user,  array($userTypeForDb)); 
                
                $em->persist($user);
                $em->flush();
				
				$this->get('User')->setRolePermissions($user);
				$this->get('User')->setDealerCompany($dealerCompany, $user);
				
                if (empty($dealerCompany)) {
					
					$this->get('User')->setUserCompany($request, $user);
				}
				
				$this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('addSuccess'));

                return $this->redirect($this->generateUrl('_core_user_list', array('userType'=> $userType) ));
            }
		}

        return $this->render('CoreBundle:User:new.html.twig', array(
            'user' => $user,
            'form'   => $form->createView(),
            'errors' => isset($errors) ? $errors : false,
            'roles' => $roles,
			'customError' => $customError,
        ));
    }	
	
	private function getCustomErrorForDealer($dealerCompany, $userType, $user=NULL)
	{
		$em = $this->getDoctrine()->getManager();
		$customError = false;
		
		if($userType == 'dealer' && empty($dealerCompany)) {
				
			$customError = $this->get('translator')->trans('companyNotBlank');
		}
		
		$existingCompanies = array();
		$existingCompaniesInDb = $em->getRepository("CoreBundle:Company")->findAll();
		
		if(!empty($existingCompaniesInDb)) {
			
			foreach($existingCompaniesInDb as $companyName) {
				$existingCompanies[] = $companyName->getName();
			}
		}
		
		
		if($userType == 'dealer' && !empty($dealerCompany)) {
			if(!empty($user)) {
				$companyInSession = $this->container->get('website.twig.extension')->getCompanyNameFromId($this->get('session')->get('company'));
				if($companyInSession != $dealerCompany && in_array($dealerCompany, $existingCompanies)) {
						
					$customError = $this->get('translator')->trans('companyNameAlreadyExists');
				}
			} else {
				if(in_array($dealerCompany, $existingCompanies)) {
						
					$customError = $this->get('translator')->trans('companyNameAlreadyExists');
				}
			}
		}
		
		return $customError;
	}

	/**
	 * Displays a form to edit an existing User entity.
	 *
	 * @param $userId
	 * @param Request $request
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 */
	public function editAction($userId, $userType, Request $request)
	{
		$em = $this->getDoctrine()->getManager();

		$user = $em->getRepository('CoreBundle:User')->find($userId);
		
		if (!$user) {
			throw $this->createNotFoundException('Unable to find User entity.');
		}

		$this->checkUserEntityAccess($user);

		$oldPassword = $user->getPassword();
		
		$roles = $this->get('core_role')->findAllRoles();
	
		$editForm = $this->createCustomForm($user, $userType, 'edit');
		$customError = false;
		
		if ($request->getMethod() == 'POST') {

			$editForm->handleRequest($request);
			$dealerCompany = $request->get('dealer_company');
			
			$userRequest = $request->request->get('swan_corebundle_user');

			$noResetPassword = false;
			if (!isset($userRequest['password']['first'])) {

				$user->setPassword($oldPassword);
				$noResetPassword = true;
			}

			$validator = $this->get('validator');
			$errors = $validator->validate($user);
			
			if($userType != 'dealer') {
				
				$customError = $this->get('translator')->trans('occupationNotBlank');
			}
			
			$customError = $this->getCustomErrorForDealer($dealerCompany, $userType, $user);

			if (($editForm->isValid() || $noResetPassword===true) && count($errors)<=0 && $customError === false) {

				if ($noResetPassword!==true) {

					$user->setPassword(sha1($user->getPassword()));
				}

				//delete user roles
				$this->get('User')->deleteUserRoles($user);
				//set user role
				$this->get('User')->setUserRole($user, $request->get('roles'));

				$user->setUpdated(new \DateTime());

				$em->persist($user);
				$em->flush();
				
                if ($user->getId()>0) {
                    $this->get('UserAccess')->addUserAccess($request, $user);
					$this->get('User')->updateDealerCompany($dealerCompany);
                }

				$this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('updateSuccess'));

				return $this->redirect($this->generateUrl('_core_user_edit', array('userType'=>$userType, 'userId'=>$userId)));
			} else {

				$formErrors = $this->get('wbs_core')->getFormErrors($editForm);
				if (count($formErrors)>0) {

					$passwordError = isset($formErrors['password']['first'][0]) ? $formErrors['password']['first'][0] : '';
				}
			}
		}

		return $this->render('CoreBundle:User:edit.html.twig', array(
			'user' => $user,
			'form'   => $editForm->createView(),
			'errors' => isset($errors) ? $errors : false,
			'passwordError' => isset($passwordError) ? $passwordError : false,
			'roles' => $roles,
			'customError' => $customError,
		));
	}

	/**
	 * Creates a form to edit a User entity.
	 *
	 * @param User $entity The entity
	 * @param Form $type
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createCustomForm(User $entity, $userType, $type = NULL)
	{
		$form = $this->createForm(new UserType($userType, $type), $entity, array(
			'action' => '',
			'method' => 'POST',
		));

		$form->add('submit', 'submit', array('label' => 'Save'));

		return $form;
	}
	
	private function createClientGoalForm(ClientGoal $entity)
	{
		$form = $this->createForm(new ClientGoalType(), $entity, array(
			'action' => '',
			'method' => 'POST',
		));

		$form->add('submit', 'submit', array('label' => 'Save'));

		return $form;
	}

	/**
	 * Displays a form to edit assumptions for an existing User entity.
	 *
	 * @param $userId
	 * @param Request $request
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 */
	public function assumptionAction($userId, Request $request)
	{
		$em = $this->getDoctrine()->getManager();
		
		$user = $em->getRepository('CoreBundle:User')->find($userId);

		if (!$user) {
			throw $this->createNotFoundException('Unable to find User entity.');
		}

		$assumptionQuestions = $em->getRepository('CoreBundle:Assumption')->findAll();
		$listAssets = $em->getRepository('CoreBundle:Asset')->findAll();

		$userAssumptionsArr = $em->getRepository('CoreBundle:UserAssumption')->findByUser($userId);
		$userAssumptions = $this->_listUserAssumptions($userAssumptionsArr);

		$userAssetArr = $em->getRepository('CoreBundle:ClientAsset')->findByUser($userId);
		$userAssets = $this->_listUserAssets($userAssetArr);

		if ($request->getMethod() == 'POST') {

			$listUserAssumptions = $request->get('user_assumption');

			if(!empty($listUserAssumptions)) {

				$this->get('User')->removeUserAssumptions($userAssumptionsArr);

				foreach($listUserAssumptions as $questionId=>$userAssumption) {

					$userAssumptionObj = new UserAssumption();
					$userAssumptionObj->setAssumption($em->getRepository('CoreBundle:Assumption')->find($questionId));
					$userAssumptionObj->setUser($user);
					$userAssumptionObj->setAnswer($userAssumption['answer']);

					$em->persist($userAssumptionObj);
				}
			}

			$listUserAssets = $request->get('user_asset');

			if(!empty($listUserAssets)) {

				$this->get('User')->removeUserAssets($userAssetArr);

				foreach($listUserAssets as $assetId=>$listUserAsset) {

					$userAssetObj = new ClientAsset();
					$userAssetObj->setAsset($em->getRepository('CoreBundle:Asset')->find($assetId));
					$userAssetObj->setUser($user);
					$userAssetObj->setValue($listUserAsset['value']);

					if(!empty($listUserAsset['fromDate'])) {

						$userAssetObj->setFromDate(new \DateTime($listUserAsset['fromDate']));
					} else {

						$userAssetObj->setFromDate(NULL);
					}


					$userAssetObj->setUpdated(new \DateTime());

					$em->persist($userAssetObj);
				}
			}

			$em->flush();

			$this->get('session')->getFlashBag()->add('success', 'Assets & Assumptions updated successfully');

			return $this->redirect($this->generateUrl('_core_user_assumption', array('userId' => $userId)));
		}

		return $this->render('CoreBundle:User:assumption.html.twig', array(
			'assumptionQuestions' => $assumptionQuestions,
			'userAssumptions' => $userAssumptions,
			'listAssets' => $listAssets,
			'userAssets' => $userAssets
		));
	}

	/**
	 * List user assumptions
	 *
	 * @param $userAssumptions
	 * @return array
	 */
	private function _listUserAssumptions($userAssumptions)
	{
		$listUserAssumptions = array();
		if(!empty($userAssumptions)) {

			foreach($userAssumptions as $userAssumption) {

				if(count($userAssumption->getAssumption()) > 0) {

					$listUserAssumptions[$userAssumption->getAssumption()->getId()] = $userAssumption->getAnswer();
				}
			}
		}

		return $listUserAssumptions;
	}

	/**
	 * List user assets
	 *
	 * @param $userAssets
	 * @return array
	 */
	private function _listUserAssets($userAssets)
	{
		$listUserAssets = array();
		if(!empty($userAssets)) {

			foreach($userAssets as $userAsset) {

				if(count($userAsset->getAsset()) > 0) {

					$listUserAssets[$userAsset->getAsset()->getId()] = array(
						'value' => $userAsset->getValue(),
						'fromDate' => $this->get('wbs_core')->validDate($userAsset->getFromDate(), 'd-m-Y')
					);
				}
			}
		}

		return $listUserAssets;
	}

	/**
	 * Displays a form to edit family details for an existing User entity.
	 *
	 * @param $userId
	 * @param Request $request
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 */
	public function familyDetailsAction($userId, Request $request)
	{
		$em = $this->getDoctrine()->getManager();

		$user = $em->getRepository('CoreBundle:User')->find($userId);

		if (!$user) {
			throw $this->createNotFoundException('Unable to find User entity.');
		}

		$familyDetails = $em->getRepository('CoreBundle:ClientFamilyDetail')->findByUser($userId);

		if ($request->getMethod() == 'POST') {

			$listFamilyDetails = $request->get('familyDetails');

			if(!empty($listFamilyDetails['firstName'])) {

				foreach($listFamilyDetails['firstName'] as $key=>$firstName) {

					if($this->_validateFamilyDetailFormRow($listFamilyDetails, $key)) {

						$clientFamilyDetailObj = new ClientFamilyDetail();
						$clientFamilyDetailObj->setUser($user);
						$clientFamilyDetailObj->setFirstName($firstName);
						$clientFamilyDetailObj->setLastName($listFamilyDetails['lastName'][$key]);
						$clientFamilyDetailObj->setDob(new \DateTime($listFamilyDetails['dob'][$key]));
						$clientFamilyDetailObj->setRelation($listFamilyDetails['relation'][$key]);
						$clientFamilyDetailObj->setOccupation($listFamilyDetails['occupation'][$key]);

						$em->persist($clientFamilyDetailObj);
					}
				}

				$em->flush();

				$this->get('session')->getFlashBag()->add('success', 'Family Details updated successfully');
				return $this->redirect($this->generateUrl('_core_user_family_details', array('userId' => $userId)));
			}
		}

		$occupations = array(
			'Business'=>'Business',
			'Service'=>'Service',
			'Housewife'=>'Housewife',
			'Student'=>'Student',
			'Retired'=>'Retired'
		);

		return $this->render('CoreBundle:User:familyInformation.html.twig', array(
			'familyDetails' => $familyDetails,
			'occupations' => $occupations,
		));
	}

	/**
	 * Validate Family Detail Form Row
	 *
	 * @param $listFamilyDetails
	 * @param $key
	 * @return bool
	 */
	private function _validateFamilyDetailFormRow($listFamilyDetails, $key)
	{
		$validateFields = array('firstName', 'dob', 'relation', 'occupation');

		foreach($validateFields as $validateField) {

			if(!isset($listFamilyDetails[$validateField][$key]) || empty($listFamilyDetails[$validateField][$key])) {

				return false;
			}
		}

		return true;
	}

	/**
	 * Agenda remove Action
	 * @param Integer familyMemberId entityId
	 *
	 * @return JSON Success or Error
	 */
	public function deleteFamilyMemberAction(Request $request, $userId, $familyMemberId)
	{
		$em = $this->getDoctrine()->getManager();

		$entity = $em->getRepository('CoreBundle:ClientFamilyDetail')->findOneBy(
			array(
				'id' => $familyMemberId,
				'user' => $userId,
			)
		);

		if (!$entity) {
			throw $this->createNotFoundException('Member does not exist');
		}

		if ($request->getMethod() == 'POST') {
			$em->remove($entity);
			$em->flush();

			$this->get('session')->getFlashBag()->add('success', 'Record removed successfully');
		}

		return $this->redirect($this->generateUrl('_core_user_family_details', array('userId' => $userId)));
	}

	/**
	 * Displays a form to edit goals for an existing User entity.
	 *
	 * @param $userId
	 * @param Request $request
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 */
	public function goalsAction($userId, Request $request)
	{
		$em = $this->getDoctrine()->getManager();
		$usedGoalsArr = array();
		
        $clientGoals = $em->getRepository('CoreBundle:ClientGoal')->findGoals($userId);
		$usedClientGoals = $em->getRepository('CoreBundle:ClientGoal')->findUsedGoals();
		
		
		if(count($usedClientGoals)>0) {
			foreach($usedClientGoals as $usedGoals) {
				
				$usedGoalsArr[] = $usedGoals['id'];
			}
		}
		
        return $this->render('CoreBundle:User:goals.html.twig', array(
            'clientGoals' => $clientGoals,
			'usedGoalsArr' => $usedGoalsArr,
        ));
	}
	
	/**
	 * Displays a form to edit goals for an existing User entity.
	 *
	 * @param $userId
	 * @param Request $request
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 */
	public function goalsNewAction($userId, Request $request)
	{
		$em = $this->getDoctrine()->getManager();
        
        $goal = new ClientGoal();
		$assets = $em->getRepository('CoreBundle:Asset')->findAll();

        $form = $this->createClientGoalForm($goal);
		$customErrors = array();
		
        if ($request->getMethod() == 'POST') {
			
            $form->handleRequest($request);
			
			$clientGoalObj = $request->request->get('swan_corebundle_client_goal');
			$user = $em->getRepository('CoreBundle:User')->find($userId);
			
			$asset = $request->request->get('asset');
			$assetValue = $request->request->get('assetValue');
			
			$targetDate = $request->get('targetDate');
			
			$targetTimestamp = strtotime('01-'.$targetDate);
			$targetDate = date('Ymd',$targetTimestamp);
			$currentDate = date('Ymd');
			
			if(count($asset)>0) {
				
				foreach($asset as $key=>$value) {
					
					if($value > 0 && empty($assetValue[$key])) {
						
						$customErrors[] = 'Please add goal assets with values';
					} else if($value > 0 && !empty($assetValue[$key])) {

						if ($this->getErrorForInvalidAssetValue($userId, $value, $assetValue[$key]) === true) {

							$customErrors[] = 'There are not enough funds added for this asset value';
						}
					}
				}
			}
			
			if($currentDate>$targetDate) {
				
				$customErrors[] = 'Please add valid target date';
			}
			
            $validator = $this->get('validator');
            $errors = $validator->validate($goal);

            if ($form->isValid() && count($errors)<=0 && count($customErrors) <= 0) {
				
				$year = date('Y',$targetTimestamp);
				$year = (int)$year;
				
				$month = date('m',$targetTimestamp);
				$month = (int)$month;
				
				$goal->setYear($year);
				$goal->setMonth($month);
				$goal->setCreatedOn(new \DateTime);
				$goal->setUpdated(new \DateTime);
				$goal->setUser($user);
                $em->persist($goal);
                $em->flush();
                
				
				foreach($asset as $key=>$assetId) {
					
					$goalAsset = new ClientGoalAsset();
					
					$asset = $em->getRepository('CoreBundle:Asset')->find($assetId);
					$value = $assetValue[$key];
					
					$goalAsset->setAsset($asset);
					$goalAsset->setValue($value);
					$goalAsset->setClientGoal($goal);
					$goalAsset->setUser($user);
					$goalAsset->setUpdated(new \DateTime());
					
					$em->persist($goalAsset);
					$em->flush();
				}
                
				
                $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('addSuccess'));

                return $this->redirect($this->generateUrl('_core_user_goals', array('userId' => $userId)));
            }
        }

        return $this->render('CoreBundle:User:goalsNew.html.twig', array(
            'goal' => $goal,
            'form'   => $form->createView(),
            'errors' => isset($errors) ? $errors : false,
			'assets' => $assets,
			'customErrors' => $customErrors
        ));
	}

	public function goalsNewAjaxAction($userId, Request $request)
	{
		$em = $this->getDoctrine()->getManager();

		$goal = new ClientGoal();
		$assets = $em->getRepository('CoreBundle:Asset')->findAll();

		$form = $this->createClientGoalForm($goal);
		$customErrors = array();

		if ($request->getMethod() == 'POST') {

			$form->handleRequest($request);

			$clientGoalObj = $request->request->get('swan_corebundle_client_goal');
			$user = $em->getRepository('CoreBundle:User')->find($userId);

			$asset = $request->request->get('asset');
			$assetValue = $request->request->get('assetValue');

			$targetDate = $request->get('targetDate');

			$targetTimestamp = strtotime('01-'.$targetDate);
			$targetDate = date('Ymd',$targetTimestamp);
			$currentDate = date('Ymd');

			if(count($asset)>0) {

				foreach($asset as $key=>$value) {

					if($value > 0 && empty($assetValue[$key])) {

						$customErrors[] = 'Please add goal assets with values';
					} else if($value > 0 && !empty($assetValue[$key])) {

						if ($this->getErrorForInvalidAssetValue($userId, $value, $assetValue[$key]) === true) {

							$customErrors[] = 'There are not enough funds added for this asset value';
						}
					}
				}
			}

			if($currentDate>$targetDate) {

				$customErrors[] = 'Please add valid target date';
			}

			$validator = $this->get('validator');
			$errors = $validator->validate($goal);

			if ($form->isValid() && count($errors)<=0 && count($customErrors) <= 0) {

				$year = date('Y',$targetTimestamp);
				$year = (int)$year;

				$month = date('m',$targetTimestamp);
				$month = (int)$month;

				$goal->setYear($year);
				$goal->setMonth($month);
				$goal->setCreatedOn(new \DateTime);
				$goal->setUpdated(new \DateTime);
				$goal->setUser($user);
				$em->persist($goal);
				$em->flush();


				foreach($asset as $key=>$assetId) {

					$goalAsset = new ClientGoalAsset();

					$asset = $em->getRepository('CoreBundle:Asset')->find($assetId);
					$value = $assetValue[$key];

					$goalAsset->setAsset($asset);
					$goalAsset->setValue($value);
					$goalAsset->setClientGoal($goal);
					$goalAsset->setUser($user);
					$goalAsset->setUpdated(new \DateTime());

					$em->persist($goalAsset);
					$em->flush();
				}


				$this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('addSuccess'));

				return $this->redirect($this->generateUrl('_core_user_goals', array('userId' => $userId)));
			}
		}

		return $this->render('CoreBundle:User:goalsNew.html.twig', array(
			'goal' => $goal,
			'form'   => $form->createView(),
			'errors' => isset($errors) ? $errors : false,
			'assets' => $assets,
			'customErrors' => $customErrors
		));
	}

	
	/**
	 * Displays a form to edit goals for an existing User entity.
	 *
	 * @param $userId
	 * @param Request $request
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 */
	public function goalsEditAction($userId, $clientGoalId, Request $request)
	{
		$em = $this->getDoctrine()->getManager();
        
        $clientGoal = $em->getRepository('CoreBundle:ClientGoal')->findBy(array('id' => $clientGoalId , 'user' => $userId));
		$assets = $em->getRepository('CoreBundle:Asset')->findAll();
		$clientGoalAsset = $em->getRepository('CoreBundle:ClientGoalAsset')->findBy(array('clientGoal' => $clientGoalId , 'user' => $userId));
		$user = $em->getRepository('CoreBundle:User')->find($userId);
		
		if (!$clientGoal) {
			throw $this->createNotFoundException('Client Goal does not exist');
		} else {
			
			$clientGoal = reset($clientGoal);
		}

		$month = $clientGoal->getMonth();
		$year = $clientGoal->getYear();
		
		$targetDate = '01-'.$month.'-'.$year;
		
		$targetTimestamp = strtotime($targetDate);
		$targetDate = date('m-Y',$targetTimestamp);
		
		$form = $this->createClientGoalForm($clientGoal);
		
		$customErrors = array();
		
        if ($request->getMethod() == 'POST') {
			
            $form->handleRequest($request);
			
			$clientGoalObj = $request->request->get('swan_corebundle_client_goal');
			
			$asset = $request->request->get('asset');
			$assetValue = $request->request->get('assetValue');
			
			$targetDatePosted = $request->get('targetDate');
			
			$targetTimestamp = strtotime('01-'.$targetDatePosted);
			$targetDatePosted = date('Ymd',$targetTimestamp);
			$currentDate = date('Ymd');
			
			$targetDate = date('m-Y',$targetTimestamp);
			
			$tmp = array_filter($asset);
			if (empty($tmp)) {
				$customErrors = array();
			} else {
				foreach($asset as $key=>$value) {
					if($value > 0 && empty($assetValue[$key])) {

						$customErrors[] = 'Please add goal assets with value greater that zero';
					} else if($value > 0 && !empty($assetValue[$key])) {
						if ($this->getErrorForInvalidAssetValue($userId, $value, $assetValue[$key], $clientGoalId) === true) {
						
							$customErrors[] = 'Please add another asset with appropriate value';
						}
					}
				}
			}
			
			if($currentDate > $targetDatePosted) {
				
				$customErrors[] = 'Please add valid target date';
			}
			
            $validator = $this->get('validator');
            $errors = $validator->validate($clientGoal);
			
            if ($form->isValid() && count($errors)<=0 && empty($customErrors)) {
				
				$year = date('Y',$targetTimestamp);
				$year = (int)$year;
				
				$month = date('m',$targetTimestamp);
				$month = (int)$month;
				
				$clientGoal->setYear($year);
				$clientGoal->setMonth($month);
				$clientGoal->setUpdated(new \DateTime);
				
                $em->persist($clientGoal);
                $em->flush();
			
				if(!empty($tmp)) {
					
					if(count($clientGoalAsset)>0) {
						
						if(count($clientGoalAsset) >= count($asset)) {
							foreach($clientGoalAsset as $goalAsset) {
								
								if(count($goalAsset->getAsset())>0) {
									
									$assetIdInDb = $goalAsset->getAsset()->getId();
									if (in_array($assetIdInDb, $asset)) {
										
										$assetObj = $em->getRepository('CoreBundle:Asset')->find($assetIdInDb);
										$assetValueKey = array_search($assetIdInDb, $asset);
										$value = $assetValue[$assetValueKey];
										$goalAsset->setAsset($assetObj);
										$goalAsset->setValue($value);
										$goalAsset->setUpdated(new \DateTime());
										
										$em->persist($goalAsset);
									} else {
										$em->remove($goalAsset);
									}
								} else {
									
									$assetObj = $em->getRepository('CoreBundle:Asset')->find($asset[0]);
									$value = $assetValue[0];
									$goalAsset->setAsset($assetObj);
									$goalAsset->setValue($value);
									$goalAsset->setUpdated(new \DateTime());
										
									$em->persist($goalAsset);
								}
							}
						} else if(count($clientGoalAsset) < count($asset)) {
							
							foreach($asset as $key=>$assetId) {
								if ($assetId>0) {
									
									$existingAssetInDb = $em->getRepository("CoreBundle:ClientGoalAsset")->findBy(array('clientGoal' => $clientGoalId , 'user' => $userId, 'asset'=>$assetId));
									if(count($existingAssetInDb)>0) {
										
										$goalAsset = reset($existingAssetInDb);
										$assetObj = $em->getRepository('CoreBundle:Asset')->find($assetId);
										$value = $assetValue[$key];
										$goalAsset->setAsset($assetObj);
										$goalAsset->setValue($value);
										$goalAsset->setUpdated(new \DateTime());
									
										$em->persist($goalAsset);
									} else {
										
										$goalAsset = new ClientGoalAsset();
										$asset = $em->getRepository("CoreBundle:Asset")->find($assetId);
										$value = $assetValue[$key];
										
										$goalAsset->setAsset($asset);
										$goalAsset->setValue($value);
										$goalAsset->setUpdated(new \DateTime());
										$goalAsset->setUser($user);
										$goalAsset->setClientGoal($clientGoal);
										$em->persist($goalAsset);
									}
								}
							}
						}
						
						$em->flush();
					} else {
						
						foreach($asset as $key=>$assetId) {
							
							if($assetId>0) {
								$goalAsset = new ClientGoalAsset();
								$asset = $em->getRepository("CoreBundle:Asset")->find($assetId);
								$value = $assetValue[$key];
								
								$goalAsset->setAsset($asset);
								$goalAsset->setValue($value);
								$goalAsset->setUpdated(new \DateTime());
								$goalAsset->setUser($user);
								$goalAsset->setClientGoal($clientGoal);
								$em->persist($goalAsset);
							}
						}
						$em->flush();
					}
				} else {
					if(count($clientGoalAsset)>0) {
						
						$em->getRepository("CoreBundle:ClientGoalAsset")->deleteAll($clientGoalId, $userId);
					}
				}
				
				$this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('updateSuccess'));

				return $this->redirect($this->generateUrl('_core_user_goals', array('userId' => $userId)));
			}
        }

        return $this->render('CoreBundle:User:goalsEdit.html.twig', array(
            'clientGoal' => $clientGoal,
            'form'   => $form->createView(),
            'errors' => isset($errors) ? $errors : false,
			'assets' => $assets,
			'clientGoalAsset' => $clientGoalAsset,
			'customErrors' => $customErrors,
			'targetDate' => $targetDate,
        ));
	}
	
	private function getAssetsInDb($userId, $clientGoalId)
	{
		$assetsinDbArr = array();
		$em = $this->getDoctrine()->getManager();
		$totalGoalAssetsForGoal = $em->getRepository('CoreBundle:ClientGoalAsset')->findBy(array('user' => $userId, 'clientGoal'=>$clientGoalId));
		if(count($totalGoalAssetsForGoal)>0) {
			
			foreach($totalGoalAssetsForGoal as $assetForGoal) {
				
				if(count($assetForGoal->getAsset())>0) {
					
					$assetsinDbArr[$assetForGoal->getAsset()->getId()] = $assetForGoal;
				}
			}
		}
		
		return $assetsinDbArr;
	}
	
	/**
	 * Agenda remove Action
	 *
	 * @param Integer userId
	 * @param Integer goalId
	 *
	 * @return
	 */
	public function deleteGoalAction(Request $request, $userId, $clientGoalId)
	{
		$em = $this->getDoctrine()->getManager();

		$entity = $em->getRepository('CoreBundle:ClientGoal')->findOneBy(
			array(
				'id' => $clientGoalId,
				'user' => $userId,
			)
		);

		if (!$entity) {
			throw $this->createNotFoundException('Goal does not exist');
		}

		if ($request->getMethod() == 'POST') {
			$em->remove($entity);
			$em->flush();

			$this->get('session')->getFlashBag()->add('success', 'Record removed successfully');
		}

		return $this->redirect($this->generateUrl('_core_user_goals', array('userId' => $userId)));
	}
	
	/**
	 * Agenda remove Action
	 *
	 * @param Integer userId
	 * @param Integer goalId
	 *
	 * @return
	 */
	public function getGoalassetValueAction(Request $request, $userId)
	{
		$em = $this->getDoctrine()->getManager();
		
		$assetId = $request->request->get('assetId');
		$clientGoal = $request->request->get('clientGoalId');
		$clientAssetValue = 0;
		
		if($assetId>0) {
			
			$clientAsset = $em->getRepository('CoreBundle:ClientAsset')->findBy(array('asset'=>$assetId, 'user' => $userId));
			
			$existingClientGoalAssetsForUser = $em->getRepository('CoreBundle:ClientGoalAsset')->findExistingClientGoalAssetsForUser($userId, $assetId, $clientGoal);
			
		}
		
		$sumofValues = 0;
		if(!empty($existingClientGoalAssetsForUser)) {
			
			$sumofValues = reset($existingClientGoalAssetsForUser)['sumOfValues'];
		}
		
		if(!empty($clientAsset)) {
			
			$clientAsset = reset($clientAsset);
			if ($clientAsset->getAsset()->getInterestTypeMonthly()) {
				
				$clientAssetValue = $clientAsset->getValue();
			} else {
				
				$clientAssetValue = $clientAsset->getValue() - $sumofValues;
			}
		}
		
		if(empty($clientAsset) || $clientAssetValue<=0) {
          $response = array('status'=>'error', 'value' => 0);
        }
		
		$response = array('status'=>'success', 'value' => $clientAssetValue);
		
		$response = new Response(json_encode($response));
		$response->headers->set('Content-Type', 'application/json');

		return $response;
	}
	
	private function getErrorForInvalidAssetValue($userId, $assetId, $postedValue, $clientGoalId=false)
	{
		$em = $this->getDoctrine()->getManager();
		$clientAssetValue = $existingClientGoalAssetValueForUser = 0;
		$error = true;
		
		if($assetId>0) {
			
			$clientAsset = $em->getRepository('CoreBundle:ClientAsset')->findBy(array('asset'=>$assetId, 'user' => $userId));
			
			if(count($clientAsset)>0){
				
				$clientAsset = reset($clientAsset);
				$clientAssetValue = $clientAsset->getValue();
			}
			
			$existingClientGoalAssetsForUser = $em->getRepository('CoreBundle:ClientGoalAsset')->findExistingClientGoalAssetsForUser($userId, $assetId, $clientGoalId);
			
			if(count($existingClientGoalAssetsForUser)>0) {
				$existingClientGoalAssetsForUser = reset($existingClientGoalAssetsForUser);
				$existingClientGoalAssetValueForUser = $existingClientGoalAssetsForUser['sumOfValues'];
			}
			
			$assetsThatCanBeAdded = $clientAssetValue - $existingClientGoalAssetValueForUser;
			$error = false;
			
			if($postedValue == 0 || $assetsThatCanBeAdded <= 0 || ($postedValue > $clientAssetValue && $clientAssetValue > 0)) {
				
				$error = true;
			}
		}
		
		return $error;
	}

	public function rolePermissionsAction(Request $request)
	{
		$modules = $this->get('Module')->getAllModulesWithActions();
		$roles = $this->get('core_role')->findActiveRolesDetails();

		$em = $this->getDoctrine()->getManager();
		$rolePermissions = $em->getRepository('CoreBundle:RolePermission')->findAll();

		$permissionsArray = array();
		if(!empty($rolePermissions)) {

			foreach($rolePermissions as $rolePermission) {

				$permissionsArray[$rolePermission->getRole()->getId()][] = $rolePermission->getModuleAction()->getId();
			}
		}

		if ($request->getMethod() == 'POST') {

			if(!empty($_POST)) {

				if(!empty($rolePermissions)) {

					foreach($rolePermissions as $rolePermission) {

						$notFlush = true;
						$this->get('wbs_core')->removeEntity(new RolePermission(), $rolePermission->getId(), $notFlush);
					}
				}

				foreach($_POST as $roleId=>$postedModules) {

					$role = $em->getRepository('CoreBundle:Role')->find($roleId);

					foreach($postedModules as $postedModuleId=>$postedModule) {

						$moduleObj = $em->getRepository('CoreBundle:ModuleAction')->find($postedModuleId);

						$rolePermissionObj = new RolePermission();
						$rolePermissionObj->setRole($role);
						$rolePermissionObj->setModuleAction($moduleObj);

						$em->persist($rolePermissionObj);
					}
				}

				$em->flush();

				$this->get('session')->getFlashBag()->add('success', 'Permissions updated successfully');
				return $this->redirect($this->generateUrl('_core_role_permissions'));
			}
		}

		return $this->render('CoreBundle:User:rolePermissions.html.twig', array(
			'roles' => $roles,
			'modules' => $modules,
			'permissionsArray' => $permissionsArray
		));
	}
	
	public function updatePermissionsAction($userType, $userId, Request $request)
	{
		$em = $this->getDoctrine()->getManager();
		
		$assignedModules = $em->getRepository("CoreBundle:UserModuleAction")->findBy(array('user' => $userId), array('id' => 'DESC'));
		$modules = $em->getRepository("CoreBundle:Module")->getAllModulesWithActions();
		$user = $em->getRepository("CoreBundle:User")->find($userId);
		
		$assignedModuleIdArr = array();
		$userModuleActionIdArr = array();
		
		if(count($assignedModules)>0) {
			foreach($assignedModules as $module) {
				$assignedModuleIdArr[] = count($module->getModuleAction())>0 ? $module->getModuleAction()->getId() : 0;
				$userModuleActionIdArr[] = $module->getId();
			}
		}
		
		if ($request->getMethod() == 'POST') {
			
			$moduleActionsFromReq = $request->request->get('moduleActions');
			$em->getRepository("CoreBundle:UserModuleAction")->deleteAllModuleActionsForUser($userModuleActionIdArr);
			
			if (count($moduleActionsFromReq)>0) {
				foreach($moduleActionsFromReq as $moduleActionId)
				{
					$moduleActionObj = $em->getRepository("CoreBundle:ModuleAction")->find($moduleActionId);
					
					$userModuleAction = new UserModuleAction();
					$userModuleAction->setUser($user);
					$userModuleAction->setModuleAction($moduleActionObj);
					
					$em->persist($userModuleAction);
				}
				
				$em->flush();
			}
			
			return $this->redirect($this->generateUrl('_core_update_user_permissions', array('userType'=>$userType, 'userId'=>$userId)));
		}
		
		return $this->render('CoreBundle:User:userModulePermissions.html.twig', array(
			'assignedModules' => $assignedModules,
			'modules' => $modules,
			'assignedModuleIdArr' => $assignedModuleIdArr,
		));
	}

	public function checkUserEntityAccess($user)
	{
		$securityContext = $this->container->get('security.context');
		$token = $securityContext->getToken();
		$loggedInUser = $token->getUser();

		$userCompany = $user->getCompany();
		if(!empty($userCompany)) {

			$userCompany = $userCompany[0]->getId();
		}

		if ($loggedInUser->getIsAdmin()!=1 && $userCompany != $this->get('session')->get('company')) {

			throw $this->createNotFoundException('Access Denied');
		}
	}

	public function editProfileAction(Request $request)
	{
		$em = $this->getDoctrine()->getManager();

		$securityContext = $this->container->get('security.context');
		$token = $securityContext->getToken();
		$user = $token->getUser();

		if (!$user) {
			throw $this->createNotFoundException('Unable to find User entity.');
		}

		$userRole = $user->getType();

		$userType = 'client';
		if($userRole==1) {
			$userType = 'dealer';
		}elseif($userRole==2) {
			$userType = 'staff';
		}

		$oldPassword = $user->getPassword();

		$roles = $this->get('core_role')->findAllRoles();

		$editForm = $this->createCustomForm($user, $userType, 'edit');
		$customError = false;

		if ($request->getMethod() == 'POST') {

			$editForm->handleRequest($request);
			$dealerCompany = $request->get('dealer_company');

			$userRequest = $request->request->get('swan_corebundle_user');

			$noResetPassword = false;
			if (!isset($userRequest['password']['first'])) {

				$user->setPassword($oldPassword);
				$noResetPassword = true;
			}

			$validator = $this->get('validator');
			$errors = $validator->validate($user);

			$customError = $this->getCustomErrorForDealer($dealerCompany, $userType, $user);

			if (($editForm->isValid() || $noResetPassword===true) && count($errors)<=0 && $customError === false) {

				if ($noResetPassword!==true) {

					$user->setPassword(sha1($user->getPassword()));
				}

				$user->setUpdated(new \DateTime());

				$em->persist($user);
				$em->flush();

				if ($user->getId()>0) {

					$this->get('User')->updateDealerCompany($dealerCompany);
				}

				$this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('updateSuccess'));

				return $this->redirect($this->generateUrl('_core_user_edit_profile'));
			} else {

				$formErrors = $this->get('wbs_core')->getFormErrors($editForm);
				if (count($formErrors)>0) {

					$passwordError = isset($formErrors['password']['first'][0]) ? $formErrors['password']['first'][0] : '';
				}
			}
		}

		return $this->render('CoreBundle:User:editProfile.html.twig', array(
			'user' => $user,
			'form'   => $editForm->createView(),
			'errors' => isset($errors) ? $errors : false,
			'passwordError' => isset($passwordError) ? $passwordError : false,
			'roles' => $roles,
			'userType' => $userType,
			'customError' => $customError,
		));
	}
}