<?php

namespace Swan\CoreBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Swan\CoreBundle\Entity\CompanySetting;
use Swan\CoreBundle\Form\CompanySettingType;

/**
 * CompanySetting controller.
 *
 */
class CompanySettingController extends Controller
{

    /**
     * Creates a form to create a CompanySetting entity.
     *
     * @param CompanySetting $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(CompanySetting $entity)
    {
        $form = $this->createForm(new CompanySettingType(), $entity, array(
            'action' => $this->generateUrl('_core_company_setting_new'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new CompanySetting entity.
     *
     * @param Request $request The request object
     *
     * @return company setting insert form
     */
    public function newAction(Request $request)
    {
		$companyId = $request->getSession()->get('company');
		
		$em = $this->getDoctrine()->getManager();
		
		$companySetting = $this->get('core_company_setting')->getCompanySettingByCompanyId($companyId);		
		
		$errors = false;
		
		$form   = $this->createCreateForm($companySetting);
		
		$emailPassword = $companySetting->getFromEmailPassword();
		
		if ($request->getMethod() == 'POST') {
			
			$form->handleRequest($request);
			
			$validator = $this->get('validator');
            $errors = $validator->validate($companySetting);
			
			$data = $request->request->get('swan_corebundle_companysetting');
			$emailPassword = $data['fromEmailPassword'];
			
			if ($form->isValid()) {
			
				$this->get('wbs_core')->setCompany($companySetting);
				
				if ($companySetting->getFile()!=NULL) {
					$companySetting->preUpload();
					$companySetting->upload();
					$this->get('core_company_setting')->updateLogoName($companySetting);
				} 
				
				$em->persist($companySetting);
				$em->flush(); 
				
				$this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('addSuccess'));

				return $this->redirect($this->generateUrl('_core_company_setting_new'));
			}
		}

        return $this->render('CoreBundle:CompanySetting:new.html.twig', array(        
            'form'   => $form->createView(),
            'errors' => $errors,
            'password' => $emailPassword,
            'companySetting' => $companySetting
        ));
    } 
    
    /**
     * Display company logo.
     *
     * @param $companySettingId
     *
     * @return company setting logo path
     */
     public function displayCompanyLogoAction($companySettingId) {
     
		$em = $this->getDoctrine()->getManager();
     
		$companySetting = $em->getRepository('CoreBundle:CompanySetting')->find($companySettingId);
		
		$companyLogo = $companySetting->getLogo();
		
		$filePath = $this->container->getParameter('_core_uploadDir').'/company/'.$companyLogo;
		
		if (!empty($companyLogo)) {
			
			return $this->get('core_upload')->download($companyLogo, $filePath);			
		}
     }
}