<?php

namespace Swan\CoreBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Swan\CoreBundle\Entity\Upload;
use Swan\CoreBundle\Form\UploadType;

/**
 * Upload controller.
 *
 */
class UploadController extends Controller
{
    /**
     * Lists and save all client Upload.
     *
     * @param Integer $clientId
     * @param Request $request 
     *
     * @return list
     */
    public function clientUploadListAction($userId, Request $request)
    {
		$em = $this->getDoctrine()->getManager();
		
		$client = $em->getRepository('CoreBundle:User')->find($userId);
        if (!$client) {
            throw $this->createNotFoundException('Unable to find client entity.');
        } 
		
		$uploads = $this->get('core_upload')->findAllUploads('client', $userId);
		
		$upload = new Upload();
		
		$form = $this->createCreateForm($request, $upload);
		
		if ($request->getMethod() == "POST") {
		
            $form->handleRequest($request);

            $errors = $this->get('wbs_core')->validate($upload);
            
            if ($form->isValid() && count($errors)<=0) {
            
				$upload->customPath = $this->get('core_upload')->getUploadPath('client', $userId);
				
				$this->get('core_upload')->addUpload('client', $client, $upload);
				
                $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('addSuccess'));
                
				return $this->redirect($this->generateUrl('_core_client_upload_list', array('userId' => $userId)));
            }
		}
        
        return $this->render('CoreBundle:Upload:clientUploadList.html.twig', array(
            'uploads' => $uploads,
            'form' => $form->createView(),
            'errors' => isset($errors) ? $errors : false,
        ));
    }
    
    /**
     * Creates a form to create a Upload entity.
     *
     * @param Upload $upload The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm($request, Upload $upload)
    {
		$form = $this->createForm(new UploadType($request->getSession()->get('company')), $upload, array(
            'action' => '',
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Upload'));

        return $form;
    }
    
    /**
     * Download client upload
     *
     * @param Integer $clientId The entity
     * @param Integer $uploadId The entity
     *
     * @return upload
     */
    public function downloadClientUploadAction($userId, $uploadId)
    {
		$this->get('core_upload')->downloadFile($uploadId, 'client', $userId);
		
		return $this->redirect($this->generateUrl('_core_client_upload_list', array('userId' => $userId)));
	}
    
    
    /**
     * Deletes a Upload entity.
     *
     * @param Request $request 
     * @param Integer $clientId
     * @param Integer $uploadId 
     *
     * @return redirect
     */
    public function deleteClientUploadAction(Request $request, $userId, $uploadId)
    {
		$this->get('core_upload')->deleteUpload($request, 'client', $userId, $uploadId);
		
		return $this->redirect($this->generateUrl('_core_client_upload_list', array('userId' => $userId)));
    }
}
