<?php

namespace Swan\CoreBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Swan\CoreBundle\Entity\GoalReport;

/**
 * PdfGenerator controller.
 *
 */
class PdfGeneratorController extends Controller
{
    /**
     * Generates pdf
     *
     */
    public function generateAction(Request $request, $userId)
    {
		set_time_limit(0);
		ini_set('memory_limit','-1');
		
		$em = $this->getDoctrine()->getManager();
		
		$user = $em->getRepository("CoreBundle:User")->find($userId);
		
		$userDetails = $this->getUserDetails($user);
		$clientGoals = $em->getRepository("CoreBundle:ClientGoal")->findGoals($userId);
		$inflationRateForUser = $this->get('core_client_goal')->getRateAsPerUserAssumption($userId, 1);
		
        $html = $this->renderView('CoreBundle:PdfGenerator:generate.html.twig', array(
			'userDetails'  => $userDetails,
			'clientGoals' => $clientGoals,
			'inflationRateForUser' => $inflationRateForUser,
		));
		//echo $html; die;
		$pdfFolder = $this->get('kernel')->getRootDir(). "/../data/downloads/goals/pdf/";
		$fileName = $user->getName().'_'.time() . '.pdf';
		$fileNameArr = explode(".", $fileName);
		$customFileName = $fileNameArr[0];
		
		$snappy = $this->get('knp_snappy.pdf');
		$snappy->generateFromHtml($html, $pdfFolder . $fileName);
		
		$goalReport = new GoalReport();
		$goalReport->setFileName($fileName);
		$goalReport->setCustomFileName($customFileName);
		$goalReport->setDateGenerated(new \DateTime());
		$goalReport->setUser($user);
		
		$em->persist($goalReport);
		$em->flush();
		
		return new Response(
			$snappy->getOutputFromHtml($html),
			200,
			array(
				'Content-Type'          => 'application/pdf',
				'Content-Disposition'   => 'attachment; filename="'.$fileName.'"'
			)
		);
    }
	
	private function getUserDetails($user)
	{
		$userName = $this->getSuffix($user).$user->getName().' '.$user->getLastName();
		$userPhoneNum = $user->getMobile();
		$userEmail = $user->getEmail();
		
		$userDetails = array('name' => $userName, 'phone' => $userPhoneNum, 'email' => $userEmail);
		
		return $userDetails;
	}
	
	private function getGender($user)
	{
		if($user->getGender() == 'female') {
			
			$status = false;
		} else {
			
			$status = true;
		}
		
		return $status;
	}
	
	private function getSuffix($user)
	{
		$status = $this->getGender($user);
		if($status) {
			
			$suffix = 'Mr ';
		} else {
			
			$suffix = 'Miss ';
		}
		
		return $suffix;
	}
	
	
}