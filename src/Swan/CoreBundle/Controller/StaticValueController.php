<?php

namespace Swan\CoreBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * StaticValue controller.
 *
 */
class StaticValueController extends Controller
{

    /**
     * Displays a list of static value.
     *
     * @param Request $request The request object
     *
     * @return rendered static value list.
     */
	public function listAction(Request $request)
	{
		$em = $this->getDoctrine()->getManager();

		$companyId = $request->getSession()->get('company');
		
		$brands = $em->getRepository('CoreBundle:Brand')->findAll();
		$usedBrands = $this->get('StaticValue')->getUsedStaticValues('brand');
		
		$suppliers = $em->getRepository('CoreBundle:Supplier')->findBy(array('company' => $companyId));
		$usedSuppliers = $this->get('StaticValue')->getUsedStaticValues('supplier');
		
		$vehicleTypes = $em->getRepository('CoreBundle:VehicleType')->findAll();
		$usedVehicleTypes = $this->get('StaticValue')->getUsedStaticValues('vehicleType');
		
		$incomeTaxAdditions = $em->getRepository('CoreBundle:IncomeTaxAddition')->findAll();
		$usedIncomeTaxAdditions = $this->get('StaticValue')->getUsedStaticValues('incomeTaxAddition');
		
		$energyLabels = $em->getRepository('CoreBundle:EnergyLabel')->findAll();
		$usedEnergyLabels = $this->get('StaticValue')->getUsedStaticValues('energyLabel');
		
		$fuelTypes = $em->getRepository('CoreBundle:FuelType')->findAll();
		$usedFuelTypes = $this->get('StaticValue')->getUsedStaticValues('fuelType');
		
		$documentTypes = $em->getRepository('CoreBundle:DocumentType')->findBy(array('company' => $companyId));
		$usedDocumentTypes = $this->get('StaticValue')->getUsedStaticValues('documentType');
		
		$countries = $em->getRepository('CoreBundle:Country')->findAll();
		$usedCountries = $this->get('StaticValue')->getUsedStaticValues('country');
		
		$leaseCategories = $em->getRepository('CoreBundle:LeaseCategory')->findAll();
        $usedLeaseCategories = $this->get('StaticValue')->getUsedStaticValues('leaseCategory');
		
		$contractStates = $em->getRepository('CoreBundle:ContractState')->findAll();
		$usedContractStates = $this->get('StaticValue')->getUsedStaticValues('contractState');
		
		$calculationTypes = $em->getRepository('CoreBundle:CalculationType')->findAll();
		$usedCalculationTypes = $this->get('StaticValue')->getUsedStaticValues('calculationType');
		
		$costTypes = $em->getRepository('CoreBundle:CostType')->findAll();
		$usedCostTypes = $this->get('StaticValue')->getUsedStaticValues('costType');
		
		$journalCodes = $em->getRepository('CoreBundle:JournalCode')->findBy(array('company' => $companyId));
		$usedJournalCodes = $this->get('StaticValue')->getUsedStaticValues('journalCode');
		
		$orderStatuses = $em->getRepository('CoreBundle:OrderStatus')->findBy(array('company' => $companyId));
		$usedOrderStatuses = $this->get('StaticValue')->getUsedStaticValues('orderStatus');
		
		$ownershipType = $em->getRepository('CoreBundle:OwnershipType')->findAll();
		$usedOwnershipTypes = $this->get('StaticValue')->getUsedStaticValues('ownershipType');
		
		return $this->render('CoreBundle:StaticValue:list.html.twig', array(
			'brands' => $brands,
			'usedBrands' => $usedBrands,
			
			'suppliers' => $suppliers,
			'usedSuppliers' => $usedSuppliers,
			
			'vehicleTypes' => $vehicleTypes,
			'usedVehicleTypes' => $usedVehicleTypes,
			
			'incomeTaxAdditions' => $incomeTaxAdditions,
			'usedIncomeTaxAdditions' => $usedIncomeTaxAdditions,
			
			'energyLabels' => $energyLabels,
			'usedEnergyLabels' => $usedEnergyLabels,
			
			'documentTypes' => $documentTypes,
			'usedDocumentTypes' => $usedDocumentTypes,
			
			'fuelTypes' => $fuelTypes,
			'usedFuelTypes' => $usedFuelTypes,
			
			'countries' => $countries,
			'usedCountries' => $usedCountries,
			
			'leaseCategories' => $leaseCategories,
			'usedLeaseCategories' => $usedLeaseCategories,
			
			'contractStates' => $contractStates,
			'usedContractStates' => $usedContractStates,
			
			'calculationTypes' => $calculationTypes,
			'usedCalculationTypes' => $usedCalculationTypes,
			
			'costTypes' => $costTypes,
			'usedCostTypes' => $usedCostTypes,
			
			'journalCodes' => $journalCodes,
			'usedJournalCodes' => $usedJournalCodes,
			
			'orderStatuses' => $orderStatuses,
			'usedOrderStatuses' => $usedOrderStatuses,

			'ownershipType' => $ownershipType,
			'usedOwnershipTypes' => $usedOwnershipTypes,
		));
	}
    
	/**
	 * Displays a form to create a new static value.
	 *
	 * @param String $formType type of static Value form
	 *
	 */
	public function newAction($formType, Request $request)
	{
		$entity = $this->get('StaticValueEntity')->getEntity($formType);
		
		$form = $this->get('StaticValue')->createStaticValueForm($formType, 
																$this->get('StaticValueEntity')->getEntity($formType), 
																$this->generateUrl('_core_static_value_new', array('formType' => $formType))
															);
		
		if ($request->getMethod() == 'POST') {
		
			return $this->get('StaticValue')->addEditStaticValues($formType, $form, $request, $entity);
		}

		return $this->render('CoreBundle:StaticValue:new.html.twig', array(
			'form'   => $form->createView(),
		));
	}

	/**
	 * Displays a form to edit an existing Brand entity.
	 *
	 * @param String $formType type of static Value form
	 * @param Integer $staticValueId primary id
	 * @param Request $request
	 */
	public function editAction($formType, $staticValueId, Request $request)
	{
		$entity = $this->get('StaticValueEntity')->getEditEntity($formType, $staticValueId);
		
		$form = $this->get('StaticValue')->createStaticValueForm($formType, $entity, $this->generateUrl('_core_static_value_edit', array('formType' => $formType, 'staticValueId' => $staticValueId)));
		
		if ($request->getMethod() == 'POST') {
		
			return $this->get('StaticValue')->addEditStaticValues($formType, $form, $request, $entity);
		}

		return $this->render('CoreBundle:StaticValue:edit.html.twig', array(
			'entity' => $entity,
			'form'   => $form->createView(),
		));
	}
	
    /**
     * Deletes a StaticValue entity.
     *
     * @param String $formType type of static Value form
     * @param Integer $staticValueId primary id
     * @param Request $request
     *
     * @return redirect
     */                         
    public function deleteAction($formType, $staticValueId, Request $request)
    {
        if ($staticValueId>0 && $formType!='') {
        
            $entity = $this->get('StaticValueEntity')->getEditEntity($formType, $staticValueId);

            $this->get('wbs_core')->hardDeleteRecord($request, $entity, $staticValueId);
        }

        return $this->redirect($this->generateUrl('_core_static_value_list'));
    }

}
