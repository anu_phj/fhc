<?php

namespace Swan\CoreBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Swan\CoreBundle\Entity\Company;



/**
 * Company controller.
 *
 */
class CompanyController extends Controller
{

    /**
     * Lists all User companies.
     *
     */
    public function listUserCompaniesAction()
    {
        $securityContext = $this->container->get('security.context');
		$token = $securityContext->getToken();
		$user = $token->getUser();
		$userCompanies = $user->getCompany();		
		
        return $this->render('CoreBundle:Company:listUserCompanies.html.twig', array(
            'userCompanies' => $userCompanies
        ));
    }
    
    /**
     * update company in session   
     */
    public function switchCompanyAction(Request $request)
    {
	    if ($request->getMethod() == 'POST') {	    
		    $company = $request->get('company');
		    if($company!="") {
		    
			    $session = $request->getSession();
			    $session->set('company', $company);
			    			  
			    return $this->redirect($request->headers->get('referer'));
		    }
	    }
    }
}
