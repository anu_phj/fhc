<?php

namespace Swan\CoreBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Swan\CoreBundle\Entity\GoalReport;
use Swan\CoreBundle\Services\GoalReportService;

/**
 * GoalReport controller.
 *
 */
class GoalReportController extends Controller
{
    public function listAction($userId)
	{
		return $this->render('CoreBundle:GoalReport:list.html.twig', array('userId'=>$userId));
	}
	
	public function listAjaxAction($userId)
	{
		set_time_limit(0);
	    ini_set('memory_limit','-1');
		
        $goalReportService = $this->get('core_goal_report');

        $result = $goalReportService->getAllGoalReportsAsArray($userId);
        
        $response = $this->get('core_datatable')->returnDataTableResponse($result);
        
        $response = new Response(json_encode($response));
        $response->headers->set('Content-Type', 'application/json');
        
        return $response;
	}
	
	public function editAction($reportId, Request $request)
	{
		$em = $this->getDoctrine()->getManager();
		$customFileName = $request->request->get('customName');
		
		$goalReport = $em->getRepository("CoreBundle:GoalReport")->find($reportId);
		$result = array('status'=>'error', 'data'=>'error');
		
		$errors = $this->getErrors($customFileName);	
		$result = array('status'=>'error', 'value'=>$errors);
		
		if(count($goalReport)>0 && count($errors)<=0) {
			
			$goalReport->setCustomFileName($customFileName);
			$em->persist($goalReport);
			$em->flush();
			
			$result = array('status'=>'success', 'reportId'=>$reportId, 'customName'=>$goalReport->getCustomFileName());
		}
		
		$response = new Response(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');
        
        return $response;
	}
	
	private function getErrors($customFileName)
	{
		$errors = array();
		
		if(empty($customFileName)) {
			$errors[] = 'Please add a valid custom name';
		}
		
		if(preg_match('/[^a-z_\-0-9]/i', $customFileName))
		{
		  $errors[] = 'Please add a valid custom name';
		}
		
		return $errors;
	}
}