<?php

namespace Swan\CoreBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Request;

class SecurityController extends Controller
{
    /**
     * Index Action
     *
     * @param array $request
     *
     * @return Renders Login form.
     */
    public function indexAction(Request $request)
    {
		$session = $request->getSession();
	
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }
        
        $csrfToken = $this->container->has('form.csrf_provider') ? $this->container->get('form.csrf_provider')->generateCsrfToken('authenticate') : null;

        return $this->render('CoreBundle:Security:login.html.twig', array(
                                    'last_username' => $session->get(SecurityContext::LAST_USERNAME),
                                    "csrf_token" => $csrfToken,
                                    'error'         => $error,
                                ));
    }
}
