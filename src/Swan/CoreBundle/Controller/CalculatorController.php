<?php

namespace Swan\CoreBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Calculator Controller
 *
 */
class CalculatorController extends Controller
{
    
	public function formAction($type, Request $request)
	{
		/*$I=1000;
		$R=7*0.01;
		$n=10*12;
		$tempval=pow((1+$R), ($n/12));
		$tempval2=($tempval-1)/(pow((1+$R),(1/12))-1);
		echo ($I*$tempval2); die;*/

		$data = array();
		$errors = false;
		$result = 0;
		
		$form = $this->createFormBuilder($data)
			->add('years', 'text', array(
					'label' => 'After Years',
					'constraints' => array(
						new NotBlank(array('message' => 'Years can not be blank')),
						new Length(array('max' => 2)),
						new Assert\Type(array(
							'type'    => 'numeric',
							'message' => 'Please add valid number of years',
						)),
					),
				)
			)
			->add('principal', 'text', array(
					'label' => 'Required Amount',
					'constraints' => array(
						new NotBlank(array('message' => 'Principal Amount can not be blank')),
						new Length(array('max' => 11)),
						new Assert\Type(array(
							'type'    => 'numeric',
							'message' => 'Please add valid principal amount',
						)),
					),
				)
			)
			->add('inflationRate', 'text', array(
					'label' => 'Inflation Rate',
					'constraints' => array(
						new NotBlank(array('message' => 'Inflation Rate can not be blank')),
						new Assert\Type(array(
							'type'    => 'numeric',
							'message' => 'Please add valid rate',
						)),
						new Assert\Range(array(
						'min'        => 1,
						'max'        => 20,
						'maxMessage' => 'Enter value between 1 and 20',
						)),
					)
			))
			->add('save', 'submit', array('label' => 'Calculate'))
		->getForm();
		
		
		$titleArr = array(
			'house' => 'Dream House Planning',
			'car' => 'Dream Car Planning',
			'vacation' => 'Dream Vacation Planning',
			'childeducation' => 'Child Education Planning',
			'childmarriage' => 'Child Marriage Planning',
			'sip' => 'Sip-o-meter',
		);
		
		if ($request->getMethod() == 'POST') {
			$form->handleRequest($request);
			if ($form->isValid()) {
				$data = $form->getData();


				if($type == 'sip') {
					$I=$data['principal'];
					$R=$data['inflationRate'];
					$n=$data['years']*12;
					/*frequency = 3 //quarterly
					frequency = 6 //half yearly
					frequency = 12 //yearly*/
					//$frequency = floor($n/12); // yearly

					$i = ($R / 100) / 12;

					$result = (intval($I) * (1 + $i)) * (pow((1+$i), $n) - 1) / $i;
					//$result = intval($I) * ((pow($R / 100 + 1, $frequency) - 1) / (1-(pow($R / 100 + 1,(-1/12))))) * (1 + $i);
				} else {

					$result = $this->get('_core_client_goal')->getFutureValue($data['principal'], $data['inflationRate'], $data['years'], false);
				}

				$result = number_format($result, 2, ".", ",");
			} else {
				$errors = $this->getErrors($form);
			}
		}
		
		return $this->render('CoreBundle:Calculator:form.html.twig', array(
			'form' => $form->createView(),
			'errors' => $errors,
			'result' => $result,
			'titles' => $titleArr,
			'formType' => $type
		));
	}
	
	private function getErrors($form)
	{
		$errorArr = array();
		$errors = $this->get('wbs_core')->getFormErrors($form);
		
		if(count($errors)>0) {
			foreach($errors as $error) {
				$errorArr[] = $error[0];
			}
		}
		
		return $errorArr;
	}
	
	public function retirementPlanningFormAction(Request $request)
	{
		$data = array();
		
		$form = $this->createFormBuilder($data)
			->add('years', 'text', array(
					'label' => 'Years',
					'constraints' => array(
						new NotBlank(array('message' => 'Years can not be blank')),
						new Length(array('max' => 2)),
						new Assert\Type(array(
							'type'    => 'numeric',
							'message' => 'Please add valid number of years',
						)),
					),
				)
			)
			->add('principal', 'text', array(
					'label' => 'Principal Amount',
					'constraints' => array(
						new NotBlank(array('message' => 'Principal Amount can not be blank')),
						new Length(array('max' => 9)),
						new Assert\Type(array(
							'type'    => 'numeric',
							'message' => 'Please add valid principal amount',
						)),
					),
				)
			)
			->add('inflationRate', 'text', array(
					'label' => 'Expected Inflation Rate',
					'constraints' => array(
						new NotBlank(array('message' => 'Inflation Rate can not be blank')),
						new Assert\Type(array(
							'type'    => 'numeric',
							'message' => 'Please add valid rate',
						)),
						new Assert\Range(array(
						'min'        => 5,
						'max'        => 15,
						'maxMessage' => 'Enter value between 5 and 15',
						)),
					)
			))
			->add('save', 'submit', array('label' => 'Calculate'))
		->getForm();

		if ($request->getMethod() == 'POST') {
			$form->handleRequest($request);
			if ($form->isValid()) {
				$data = $form->getData();
				
				$result = $this->get('_core_client_goal')->getFutureValue($data['principal'], $data['inflationRate'], $data['years'], false);
				if($type == 'sip') {
					
					$result = $this->get('_core_client_goal')->calculateFutureValueForMonthlyInterest($data['principal'], $data['inflationRate'], $data['years']);
				}
			} else {
				$errors = $this->getErrors($form);
			}
		}
		
		return $this->render('CoreBundle:Calculator:form.html.twig', array(
			'form' => $form->createView(),
			'errors' => $errors,
			'result' => $result,
		));
	}
	
}