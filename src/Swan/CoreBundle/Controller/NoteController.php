<?php

namespace Swan\CoreBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Note controller.
 *
 */
class NoteController extends Controller
{
    /**
     * Displays a form to create a new Note entity.
     *
     */
    public function addNoteAction(Request $request, $entity, $entityId)
    {
        $noteService = $this->get('core_note');
        
        $url = $this->generateUrl('_core_note_new', array('entity' => $entity, 'entityId' => $entityId));
        $note = $noteService->getEntity($entity);

        $form = $noteService->createNoteForm($noteService->getNoteFormType($entity), $note, $url);

        if ($request->getMethod() == 'POST') {
        
            return $noteService->addNote($request, $form, $note , $entity, $entityId);
        }

        return $noteService->renderNoteFormTwigFile($entity, $entityId, $form);
    }
    
    public function listAction($entity, $entityId)
    {
        $noteService = $this->get('core_note');
        $infoContainer = $noteService->getNotes($this->container->get('security.context')->getToken()->getUser()->getId(), $entityId, $entity);
        
        return $noteService->renderNoteListTwigFile($entity, $entityId, $infoContainer);
    }

    /**
     * Displays a form to edit an existing Note entity.
     *
     */
    public function editNoteAction(Request $request, $entity, $noteId)
    {
        $noteService = $this->get('core_note');
        $note = $noteService->getEntity($entity, $noteId);
        
        if (!$note) {
            throw $this->createNotFoundException('Unable to find Note entity.');
        }
        
        $url = $this->generateUrl('_core_note_edit', array('entity'=> $entity, 'noteId' => $note->getId()));
    
        $form = $noteService->createNoteForm($entity, $note, $url);
        
        if ($request->getMethod() == 'POST') {
            return $noteService->editNote($request, $form, $note);
        }
    }

    
    /**
     * Deletes a Note entity.
     *
     */
    public function deleteNoteAction(Request $request, $entity, $entityId, $noteId)
    {   
        $noteService = $this->get('core_note');
        $entityName = $noteService->getEntity($entity);
        
        $this->get('wbs_core')->hardDeleteRecord($request, $entityName, $noteId, true);
    
        return $noteService->renderNoteListTwigFile($entity, $entityId);
    }
    
}