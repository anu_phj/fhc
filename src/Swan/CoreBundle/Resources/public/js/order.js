$(document).ready(function() {
	var table;
	table = $('#jq-datatables').DataTable({
        "iDisplayLength": parseInt($('#rowsLimit').html()),
		"aoColumnDefs": [
			{ "bSortable": false, "aTargets": [ 8 ] }
		],
		"oLanguage": {
			"sLengthMenu":   $('#perPage').html()+":   _MENU_",
			"sInfo":         $('#showing').html()+" _START_ "+$('#to').html()+" _END_ "+$('#of').html()+" _TOTAL_ "+$('#entries').html(),
			"sInfoEmpty":    $('#showing').html()+" 0 "+$('#to').html()+" 0 "+$('#of').html()+" 0 "+$('#entries').html(),
			"sZeroRecords":  $('#noDataAvailable').html(),
			"oPaginate": {
				"sPrevious": $('#previous').html(),
				"sNext":     $('#next').html(),
			}
		}
	});
	
	$('#jq-datatables_wrapper .table-caption').text($('#ordersList').html());
	$('#jq-datatables_wrapper .dataTables_filter input').attr('placeholder', $('#searchLabel').html());
    
    
    var deleteOrderPath; 
    //Delete vehicle confirm popup
    $("body").delegate( ".deleteOrder", "click", function(event) {       
        deleteOrderPath = $(this).attr('id');
        $('#wbsModal .modal-title').html($('#confirm').html());
        $('#wbsModal .modal-body').html($('#removeMsg').html());
        $('#wbsModal .modal-footer').removeClass('hide');
        $('#wbsModal .modal-footer').html("<button type='button' class='btn btn-default' data-dismiss='modal'>"+$('#cancelLabel').html()+"</button>&nbsp;<button type='button' class='btn btn-primary' id='deleteOrderBtn'>"+$('#deleteLabel').html()+"</button>");
        $('#wbsModal').modal('show');
    });
    
    //Form submit for delete vehicle record 
    $("body").delegate( "#deleteOrderBtn", "click", function(event) {       
        $('#orderFrm').attr('action', deleteOrderPath)
        $('#orderFrm').submit();
    });
});
 
