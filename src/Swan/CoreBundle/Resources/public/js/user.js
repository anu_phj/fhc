$(document).ready(function() {
    var table = $('#jq-datatables').dataTable({
        "iDisplayLength": parseInt($('#rowsLimit').html()),
		"oLanguage": {
			"sLengthMenu":   $('#perPage').html()+":   _MENU_",
			"sInfo":         $('#showing').html()+" _START_ "+$('#to').html()+" _END_ "+$('#of').html()+" _TOTAL_ "+$('#entries').html(),
			"sInfoEmpty":    $('#showing').html()+" 0 "+$('#to').html()+" 0 "+$('#of').html()+" 0 "+$('#entries').html(),
			"sZeroRecords":  $('#noDataAvailable').html(),
			"oPaginate": {
				"sPrevious": $('#previous').html(),
				"sNext":     $('#next').html(),
			}
		}
    });

    customSearchOnEnter(table, 1);

    $('#jq-datatables_wrapper .table-caption').text($('#userListTitle').html());
    $('#jq-datatables_wrapper .dataTables_filter input').attr('placeholder', $('#searchLabel').html());

	var inActivateUserPath, activateUserPath, deleteUserPath;

    //Delete confirm box
    $("body").delegate( ".removeUser", "click", function(event) {
			deleteUserPath = $(this).attr('id');

            $('#wbsModal .modal-title').html($("#confirm").html());
            $('#wbsModal').modal();
            $('#wbsModal').modal({ keyboard: false })
            $('#wbsModal .modal-body').html($('#removeMsg').html());
            $('#wbsModal .modal-footer').html('<button type="button" class="btn btn-primary" id="deleteUserBtn">'+$("#deleteLabel").html()+'</button><button type="button" class="btn btn-default" data-dismiss="modal">'+$("#cancelLabel").html()+'</button>');
            $('#wbsModal').modal('show');
    });

    $("body").delegate( ".activeUser", "click", function(event) {

		activateUserPath = $(this).attr('id');
		$('#wbsModal .modal-title').html($("#confirm").html());
		$('#wbsModal').modal({ keyboard: false })
		$('#wbsModal .modal-body').html($('#activeUserHtml').html());
		$('#wbsModal .modal-footer').html('<button type="button" class="btn btn-primary" id="activeUserBtn">'+$("#confirm").html()+'</button><button type="button" class="btn btn-default" data-dismiss="modal">'+$("#cancelLabel").html()+'</button>');
		$('#wbsModal').modal('show');
    });

    $("body").delegate( ".inactiveUser", "click", function(event) {

        inActivateUserPath = $(this).attr('id');
		$('#wbsModal .modal-title').html($("#confirm").html());
		$('#wbsModal').modal({ keyboard: false })
		$('#wbsModal .modal-body').html($('#inactiveUserHtml').html());
		$('#wbsModal .modal-footer').html('<button type="button" class="btn btn-primary" id="inactiveUserBtn">'+$("#confirm").html()+'</button><button type="button" class="btn btn-default" data-dismiss="modal">'+$("#cancelLabel").html()+'</button>');
		$('#wbsModal').modal('show');
    });

    $("body").delegate( "#activeUserBtn", "click", function(event) {

		$('#userFrm').attr('action', activateUserPath);
		$('#userFrm').submit();
    });

    $("body").delegate( "#inactiveUserBtn", "click", function(event) {

		$('#userFrm').attr('action', inActivateUserPath);
		$('#userFrm').submit();
    });

    $("body").delegate( "#deleteUserBtn", "click", function(event) {

		$('#userFrm').attr('action', deleteUserPath);
		$('#userFrm').submit();
    });


});
