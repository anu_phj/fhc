
var targetArr = [ 12 ];

$(document).ready(function() {
	
	targetArr.push(targetArrForCostPlaceLevel(13));
	
    var table = $('#jq-datatables').DataTable({

		"iDisplayLength": parseInt($('#rowsLimit').html()),
		columnDefs: [
            { type: 'date-sort', targets: [0,6,7] },                                        
            { targets: targetArr, "visible": false }
        ],
		"scrollX": true,
		"oLanguage": {
			"sLengthMenu":   $('#perPage').html()+":   _MENU_",
			"sInfo":         $('#showing').html()+" _START_ "+$('#to').html()+" _END_ "+$('#of').html()+" _TOTAL_ "+$('#entries').html(),
			"sInfoEmpty":    $('#showing').html()+" 0 "+$('#to').html()+" 0 "+$('#of').html()+" 0 "+$('#entries').html(),
			"sZeroRecords":  $('#noDataAvailable').html(),
			"oPaginate": {
				"sPrevious": $('#previous').html(),
				"sNext":     $('#next').html(),
			}
		}
    });
	
	new $.fn.dataTable.ColReorder( table);	

	var colvis = new $.fn.dataTable.ColVis( table, {
		"aiExclude": [],
	});
	
	$( colvis.button() ).insertBefore('div.showHideColumn');
	
	var buttons = new $.fn.dataTable.Buttons( table, {
		buttons: [
			{
				extend: 'csv',
				exportOptions: {
					columns: ':visible',
				},
			},
			{
				extend: 'excel',
				exportOptions: {
					columns: ':visible',
				},
			}
		]
	} );
	
	table.buttons( 0, null ).container().prependTo(
		table.table().container()
	);
	
	
	$( colvis.button() ).attr('title', $('#colVisBtnLabel').html());

	$('.buttons-csv').attr('title', $('#csvBtnLabel').html());
	$('.buttons-excel').attr('title', $('#excelBtnLabel').html());
	
	
	$('#jq-datatables_wrapper .table-caption').text($('#pageTitle').html());
	$('#jq-datatables_wrapper .dataTables_filter input').attr('placeholder', $('#searchLabel').html());
	
	
	
	$('#jq-datatables').bind('DOMNodeInserted', function(event) {
		/*alert('inserted ' + event.target.nodeName + // new node
				' in ' + event.relatedNode.nodeName);*/ // parent
		
		if (event.target.nodeName == 'TD') {
			
			addScroll();
		}
	});
	
	$("#jq-datatables").bind("DOMNodeRemoved",function(event){
		//alert(event.target.nodeName);
		
		if (event.target.nodeName == 'TD') {
			
			addScroll();
		}
	});	
	
	$( window ).resize(function() {
		
		addScroll();
	});
		
	
});
