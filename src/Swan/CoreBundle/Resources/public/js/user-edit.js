$(document).ready(function() {

    var bodyObj = $("body");

    bodyObj.delegate( "#editPassword", "click", function(event) {
        $('#swan_corebundle_user_password_first').removeAttr('disabled');
        $('#swan_corebundle_user_password_second').removeAttr('disabled');
    });

    bodyObj.delegate( ".parentSelectAll", "click", function(event) {
        isChecked = $(this).attr('checked') ? true : false;
        if(isChecked) {
            $(this).parent().find('.costAllocationInput').attr('checked', true);
        } else {
            $(this).parent().find('.costAllocationInput').attr('checked', false);
        }
    });

    bodyObj.delegate( ".selectAll", "click", function(event) {
        isChecked = $(this).attr('checked') ? true : false;
        if(isChecked) {
            $(this).closest('form').find('input:checkbox').attr('checked', true);
        } else {
            $(this).closest('form').find('input:checkbox').attr('checked', false);
        }
    });
});