$(document).ready(function() {
	
	$(".removeRow").live('click', function() {	
		var rowCount = $('.table tr').length;

        var whichtr = $(this).closest("tr");
		if(rowCount>2) {

            var selectedValue = whichtr.find('.asset').val();
            var selectedText = whichtr.find('.asset option:selected').text();

            var assets = $(this).closest('table').find('.asset');
            assets.each(function(){
                $(this).append($('<option>', {
                    value: selectedValue,
                    text: selectedText
                }));
            });

			whichtr.remove();
		} else {

            whichtr.find('.asset').val(0);
            whichtr.find('.assetValue').val('');
        }
	});
	
	$(".asset").live('change', function() {

		var element = $(this);
		var clientGoalId = false;
		
		if(typeof clientGoalId !== 'undefined') {

			clientGoalId = $(this).attr('data');
		}
		
		$.ajax({
            type: "POST",
            url: $('#getAssetValue').html(),
            data: $('#goalForm').serialize()+'assetId='+$(this).val()+'&clientGoalId='+clientGoalId,
            beforeSend: function( xhr ) {
				showLoading();
			},
			success: function(data) {
                hideLoading();
				
				if ($('.alert').length) {
					$('.alert').remove();
				}
				if(data.status == 'success') {
					
					var selectElements = element.closest('tbody').find('.asset').not(element);
					selectElements.each(function(){

                        $(this).find('option[value="'+element.val()+'"]').remove();
					});

					element.parents().eq(1).find('.assetValue').val(data.value);

                    addNotUsedValues();
				} else {
					
					$('.alert-danger').children('span').html(data.value);
					$('.alert-danger').fadeIn('slow');
				}
            }
        });
	});
	
	$(".addRow").live('click', function() {
		
		var assets = $(this).closest('table').find('.asset');
		var assetsValue = $(this).closest('table').find('.assetValue');
		var assetsArray = Array();
		var assetsValueArray = Array();
		var currentElement = $(this);
		//var selectElement = currentElement.closest('tr');
		
		assets.each(function(){
			assetsArray.push($(this).val());
		});
		assetsValue.each(function(){
			assetsValueArray.push($(this).val());
		});
		
		var element = $(this);
		$.ajax({
            type: "POST",
            url: $('#getAssets').html(),
            data: $('#assetForm').serialize()+'assets='+assetsArray+'&assetValue='+assetsValueArray,
            beforeSend: function( xhr ) {
				showLoading();
			},
			success: function(data) {
				hideLoading();

				if (data.status == 'success') {

                    if ($('.alert').length) {
                        $('.alert').remove();
                    }

					var $tr = currentElement.closest('tr');
					var $clone = $tr.clone();
					var selectElement = $clone.find('.asset')
                    /*var removeRowElement = $clone.find('.removeRow')
                    removeRowElement.removeClass('hide');*/
					$.each( data.asset, function( key, value ) {
						selectElement.find('option[value="'+value+'"]').remove();
						selectElement.closest('tr').find('input').val('');
					});

					$clone.find(':text').val('');
					$tr.after($clone);
				} else {

					$('.alert-danger').children('span').html(data.value);
					$('.alert-danger').fadeIn('slow');
				}
            }
        });
	});
});

function addNotUsedValues() {

    var selectedAssets = [];
    //var selectedAssetsCnt = $(this).closest('table').find('.asset');
    $('.asset').each(function(){

        selectedAssets.push(this.value);
    });

    var notSelectedAssets = {};
    $(".defaultAssets > option").each(function() {

        if(jQuery.inArray(this.value, selectedAssets) === -1) {

            notSelectedAssets[this.value] = this.text;
        }
    });

    if(!$.isEmptyObject(notSelectedAssets)) {

        $('.asset').each(function(){

            var currentAsset = $(this);
            $.each(notSelectedAssets, function( index, value ) {

                //alert(index+'  '+value)
                //if($("option:[value='"+index+"']", currentAsset).length == 0) {

                //}
                var addIt = 1;
                $("option", currentAsset).each(function(){

                    if($(this).val()==index) {

                        addIt = 0;
                    }
                });

                if(addIt==1) {
                    currentAsset.append($('<option>', {
                        value: index,
                        text: value
                    }));
                }
            });
        });
    }
};