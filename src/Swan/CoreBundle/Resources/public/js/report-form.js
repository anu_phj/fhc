$(document).ready(function(){
	$(".datepickerControl").keypress(function (e) {
		e.preventDefault();
		if (checkValidDate($(this).find("input").val()) == false) {
			$(this).datepicker("setDate", new Date);
		}
		return false;
	});
			
	var fields = $(".reportForm :input");
	$('#searchAllocations').click(function(){
		
		var errorMsg = '';
		var reportFormStartDate = $('#reportStartDate').val();
		var reportFormStartDateArr = $('#reportStartDate').val().split('-');
		var reportFormStartDateNumber = reportFormStartDateArr[2]+reportFormStartDateArr[1]+reportFormStartDateArr[0];
		var reportFormEndDate = $('#reportEndDate').val();
		var reportFormEndDateArr = $('#reportEndDate').val().split('-');
		var reportFormEndDateNumber = reportFormEndDateArr[2]+reportFormEndDateArr[1]+reportFormEndDateArr[0];
		
		if (reportFormEndDate=='' || reportFormStartDate=='') {
		
			errorMsg = 'selectStartDateAndEndDate';
			
		} else if( reportFormStartDateNumber > reportFormEndDateNumber) {
			errorMsg = 'InvalidSelectedDates';
		
		}
		
		if (errorMsg!='') {
		
			$('#reportFormErroMsg span').html(errorMsg);
			$('#reportFormErroMsg').removeAttr('style');
		
			return false;
		} else {
			$('#reportFormErroMsg').hide();
		}
	});
})