init.push(function () {
    if (! $('html').hasClass('ie8')) {
        $('#corebundle_allocation_mutation_email_body').summernote({
            height: 500,
            tabsize: 2,
            codemirror: {
                theme: 'monokai'
            }
        });
    }
    $('#summernote-boxed').switcher({
        on_state_content: '<span class="fa fa-check" style="font-size:11px;"></span>',
        off_state_content: '<span class="fa fa-times" style="font-size:11px;"></span>'
    });
    $('#summernote-boxed').on($('html').hasClass('ie8') ? "propertychange" : "change", function () {
        var $panel = $(this).parents('.panel');
        if ($(this).is(':checked')) {
            $panel.find('.panel-body').addClass('no-padding');
            $panel.find('.panel-body > *').addClass('no-border');
        } else {
            $panel.find('.panel-body').removeClass('no-padding');
            $panel.find('.panel-body > *').removeClass('no-border');
        }
    });
});