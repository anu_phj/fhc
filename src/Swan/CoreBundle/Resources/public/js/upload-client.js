$(document).ready(function() {

	var table = $('#jq-datatables').dataTable({
      "iDisplayLength": parseInt($('#rowsLimit').html()),  
      "aoColumnDefs": [
            { "bSortable": false, "aTargets": [ 2 ] }
        ],
		"oLanguage": {
			"sLengthMenu":   $('#perPage').html()+":   _MENU_",
			"sInfo":         $('#showing').html()+" _START_ "+$('#to').html()+" _END_ "+$('#of').html()+" _TOTAL_ "+$('#entries').html(),
			"sInfoEmpty":    $('#showing').html()+" 0 "+$('#to').html()+" 0 "+$('#of').html()+" 0 "+$('#entries').html(),
			"sZeroRecords":  $('#noDataAvailable').html(),
			"oPaginate": {
				"sPrevious": $('#previous').html(),
				"sNext":     $('#next').html(),
			}
		}
    });
	
	customSearchOnEnter(table, 1);

	$('#jq-datatables_wrapper .table-caption').text($('#clientUploadListTitle').html());
	$('#jq-datatables_wrapper .dataTables_filter input').attr('placeholder', $('#searchLabel').html());
	
    //Delete confirm box
	var deleteClientUploadPath;
    $("body").delegate( ".deleteClientUpload", "click", function(event) {
        deleteClientUploadPath = $(this).attr('id');
			
            var frm; 
            frm += $('#removeMsg').html();          
            var confirm = $("#confirm").html();
            $('#wbsModal .modal-title').html(confirm);
            $('#wbsModal').modal();
            $('#wbsModal').modal({ keyboard: false })
            $('#wbsModal .modal-body').html(frm);
            $('#wbsModal .modal-footer').html('<button type="button" class="btn btn-primary" id="deleteClientUploadBtn">'+$("#deleteLabel").html()+'</button><button type="button" class="btn btn-default" data-dismiss="modal">'+$("#cancelLabel").html()+'</button>');
            $('#wbsModal').modal('show');
    });
	
    $("body").delegate( "#deleteClientUploadBtn", "click", function(event) {
		$('#deleteClientUploadFrm').attr('action', deleteClientUploadPath);
		$('#deleteClientUploadFrm').submit();
    });

});