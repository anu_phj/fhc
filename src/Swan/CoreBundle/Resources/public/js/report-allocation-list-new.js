var targetArr = [ 19 ];

$(document).ready(function() {
	
	targetArr.push(targetArrForCostPlaceLevel(19));
	
	var dataArr = [
			{ "data": "employeeId"},
			{ "data": "initials"},
			{ "data": "familyNamePrefix"},
			{ "data": "familyName"},
			{ "data": "costAllocationLevelOne"},
			{ "data": "costPlace"},
			{ "data": "costAllocationLevelThree"},
            { "data": "dateStart"},
            { "data": "dateEnd"},
			{ "data": "licensePlate"},
			{ "data": "fiscalValue"},
			{ "data": "incomeTaxAddtion"},
			{ "data": "supplier"},
			{ "data": "fixedPersonalContribution"},
			{ "data": "budgetOverdraw"},
			{ "data": "eb1"},
			{ "data": "eb2"},
			{ "data": "mutationDate"},
			{ "data": "type"},
			{ "data": "totalEB"},
    ];
	
	var costAllocationCount = $('#costAllocationCount').html();
    
    for (i = 0; i < costAllocationCount; ++i) {
        dataArr.push({ "data": 'costAllocationLevel'+i})
    }
	
	var url = $("#allocationAjaxListUrl").val();
	
	var table = $('#jq-datatables').DataTable({
		"ajax": url+"?startDate="+$("#reportStartDate").val()+"&endDate="+$("#reportEndDate").val(),
        "type": "POST",   
        //"serverSide": true,
        "processing": true,
        "aoColumns": dataArr,
        "deferRender": true,
		"iDisplayLength": parseInt($('#rowsLimit').html()),
		columnDefs: [
            { type: 'date-sort', targets: [7,8,17] },                                        
            { targets: targetArr, "visible": false }
        ],
		"scrollX": true,
		"oLanguage": {
			"sLoadingRecords": "Loading...",
			"sLengthMenu":   $('#perPage').html()+":   _MENU_",
			"sInfo":         $('#showing').html()+" _START_ "+$('#to').html()+" _END_ "+$('#of').html()+" _TOTAL_ "+$('#entries').html(),
			"sInfoEmpty":    $('#showing').html()+" 0 "+$('#to').html()+" 0 "+$('#of').html()+" 0 "+$('#entries').html(),
			"sZeroRecords":  $('#noDataAvailable').html(),
			"oPaginate": {
				"sPrevious": $('#previous').html(),
				"sNext":     $('#next').html(),
			}
		}
    });
	
	customSearchOnEnter(table);
	
	new $.fn.dataTable.ColReorder( table);	

	var colvis = new $.fn.dataTable.ColVis( table, {
		"aiExclude": [],
	});
	
	$( colvis.button() ).insertBefore('div.showHideColumn');
	
	var buttons = new $.fn.dataTable.Buttons( table, {
		buttons: [
			{
				extend: 'csv',
				exportOptions: {
					columns: ':visible',
				},
			},
			{
				extend: 'excel',
				exportOptions: {
					columns: ':visible',
				},
			}
		]
	} );
	
	table.buttons( 0, null ).container().prependTo(
		table.table().container()
	);
	
	
	$( colvis.button() ).attr('title', $('#colVisBtnLabel').html());

	$('.buttons-csv').attr('title', $('#csvBtnLabel').html());
	$('.buttons-excel').attr('title', $('#excelBtnLabel').html());
	
	
	$('#jq-datatables_wrapper .table-caption').text($('#pageTitle').html());
	$('#jq-datatables_wrapper .dataTables_filter input').attr('placeholder', $('#searchLabel').html());
	
	
	
	$('#jq-datatables').bind('DOMNodeInserted', function(event) {
		/*alert('inserted ' + event.target.nodeName + // new node
				' in ' + event.relatedNode.nodeName);*/ // parent
		
		if (event.target.nodeName == 'TD') {
			
			addScroll();
		}
	});
	
	$("#jq-datatables").bind("DOMNodeRemoved",function(event){
		//alert(event.target.nodeName);
		
		if (event.target.nodeName == 'TD') {
			
			addScroll();
		}
	});	
	
	$( window ).resize(function() {
		
		addScroll();
	});
		
	
});