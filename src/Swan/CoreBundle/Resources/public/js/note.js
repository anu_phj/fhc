$(document).ready(function() {
    
    var deleteForm = '';
    var noteId = '';
    
    updateNotes(); //LIST OF NOTES
    
    /*
     * Function that will enable submit when user
     * enters any text
     */
    $('.createNote').prop('disabled',true);
    $('#swan_corebundle_note_body, #swan_corebundle_vehicle_note_body').keyup(function(){
        $('.createNote').prop('disabled', this.value == "" ? true : false);     
    });
    
    /*
     * Function that will send ajax request 
     * to create a new note
     * 
     */
    $('.createNote').click(function(event) {
        var noteForm = $(this).closest('form');
        $.ajax({
            type: "POST",
            url: noteForm.attr('action'), 
            data: noteForm.serialize(),
            beforeSend: function( xhr ) {
                console.log('loading')
                hideMessage();
            },
            success: function(data) {
                $('#swan_corebundle_note_body').val('');
                $('#swan_corebundle_vehicle_note_body').val('');
                $('.createNote').attr('disabled', true);
                successMessage(data.success);
                setTimeout('hideMessage()', 1500);
                updateNotes();
            }
        });    
        return false;
    });
    
    /*
     * This function open a modal popup
     * for the confirmation of deleting node
     * 
     */
    $( "body" ).delegate( ".deleteNote", "click", function(event) {
        deleteForm = $(this).attr('id');
        $('#deleteFrm').attr('action', deleteForm);
        
        var modal = $('body').find('#wbsModal .modal-dialog');
        modal.find('.modal-header .modal-title').html($("#confirm").html());
        modal.find('.modal-body').html($('#removeMsg').html());
        modal.find('.modal-footer').html('<button type="button" class="btn btn-primary" id="confirmNoteDelete">'+$("#deleteLabel").html()+'</button><button type="button" class="btn btn-default" data-dismiss="modal">'+$("#cancelLabel").html()+'</button>');
    });
	
    /*
     * Function to delete  a note through ajax request
     * 
     */
    $(document).on('click', '#confirmNoteDelete', function(event){
        $('#wbsModal').modal('hide');
        $.ajax({
            type: "POST",
            url: $('#deleteFrm').attr('action'), 
            data: $('#deleteFrm').serialize(),
            beforeSend: function( xhr ) {
                console.log('loading')
            },
            success: function(data) {
                $('#swan_corebundle_note_body').val('');
                updateNotes();
            }
        });    
        return false;
    });
    
    /*
     * This function will show up the edit form when user
     * clicks on the edit button
     */
    $(document).on('click', '.editNote', function(event){
	noteId = $(this).attr('href').match(/\d+/);
	$('#noteData .noteTable #showNote_'+noteId).css('display','none');
	$('#noteData .noteTable #editNote_'+noteId).removeAttr('style');
	
	$('.updateNote').closest('.noteTable').find('#swan_corebundle_note_body').keyup(function() {
	    $('.updateNote').prop('disabled', this.value == "" ? true : false);     
	});
	
	return false;
    });
    
    $(document).on('click', '.cancelEdit', function(event){
	var driverNoteId = $(this).attr('id');
	$('#noteData .noteTable #showNote_'+driverNoteId).removeAttr('style');
	$('#noteData .noteTable #editNote_'+driverNoteId).css('display','none');
	
	return false;
    });
    
    /*
     * This function will update the user edited note
     * 
     */
    $(document).on('click', '.updateNote', function(event){
        var editForm = $(this).closest('.noteTable').find('#editNoteForm_'+noteId);
            $.ajax({
                type: "POST",
                url: editForm.attr('action'), 
                data: editForm.serialize(),
                beforeSend: function( xhr ) {
                    console.log('loading')
                    hideMessage();
                },
                success: function(data) {
                    $('#swan_corebundle_note_body').val('');
                    successMessage(data.success);
                    setTimeout('hideMessage()', 1500);
                    updateNotes();
                }
            });    
        return false;
    });
});

function updateNotes() {
    
    var finalUrl = $('#noteListUrl').html();
    
    $.ajax({
        type: "GET",
        url: finalUrl,
        beforeSend: function( xhr ) {
            
        },
        success: function(data) {
            $('#noteData').html(data);                
        }
    });
}
