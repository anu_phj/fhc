var targetArr = [];

$(document).ready(function() {
    
    var dataArr = [
            { "data": "fileName"},
            { "data": "fileCustomName"},
            { "data": "dateGenerated"},
			{ "data": "actions"}
    ];
    
	var url = $("#ajaxListUrl").html();
	
    var table = $('#jq-datatables').DataTable({
		"ajax": url,
		"type": "POST",   
        //"serverSide": true,
        "processing": true,
        "aoColumns": dataArr,
        "iDisplayLength": parseInt($('#rowsLimit').html()),
		columnDefs: [
            { 
                type: 'date-sort', 
                targets: [] 
            },
        ],
		"deferRender": true,
		"scrollX": true,
        "iDisplayLength": parseInt($('#rowsLimit').html()),
		"oLanguage": {
			"sLoadingRecords": $("#loadingText").html(),
			"sProcessing": $("#processingText").html(),											  
            "sLengthMenu":   $('#perPage').html()+":   _MENU_",
			"sInfo":         $('#showing').html()+" _START_ "+$('#to').html()+" _END_ "+$('#of').html()+" _TOTAL_ "+$('#entries').html(),
			"sInfoEmpty":    $('#showing').html()+" 0 "+$('#to').html()+" 0 "+$('#of').html()+" 0 "+$('#entries').html(),
			"sZeroRecords":  $('#noDataAvailable').html(),
			"sInfoFiltered": "",
			"oPaginate": {
				"sPrevious": $('#previous').html(),
				"sNext":     $('#next').html(),
			}
		}
	});
	
	customSearchOnEnter(table);
	
	$('#jq-datatables_wrapper .table-caption').text($('#userListTitle').html());
	$('#jq-datatables_wrapper .dataTables_filter input').attr('placeholder', $('#searchLabel').html());
	
	$('#jq-datatables').bind('DOMNodeInserted', function(event) {
		/*alert('inserted ' + event.target.nodeName + // new node
				' in ' + event.relatedNode.nodeName);*/ // parent
		
		if (event.target.nodeName == 'TD') {
			
			addScroll();
		}
	});
	
	$("#jq-datatables").bind("DOMNodeRemoved",function(event){
		
		if (event.target.nodeName == 'TD') {
			
			addScroll();
		}
	});	
	
	var editReportPath;
	var rowElement;
	var classAttr;
	
	$("body").delegate( ".editReport", "click", function(event) {
			editReportPath = $(this).attr('id');
			customFileName = $(this).attr('data').split('.');
			rowElement = $(this).closest('tr');
			classAttr = $(this).attr('class');

            $('#wbsModal .modal-title').html('Edit');
            $('#wbsModal').modal();
            $('#wbsModal').modal({ keyboard: false })
            $('#wbsModal .modal-body').html('<form name="editForm" id="editForm"><div class="row"><div class="col-md-6 col-sm-6"><input type="text" class="form-control" name="customName" value="'+customFileName[0]+'"/></div></div></form>');
            $('#wbsModal .modal-footer').html('<button type="button" class="btn btn-primary" id="editReportBtn">Save</button><button type="button" class="btn btn-default" data-dismiss="modal">'+$("#cancelLabel").html()+'</button>');
            $('#wbsModal').modal('show');
    });
	
	$("body").delegate( "#editReportBtn", "click", function(event) {
		
		$.ajax({
            type: "POST",
            url: editReportPath,
            data: $('#editForm').serialize(),
            success: function(data) {
                if ($('#wbsModal .modal-body .alert').length) {
					$('#wbsModal .modal-body .alert').remove();
				}
				if(data.status == 'success') {
					$('#wbsModal .modal-body').prepend(
						'<div id="success" class="alert alert-success"><i class="fa fa-check"></i>&nbsp;'+data.status+
						'<button data-dismiss="alert" class="close" type="button" data-original-title="">×</button></div>'
					);
					rowElement.find('span').html(data.customName);
					$('.'+classAttr).attr('data', data.customName);
					$('#wbsModal').modal('hide');
				} else {
					
					$('#wbsModal .modal-body').prepend(
						'<div id="error" class="alert alert-danger"><i class="fa fa-ban"></i>&nbsp;'+data.value+
						'<button data-dismiss="alert" class="close" type="button" data-original-title="">×</button></div>'
					);
					
					return false;
				}
            }
        });
    });
	
	$( window ).resize(function() {
		
		addScroll();
	});
	
});
