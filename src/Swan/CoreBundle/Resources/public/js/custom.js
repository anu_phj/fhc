var init = [];
$(document).ready(function() {
    window.PixelAdmin.start(init);

    $('.datepickerControl').datepicker({
		format: "dd-mm-yyyy",
		autoclose: true,
		clearBtn:true,
		forceParse: false
    }).on('changeDate', function(ev){
        $(this).datepicker('hide');
    });
	
	$(".datepickerControl").keypress(function (e) {
		
		if (e.which == 13) {
			
			e.preventDefault();
			if(checkValidDate($(this).find("input").val()) == false) {
				$(this).datepicker("setDate", new Date);
			}
			return false;
		}
	});	
	
	// Single select
	$("#select2-companies").select2({		
		allowClear: true,
		placeholder: "Select a State"
	});
    
    
    jQuery.extend( jQuery.fn.dataTableExt.oSort, {
        "date-sort-pre": function ( a ) {
            if (a == null || a == "") {
                return 0;
            }
            var ukDatea = a.split('-');
            return (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
        },
    
        "date-sort-asc": function ( a, b ) {
            return ((a < b) ? -1 : ((a > b) ? 1 : 0));
        },
    
        "date-sort-desc": function ( a, b ) {
            return ((a < b) ? 1 : ((a > b) ? -1 : 0));
        }
    });

    $('body').delegate(".multipleDatepickerControl", 'focus', function(){
        $(this).datepicker({
            format: "dd-mm-yyyy",
            autoclose: true,
            clearBtn:true,
            forceParse: false,
        }).on('changeDate', function(ev){
            $(this).datepicker('hide');
        });
    });
 });            
    
//For hide column of dataTables
function hideDataTableColumns(hideColumnsIndexArr, table) {
    
    if (hideColumnsIndexArr.length>0) {
        
        jQuery.each( hideColumnsIndexArr, function( i, val ) {
        table.fnSetColumnVis( val, false );
    });
    }
}

//global functions
function successMessage(msg, elementId) {
    elementId = typeof elementId!='undefined' && elementId!='' ? '#'+elementId : '#successMsg';
    elementId = elementId.replace("##", "#")
	if (msg!='' && typeof msg !='undefined') {
		$(elementId).children('span').html(msg);
		$(elementId).fadeIn('slow');
	}
}

function warningMessage(msg, elementId) { 
    elementId = typeof elementId!='undefined' && elementId!='' ? '#'+elementId : '#warningMsg';
    elementId = elementId.replace("##", "#")
	if (msg!='' && typeof msg !='undefined') {
		$(elementId).children('span').html(msg);
		$(elementId).fadeIn('slow');
	}
}
function hideMessage() {
	$('#errorMsg, #warningMsg, #successMsg, #successMsgPopUp, #errorMsgPopUp, #warningMsgPopUp').fadeOut();
}

function errorMessage(msg, elementId) {
    elementId = typeof elementId!='undefined' && elementId!='' ? '#'+elementId : '#errorMsg';
    elementId = elementId.replace("##", "#")
	if (msg!='' && typeof msg!='undefined') {
		$(elementId).children('span').html(msg);
        $(elementId).fadeIn('slow');
	}
}
function showLoading() {
	$('.loading-image, .load-lightbox').fadeIn();
}

function hideLoading() {
	$('.loading-image, .load-lightbox').fadeOut();
}

function progressBar() {
    var html = '<div class="progress progress-striped active"><div style="width: 100%;" class="progress-bar progress-bar-default"></div></div>';
    
    return html;
}
    
function checkValidDate(text)
{
	var comp = text.split('-');
	var d = parseInt(comp[0], 10);
	var m = parseInt(comp[1], 10);
	var y = parseInt(comp[2], 10);
	var date = new Date(y,m-1,d);
	
	if (date.getFullYear() == y && date.getMonth() + 1 == m && date.getDate() == d) {
		return true
	}
	
	return false;
}

function targetArrForCostPlaceLevel(start) 
{
	var colCount = 0;
	
    $('tr:nth-child(1) th').each(function () {
		
		if ($(this).attr('class') != 'action') {
			
			if ($(this).attr('colspan')) {
				
				colCount += +$(this).attr('colspan');
			} else {
				
				colCount++;
			}
		}
    });
	
	for (var i=start; i<colCount; i++) {
		
		targetArr.push(i);
	}
	
	return targetArr;
}

function getLastValueOfArray(array) 
{
	var value = array[array.length-1];
	
	return value+1;
}

function addScroll() 
{
	
	var w = jQuery('.dataTables_scrollBody').width();
	var c_w = jQuery('.dataTables_scrollBody > table').width();

	if(w >= c_w){
		
		jQuery('.dataTables_scrollBody').css('overflow', 'inherit')
	}
	else{
		
		jQuery('.dataTables_scrollBody').css('overflow', 'auto');
	}	
}

function updateContactPersonHtml(data)
{
    if (typeof data.contactPerson.name != 'undefined') {

        $('#contactPersonDetails').removeClass('hide');
        $('#contactPersonDetails_name').html(data.contactPerson.name);
        $('#contactPersonDetails_address').html(data.contactPerson.address);
        $('#contactPersonDetails_phone').html(data.contactPerson.phone);

        var email = '<a href="mailto:'+data.contactPerson.email+'">'+data.contactPerson.email+'</a>';
        $('#contactPersonDetails_email').html(email);
    } else {
        $('#contactPersonDetails').addClass('hide');
    }
}

function customSearchOnEnter(table, isOld)
{
	$('#jq-datatables_filter input').unbind();
	$('#jq-datatables_filter input').bind('keyup', function(e) {
		if (e.keyCode == 13) {
            if(typeof isOld !== 'undefined') {

                table.fnFilter(this.value);
            } else {
                table.search(this.value).draw();
            }
		}
	});
}