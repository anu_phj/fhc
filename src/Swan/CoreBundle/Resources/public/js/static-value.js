var eventFlag;
$(document).ready(function() {
    
    $("body").delegate( ".addStaticValue", "click", function(event) {  
        var url = $(this).attr('id');
        var title = $(this).attr('title');
        
		eventFlag = 'add';
		
        $.ajax({
            type: "GET",
            url: url,
            data: '',
            beforeSend: function( xhr ) {
                showLoading();
            },
            success: function(data) {
                hideLoading();
                $('#wbsModal .modal-title').html(title);
                $('#wbsModal .modal-body').html(data);
                $('#wbsModal .modal-footer').html("<button type='button' class='btn btn-default' data-dismiss='modal'>"+$('#cancelLabel').html()+"</button>&nbsp;<button type='button' class='btn btn-primary' id='addNewStaticValueFrmBtn'>"+$('#saveLabel').html()+"</button>");
                $('#wbsModal').modal('show');
            }
        });
    });
    
    $( "body" ).delegate( "#addNewStaticValueFrmBtn", "click", function(event) {
		addStaticValueFrm();
    });
	

    $("body").delegate( ".editStaticValue", "click", function(event) {  
        var url = $(this).attr('id');
        var title = $(this).attr('title');
        eventFlag = 'edit';
		
        $.ajax({
            type: "GET",
            url: url,
            data: '',
            beforeSend: function( xhr ) {
                showLoading();
            },
            success: function(data) {
                hideLoading();
                $('#wbsModal .modal-title').html(title);
                $('#wbsModal .modal-body').html(data);
                $('#wbsModal .modal-footer').html("<button type='button' class='btn btn-default' data-dismiss='modal'>"+$('#cancelLabel').html()+"</button>&nbsp;<button type='button' class='btn btn-primary' id='editStaticValueFrmBtn'>"+$('#saveLabel').html()+"</button>");
                $('#wbsModal').modal('show');
            }
        });
    });
    
    $( "body" ).delegate( "#editStaticValueFrmBtn", "click", function(event) {
		editStaticValueFrm();
    });
	
    $( "body" ).delegate( "input", "keypress", function(event) {
		
		if (event.which == 13) {
			
			if (eventFlag == 'edit') {
				
				editStaticValueFrm();
			} else if (eventFlag == 'add') {
				
				addStaticValueFrm();
			}
			
			return false;
		}
    });
    
    var deleteStaticValuePath; 
    //Delete staticValue confirm popup
    $("body").delegate( ".deleteStaticValue", "click", function(event) {       
        deleteStaticValuePath = $(this).attr('id');
        $('#wbsModal .modal-title').html($('#confirm').html());
        $('#wbsModal .modal-body').html($('#removeMsg').html());
        $('#wbsModal .modal-footer').html("<button type='button' class='btn btn-default' data-dismiss='modal'>"+$('#cancelLabel').html()+"</button>&nbsp;<button type='button' class='btn btn-primary' id='deleteStaticValueBtn'>"+$('#deleteLabel').html()+"</button>");
        $('#wbsModal').modal('show');
    });
    
    //Form submit for delete staticValue record 
    $("body").delegate( "#deleteStaticValueBtn", "click", function(event) {       
        $('#staticValueFrm').attr('action', deleteStaticValuePath)
        $('#staticValueFrm').submit();
    });
});

function addStaticValueFrm() {
	var url = $('#addStaticValueFrm').attr('action');
	
	$.ajax({
		type: "POST",
		url: url, 
		data: $('#addStaticValueFrm').serialize(),
		beforeSend: function( xhr ) {
			hideMessage();
		},
		success: function(data) {
			if (data.error) {
				
				errorMessage(data.error, 'errorMsgPopUp')
			} else if(data.success)  {
				
				successMessage(data.success, 'successMsgPopUp')
				$('#wbsModal').animate({ scrollTop: 0 }, 'slow');
				setTimeout(function() {
					$('#wbsModal').modal('hide');
					location.reload();
				}, 1000)
				
			} else {
				errorMessage(data, 'errorMsgPopUp')
			}
		}
	});    
}

function editStaticValueFrm() {
	
	var url = $('#editStaticValueFrm').attr('action');
	
	$.ajax({
		type: "POST",
		url: url, 
		data: $('#editStaticValueFrm').serialize(),
		beforeSend: function( xhr ) {
			hideMessage();
		},
		success: function(data) {
			if (data.error) {
				errorMessage(data.error, 'errorMsgPopUp')
			} else if(data.success)  {
				
				successMessage(data.success, 'successMsgPopUp')
				$('#wbsModal').animate({ scrollTop: 0 }, 'slow');
				setTimeout(function() {
					$('#wbsModal').modal('hide');
					location.reload();
				}, 1000)
				
			} else {
				errorMessage(data, 'errorMsgPopUp')
			}
		}
	});    
}