<?php
namespace Swan\CoreBundle\Handler;

use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Routing\RouterInterface;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Swan\CoreBundle\Entity\User;

class AuthenticationHandler extends ContainerAware implements AuthenticationSuccessHandlerInterface
{
	protected $container;
	protected $em;
	protected $session;
	protected $router;
	
	public function __construct($container, RouterInterface $router) 
	{
		$this->session = $container->get('session');
		$this->container = $container;
		$this->router = $router;
	}
	
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
		$user_entity = $token->getUser();
				
		$userCompany = $user_entity->getCompany();
		$userId = $user_entity->getId();
		
		if(!empty($userCompany)) {
		
			$this->session->set('company', $userCompany[0]->getId());
			$this->session->set('user', $userId);
		}

	    if($user_entity->getIsAdmin()!=1) {

		    $userModuleActions = $user_entity->getUserModuleAction();
		    $userModulesArray = array();
		    $userPermissionsArray = array();
		    if(!empty($userModuleActions)){
			    foreach($userModuleActions as $userModuleAction){
				    $userModulesArray[$userModuleAction->getModuleAction()->getModule()->getSlug()]['actions'][$userModuleAction->getModuleAction()->getId()] = $userModuleAction->getModuleAction()->getUrl();
					$userModulesArray[$userModuleAction->getModuleAction()->getModule()->getSlug()]['slug'][$userModuleAction->getModuleAction()->getId()] = $userModuleAction->getModuleAction()->getSlug();
				    $userPermissionsArray[$userModuleAction->getModuleAction()->getId()] = $userModuleAction->getModuleAction()->getUrl();
			    }
		    }

		    $this->session->set('modules', $userModulesArray);
		    $this->session->set('moduleActions', $userPermissionsArray);
	    }

		$uri = $this->router->generate('_core_homepage');
		
		return new RedirectResponse($uri);
    }
    
}
