<?php
namespace Swan\CoreBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Swan\CoreBundle\Entity\Allocation;

class ImportAllocationsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('import:allocations')
            ->setDescription('For directly import allocations')
            ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /*set_time_limit(0);
        ini_set('memory_limit','-1');
        
		$em = $this->getContainer()->get('doctrine')->getEntityManager();
		
		echo "\n ######### Start import ". date('d-m-Y H:i:s')." ######### \n";
	
        $allErrors = array();
        $successRecord = 0;
        $singleError = '';
        
        $filePath = $this->getContainer()->get('kernel')->getRootDir() . '/../data/uploads/151218_ImportAllocation_Feenstra_per_17-12-15.xlsx';
        if (file_exists($filePath)) {
            $allocationInfo = $this->getContainer()->get('wbs_core')->excelToArray($filePath);
            if (count($allocationInfo)>0) {
            
                foreach($allocationInfo as $key => $allocation) {
                    
                    $rowNumber = $key+2;
                    
                    $rowError = $this->validateData($allocation);
                    
                    if (count($rowError)<=0) {
                    
                        $vehicle = $em->getRepository('CoreBundle:Vehicle')->findByLicensePlate(trim($allocation['vehicle.licence_plate']));
                        if (count($vehicle)>0) {
                        
                            $vehicle = reset($vehicle);
                            $companyId = $vehicle->getCompany()->getId();
                            
                            $driver = $em->getRepository('CoreBundle:Driver')->findBy(array('employeeId' => trim($allocation['driver.employee_id']), 'companyId' => $companyId));
                            
                            if (count($driver)>0) {
                            
                                $driver = reset($driver);
                                $driverCostAllocation = $em->getRepository('CoreBundle:DriverCostAllocation')->findActiveCostAllocation($driver->getId());
                                if (count($driverCostAllocation)>0) {
                                
                                    $driverCostAllocation = reset($driverCostAllocation);
                                    
                                    $startDate = date('Y-m-d',\PHPExcel_Shared_Date::ExcelToPHP($allocation['allocation.date_start']));
                                    $endDate = $allocation['allocation.date_end']!='' ? new \Datetime(date('Y-m-d',\PHPExcel_Shared_Date::ExcelToPHP($allocation['allocation.date_end']))) : NULL;
                                    
                                    $checkExistRecord = $em->getRepository('CoreBundle:Allocation')->findBy(array(
                                                                                                            'vehicleId' => $vehicle->getId(),
                                                                                                            'driverId' => $driver->getId(),
                                                                                                            'dateStart' => new \Datetime($startDate),
                                                                                                            'dateEnd' => $endDate,
                                                                                                        ));
                                    if (count($checkExistRecord)<=0) {
                                    
                                        $criteria = array();
                                        $criteria['dateActiveFinal'] = $startDate;
                                        $criteria['dateActive'] = $startDate;
                                        $criteria['vehicleId'] = $vehicle->getId();
                                        $criteria['costAllocationId'] = $driverCostAllocation->getCostAllocationId();
                                        
                                        $getActiveCostAllocation = $em->getRepository('CoreBundle:VehicleCostAllocation')->findActiveCostAllocation($criteria);
                                        if (count($getActiveCostAllocation)>0) {
                                            $rowError[] = 'vehicle allocation added and vehicle('.$vehicle->getId().') already has active cost allocation('.$driverCostAllocation->getCostAllocationId().')';
                                        } 
                                        
                                        $allocationObj = new Allocation();
                                        
                                        $allocationObj->setDateStart(new \Datetime($startDate));
                                        $allocationObj->setDateEnd($endDate);
                                        $allocationObj->setFixedPersonalContribution($allocation['allocation.fixed_personal_contribution']);
                                        $allocationObj->setBudgetOverdraw($allocation['allocation.budget_overdraw']);
                                        $allocationObj->setEb1($allocation['allocation.eb1']);
                                        $allocationObj->setEb2($allocation['allocation.eb2']);
                                        $allocationObj->setVehicle($vehicle);
                                        $allocationObj->setDriver($driver);
                                        $allocationObj->setPrivateUse(NULL);
                                        $allocationObj->setMutationDate(new \Datetime($startDate));
                                        
                                        $em->persist($allocationObj);
                                        $em->flush();
                                        
                                        //$this->getContainer()->get('CostAllocation')->assignDriverCostAllocationToVehicle($allocationObj->getDriver(), $allocationObj->getVehicle(), array('dateStart' => $allocationObj->getDateStart()->format('Y-m-d')));
                                        
                                        //Start Add CostAllocation
                                        $allocations = $em->getRepository('CoreBundle:Allocation')->findBy(
                                                                                                    array('vehicleId' => $vehicle->getId()), 
                                                                                                    array('dateStart' => 'ASC')
                                                                                                );
                                        if (count($allocations)>0) {
                                            $costAllocations = $em->getRepository('CoreBundle:VehicleCostAllocation')->findByVehicleId($vehicle->getId());
                                            if (count($costAllocations)>0) {
                                                foreach($costAllocations as $costAllocation) {
                                                    
                                                    $em->remove($costAllocation);
                                                    $em->flush();
                                                }
                                            }
                                            
                                            foreach($allocations as $vehicleAllocation) {
                                                
                                                $otherData = array('dateStart' => $vehicleAllocation->getDateStart()->format('Y-m-d'), 'allocationStartDate' =>$vehicleAllocation->getDateStart()->format('Y-m-d'), "importAllocation" => true);
                                                $this->getContainer()->get('CostAllocation')->assignDriverCostAllocationToVehicle($vehicleAllocation->getDriver(), $vehicle, $otherData);
                                            }
                                        }
                                        //End Add CostAllocation
                                        
                                        $successRecord++;
                                    } 
                                } else {
                                    $rowError[] = 'driver has not active cost allocation';
                                }
                                
                            } else {
                                $rowError[] = 'driver does not exist in database';
                            }
                        } else {
                            $rowError[] = 'vehicle does not exist in database';
                        }
                    } 
                    
                    if (count($rowError)>0) $allErrors[$rowNumber] = $rowError;
                }
                
            } else {
                $singleError = 'Data no found in excel file';
            } 
            
        } else {
            $singleError = 'File not exist';
        }
        
        if ($singleError!='') {
            echo "\n".$singleError."\n\n"; die();
        }
        
        
        echo "\n".$successRecord." record(s) inserted successfully.\n";
        
        if (array_filter($allErrors)>0) {
        
            echo "\nErrors Detail \n\n";
            foreach($allErrors as $rowNumber => $finalArr) {
                echo "Row Number - ".$rowNumber."\n";
                echo implode("\n", $finalArr);
                echo "\n\n";
            }
        }
        
        
        echo "\n".$successRecord." record(s) inserted successfully.\n\n";
        
        
        echo "\n ######### End import ". date('d-m-Y H:i:s')." ######### \n";
        */
    }
    
    
    private function validateData($allocation)
    {
        $rowError = array();
        if (trim($allocation['vehicle.licence_plate'])=='') {
            $rowError[] = 'licence_plate is blank';
        } 
        if (trim($allocation['driver.employee_id'])=='') {
            $rowError[] = 'employee_id is blank';
        } 
        if (trim($allocation['allocation.date_start'])=='') {
            $rowError[] = 'date_start is blank';
        } 
        
        return $rowError;
    }
}