<?php
namespace Swan\CoreBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Swan\CoreBundle\Entity\User;
use Swan\CoreBundle\Entity\Company;

class CreateDummyUserCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('dummy:user')
            ->setDescription('For first time admin user')
            ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
		$em = $this->getContainer()->get('doctrine')->getEntityManager();
		
		$user = $em->getRepository('CoreBundle:User')->find(1);
		
		if (count($user)<=0) {
		
			/* Add company */
			$company = $em->getRepository('CoreBundle:Company')->find(1);
			if (count($company)<=0) {
			
				$company = new Company();
				$company->setName('Swan');
				
				$em->persist($company);
				$em->flush();
			}
			
			$user = new User();
			$user->setName('Admin');
			$user->setEmail('admin@fhc.com');
			$user->setPassword(sha1('admin'));
			$user->setIsAdmin(true);
			$user->setInactive(false);
			$user->setDeleted(false);
			$user->addCompany($company);
			
			$em->persist($user);
			$em->flush();
			
			$text = '
			Login with 
			email:		admin@swan.com
			password :	admin
			';
		} else {
			$text = 'User already exist.';
		}
		
		$output->writeln($text);
    }
}