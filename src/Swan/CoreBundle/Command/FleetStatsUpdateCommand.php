<?php
namespace Swan\CoreBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Swan\CoreBundle\Entity\FleetStat;

class FleetStatsUpdateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('cron:fleetStatsUpdate')
            ->setDescription('For update fleet stats into database')
            ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getEntityManager();
        
        $companies = $em->getRepository('CoreBundle:Company')->findAll();
        
        $result = array();
        foreach($companies as $company) {
            
            $fleetStats = $this->getContainer()->get('core_fleet_stat')->getFleetStatByDate($company->getId());
            if (count($fleetStats)>0) {
                
                $fleetStats = reset($fleetStats);
                $result[] = $this->updateFleetStats($fleetStats, $company);
                
            } else {
                $result[] = 'No record found for company '.$company->getName();
            }
        }
        
        $output->writeln(implode("\n", $result));
    }
    
    private function updateFleetStats($fleetStats, $company)
    {
        $em = $this->getContainer()->get('doctrine')->getEntityManager();
        
        $totalActiveVehicle = $fleetStats['totalActiveVehicle'];
        $totalActiveVehicleLease = $fleetStats['totalActiveVehicleLease'];
        $totalActiveVehicleLeaseLimited = $fleetStats['totalActiveVehicleLeaseLimited'];
        $totalActiveVehicleRent = $fleetStats['totalActiveVehicleRent'];
        $totalActiveVehicleFuel = $fleetStats['totalActiveVehicleFuel'];
        $totalActiveVehicleOwnedByCompany = $fleetStats['totalActiveVehicleOwnedByCompany'];
        
        $existFleetStats = $em->getRepository('CoreBundle:FleetStat')->findBy(array(
            'dateGenerated' => new \Datetime(),
            'company' => $company
        ));
        
        if (count($existFleetStats)<=0) {
            
            $fleetStats = new FleetStat();
                                        
            $fleetStats->setTotalActiveVehicle($totalActiveVehicle);
            $fleetStats->setTotalActiveVehicleLease($totalActiveVehicleLease);
            $fleetStats->setTotalActiveVehicleLeaseLimited($totalActiveVehicleLeaseLimited);
            $fleetStats->setTotalActiveVehicleRent($totalActiveVehicleRent);
            $fleetStats->setTotalActiveVehicleFuel($totalActiveVehicleFuel);
            $fleetStats->setTotalActiveVehicleOwnedByCompany($totalActiveVehicleOwnedByCompany);
            $fleetStats->setCompany($company);
            $fleetStats->setDateGenerated(new \Datetime());
            
            $em->persist($fleetStats);
            $em->flush();
            
            $text = 'Record inserted succesfully for company '.$company->getName();
            
        } else {
            $text = 'Record allready inserted for company '.$company->getName();
        }
        
        return $text;
    }
}