<?php

namespace Swan\CoreBundle\Extension;

use Doctrine\Common\Persistence\ManagerRegistry;
use Swan\CoreBundle\Entity\DriverImport;
use Swan\CoreBundle\Entity\VehicleImport;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Swan\CoreBundle\Entity\CostAllocation;
use Swan\CoreBundle\Entity\Vehicle;
use Swan\CoreBundle\Entity\ClientGoal;

class WebsiteExtension extends \Twig_Extension {
	
	protected $mr;
	protected $container;
	
	public function __construct(ManagerRegistry $mr, Container $container) 
	{
		$this->mr = $mr;
		$this->container = $container;
	}
    
    public function getFilters() 
    {
        return array(
			new \Twig_SimpleFilter('costAllocationBreadcrumb', array($this, 'breadcrumbLinks')),
			new \Twig_SimpleFilter('validateEntity', array($this, 'validateEntity')),
			new \Twig_SimpleFilter('costAllocationName', array($this, 'costAllocationName')),
			new \Twig_SimpleFilter('vehicleLicense', array($this, 'vehicleLicense')),
			new \Twig_SimpleFilter('getUserAccess', array($this, 'getUserAccess')),
			new \Twig_SimpleFilter('checkUserAccess', array($this, 'checkUserAccess')),
	        new \Twig_SimpleFilter('costAllocationStatus', array($this, 'getCostAllocationStatus')),
	        new \Twig_SimpleFilter('getVehicleUpdatedColumns', array($this, 'getVehicleUpdatedColumns')),
	        new \Twig_SimpleFilter('getDriverUpdatedColumns', array($this, 'getDriverUpdatedColumns')),
	        new \Twig_SimpleFilter('checkValidDate', array($this, 'checkValidDate')),
	        new \Twig_SimpleFilter('getActiveCostAllocation', array($this, 'getActiveCostAllocation')),
	        new \Twig_SimpleFilter('getMaxLengthOfArray', array($this, 'getMaxLengthOfArray')),
	        new \Twig_SimpleFilter('getVehicle', array($this, 'getVehicle')),
	        new \Twig_SimpleFilter('getInvoiceLineAllocations', array($this, 'getInvoiceLineAllocations')),
	        new \Twig_SimpleFilter('formedDate', array($this, 'formedDate')),
	        new \Twig_SimpleFilter('formedAmount', array($this, 'formedAmount')),
	        new \Twig_SimpleFilter('processFunction', array($this, 'processFunction')),
	        new \Twig_SimpleFilter('checkActiveAllocation', array($this, 'checkActiveAllocation')),
	        new \Twig_SimpleFilter('contractCalculations', array($this, 'contractCalculations')),
	        new \Twig_SimpleFilter('getMaxCostAllocatonLevelCount', array($this, 'getMaxCostAllocatonLevelCount')),
	        new \Twig_SimpleFilter('getCostAllocationNameLevelWise', array($this, 'getCostAllocationNameLevelWise')),
	        new \Twig_SimpleFilter('validateCompanySettings', array($this, 'validateCompanySettings')),
	        new \Twig_SimpleFilter('costAllocationBreadcrumbHtml', array($this, 'costAllocationBreadcrumbHtml')),
	        new \Twig_SimpleFilter('driverFullName', array($this, 'driverFullName')),
	        new \Twig_SimpleFilter('getInvoiceLineAllocationsDetail', array($this, 'getInvoiceLineAllocationsDetail')),
	        new \Twig_SimpleFilter('getInvoiceLinesStatus', array($this, 'getInvoiceLinesStatus')),
	        new \Twig_SimpleFilter('getInvoiceLinesByInvoiceNumber', array($this, 'getInvoiceLinesByInvoiceNumber')),
	        new \Twig_SimpleFilter('getCostAllocationWithAllocation', array($this, 'getCostAllocationWithAllocation')),	    
	        new \Twig_SimpleFilter('getAllocationTotalEB', array($this, 'getAllocationTotalEB')),
	        new \Twig_SimpleFilter('getcostAllocationBreadcrumbHtmlWithEntity', array($this, 'getcostAllocationBreadcrumbHtmlWithEntity')),
	        new \Twig_SimpleFilter('getcostAllocationWithEntity', array($this, 'getcostAllocationWithEntity')),
	        new \Twig_SimpleFilter('getcostAllocationContactPerson', array($this, 'getcostAllocationContactPerson')),
	        new \Twig_SimpleFilter('getBinaryValueToStringValue', array($this, 'getBinaryValueToStringValue')),
	        new \Twig_SimpleFilter('costAllocationForPoolOrGeneralUse', array($this, 'costAllocationForPoolOrGeneralUse')),
	        new \Twig_SimpleFilter('getContractAttributeName', array($this, 'getContractAttributeName')),
	        new \Twig_SimpleFilter('getContractForeignAttributeName', array($this, 'getContractForeignAttributeName')),
			new \Twig_SimpleFilter('getMonthName', array($this, 'getMonthName')),
			new \Twig_SimpleFilter('calculateGoalMonths', array($this, 'calculateGoalMonths')),
			new \Twig_SimpleFilter('getFutureValue', array($this, 'getFutureValue')),
			new \Twig_SimpleFilter('getRateAsPerUserAssumptionInTwig', array($this, 'getRateAsPerUserAssumptionInTwig')),
			new \Twig_SimpleFilter('getTotalAssetValueForAGoal', array($this, 'getTotalAssetValueForAGoal')),
			new \Twig_SimpleFilter('calculateFutureValueForMonthlyInterestInTwig', array($this, 'calculateFutureValueForMonthlyInterestInTwig')),
			new \Twig_SimpleFilter('calculateMonthsForAssets', array($this, 'calculateMonthsForAssets')),
	        new \Twig_SimpleFilter('checkIfRoleSelected', array($this, 'checkIfRoleSelected')),
			new \Twig_SimpleFilter('getPermittedActions', array($this, 'getPermittedActions')),
			new \Twig_SimpleFilter('getCompanyNameFromId', array($this, 'getCompanyNameFromId')),
        );
    }
	
	/**
	 * get array of parents
	 */
	private function costAllocationParentTree($costAllocationId)
	{
		return $this->container->get('CostAllocation')->costAllocationParentTree($costAllocationId);
	}
	
	/**
	 * get breadcrumLinks
	 */
	public function breadcrumbLinks($costAllocationId) 
	{
		$parentItems = array($costAllocationId);

		$listParentItems = $this->costAllocationParentTree($costAllocationId);

		$em = $this->mr->getManagerForClass(get_class(new CostAllocation()));
		
		$parentArr = array();
		
		if(!empty($listParentItems)) {

			$parentItems = array_merge($listParentItems,$parentItems);
		}

		foreach($parentItems as $parentItem) {

			$costAllocation = $em->getRepository('CoreBundle:CostAllocation')->find($parentItem);
			if (!empty($costAllocation)) {

				$parentArr[] = $costAllocation->getName();
			}
		}
		
		return $this->costAllocationBreadcrumbHtml($parentArr);
	}
	
	public function costAllocationBreadcrumbHtml($parentItems)
	{
		if (count($parentItems) >0) {
			
			$last = end($parentItems);

			$breadcrumb = '<ul class="breadcrumb">';
		
			foreach($parentItems as $parentItem) {
			
				$class = "";
				
				if($last == $parentItem) {
					$class = "active";
				}
				
				$breadcrumb	.= '<li class = "'.$class.'">'.$parentItem. '</li> ';
			}
			
			$breadcrumb .= "</ul>";
			
			return $breadcrumb;
		}
	}
	
	
	public function getName() 
    {
        return 'website_extension';
    }

	public function validateEntity($entity, $entityType)
	{
		$entityField = 'get'.ucfirst($entityType).'Id';
		$setEntityField = 'set'.ucfirst($entityType).'Id';
		if($entity->$entityField()=='') {

			$entity->$setEntityField(false);
		}

		$validator = $this->container->get('validator');
		$errors = $validator->validate($entity);

		return $errors;
	}
	
	private function getCostAllocation($costAllocationId)
	{
		$em = $this->mr->getManagerForClass(get_class(new CostAllocation()));
		
		$costAllocation = $em->getRepository('CoreBundle:CostAllocation')->find($costAllocationId);
		
		return $costAllocation;
	}
	
	public function costAllocationName($costAllocationId)
	{
		$costAllocation = $this->getCostAllocation($costAllocationId);

		return count($costAllocation)>0 ? $costAllocation->getName() : '-';
	}
	
	public function vehicleLicense($vehicleId)
    {
        $em = $this->mr->getManagerForClass(get_class(new Vehicle()));
        
        $vehicle = $em->getRepository('CoreBundle:Vehicle')->find($vehicleId);
        if (count($vehicle)>0) {
            return $vehicle->getLicensePlate();
        }
        

        return false;
    }
    
    public function getUserAccess()
    {
        $securityContext = $this->container->get('security.context')->getToken();
        $user = $securityContext->getUser();
        
        $userAccess = $this->container->get('UserAccess')->getAcess($user);
        
        return $userAccess;
    }
    
    public function checkUserAccess($accessObject, $costAllocationId, $action=false)
    {
        $access = false; 
        
        $securityContext = $this->container->get('security.context')->getToken();
        $user = $securityContext->getUser();
        if ($user->getIsAdmin()==true) {
            $access = true; 
        } else if (count($accessObject)>0 && $costAllocationId>0 && $action!==false) {
            if (isset($accessObject[$costAllocationId]) && count($accessObject[$costAllocationId])>0) {
                $userCostAllocationAccess = $accessObject[$costAllocationId];
                
                return in_array($action, $userCostAllocationAccess);
            }
        }
        
        return $access;
    }
    
	public function getCostAllocationStatus($costAllocation)
	{
		return count($costAllocation)>0 ? $costAllocation->getInactive() : 0;
	}
	
	/**
	 * get updated columns
	 *
	 * @param Integer $vehicleImportId
	 *
	 * return array
	 */
	public function getVehicleUpdatedColumns($vehicleImportId)
	{
		return $this->container->get('core_process_import')->getUpdatedColumns($vehicleImportId, 'VehicleImport');
	}

	/**
	 * get updated columns
	 *
	 * @param Integer $driverImportId
	 *
	 * return array
	 */
	public function getDriverUpdatedColumns($driverImportId)
	{
		return $this->container->get('core_process_import')->getUpdatedColumns($driverImportId, 'DriverImport');
	}

	/**
	 * check date is string or numeric
	 *
	 * @param $date
	 * @return bool
	 */
	public function checkValidDate($date)
	{
		$dateArr = explode("-", $date);
		
		if (count($dateArr) >= 3) {
		
			return true;
		}
		
		return false;
	}
	
	/**
	 * get Active Cost Allocations on the basis of current Date
	 *
	 */
	public function getActiveCostAllocation($entityId, $entityType)
	{
		return $this->container->get('CostAllocation')->getActiveCostAllocation($entityId, $entityType);
	}
	
	/**
     * get Active Allocations on the basis of current Date
     *
     */
    public function checkActiveAllocation($entityId, $entityType)
    {
        return $this->container->get('core_allocation')->checkActiveAllocation($entityId, $entityType);
    }
    
	/**
     * calculations for lease contract
     *
     */
    public function contractCalculations($leaseContract)
    {
        return $this->container->get('core_contract')->contractCalculations($leaseContract);
    }
	
	/**
	 * get max length of multidimensional array
	 *
	 * @param Array $array
	 *
	 */
	public function getMaxLengthOfArray($array)
	{
		return max(array_map('count', $array));
	}
	
	/**
	 * get vehicle
	 *
	 * @param Integer $vehicleId
	 * @return vehicle object
	 */
	public function getVehicle($vehicleId)
	{
        $em = $this->mr->getManagerForClass(get_class(new Vehicle()));
        
        $vehicle = $em->getRepository('CoreBundle:Vehicle')->find($vehicleId);
		
		return $vehicle;
	}
	
	/**
     * get driver full name
     *
     * @param $driver
     *
     * @return string
     */
    public function driverFullName($driver)
    {
        if (is_object($driver) && count($driver)>0) {
        
            return $driver->getInitials()." ".$driver->getFamilyNamePrefix()." ".$driver->getFamilyName();
        }
    }   

    /**
     * Get invoice line by invoice number
     *
     * @param $invoiceNumber
     * @param $supplierId
     *
     * @return object
     */    
    public function getInvoiceLinesByInvoiceNumber($invoiceNumber, $supplierId=0)
    {
        return $this->container->get('core_invoiceline')->findInvoiceLinesForInvoice($invoiceNumber,$supplierId);
    }
    
    /**
     * Get invoice line status
     *
     * @param $invoiceLines
     *
     * @return object
     */    
    public function getInvoiceLinesStatus($invoiceLines)
    {
        $statusLabels = array(0 => '', 1 => 'approve', 2 => 'disApprove', 3 => 'processed');
        if (count($invoiceLines)>0) {
            
            $statusArr = array();
            foreach($invoiceLines as $invoiceLine) {
                $status = $invoiceLine->getStatus()=='' ? 0 : $invoiceLine->getStatus();
                $statusArr[$status] = isset($statusArr[$status]) ? $statusArr[$status]+1 : 1;
            }
            if (count($statusArr)>0) {
                if (count($statusArr)==1) return $statusLabels[key($statusArr)];
                
                
                return 'mixed';
            }
        }
    }    
    
    
    /**
     * Get invoice line allocations
     *
     * @param $invoiceLines
     *
     * @return array
     */
    public function getInvoiceLineAllocationsDetail($invoiceLines)
    {
        if (count($invoiceLines)>0) {

            foreach($invoiceLines as $invoiceLine) {
            
                $allocations = $this->getInvoiceLineAllocations($invoiceLine);

                if (count($allocations)>0) {

                    $allocations = reset($allocations);

	                if(!empty($allocations['costAllocationLevel'])) {

		                $allocationsArr = explode(",", $allocations['costAllocationLevel']);

		                return $allocationsArr[0];
	                }
                }
            }
        }
    }    
	
    /**
     * get invoiceLine other detail
     *
     * @param Integer $invoiceLine
     *
     * @return array
     */
    public function getInvoiceLineAllocations($invoiceLine)
    {
        $allocationArr = $this->container->get('core_invoiceline')->getInvoiceLineAllocations($invoiceLine);

	    if(!empty($allocationArr['costAllocations'])) {

		    $allocationArr = $allocationArr['costAllocations'];
	    }

        krsort($allocationArr);
        
        return $allocationArr;
    }
    
    /**
     * get formed Date
     *
     * @param $date
     *
     * @return array
     */
    public function formedDate($date)
    {
        return $date!='' ? $date->format('d/m/Y') : '-';
    }
    
    /**
     * get formed Amount
     *
     * @param $amount
     *
     * @return numeric
     */
    public function formedAmount($amount=0)
    {
        return '&euro; '.round($amount, 2);
    }
    
    /**
     * get formed vatPercentage
     *
     * @param $vatPercentage
     *
     * @return mixed
     */
    public function vatPercentage($vatPercentage=0)
    {
        return $vatPercentage!='' ? $vatPercentage."%" : '0%';
    }
    
    /**
     * get driver complete name
     *
     * @param $journalEntry
     *
     * @return mixed
     */
    public function getDriverCompleteName($journalEntry)
    {
	    $driverName = '';
		if (!empty($journalEntry['driverInitials'])) {

			$driverName .= $journalEntry['driverInitials']." ";
        }

	    $driverName .= $journalEntry['driverFamilyName'];

	    return $driverName;
    }    
    
    /**
     * call process Function of journal entries 
     *
     * @param $amount
     *
     * @return mixed
     */
    public function processFunction($functionName, $value, $journalEntry=false)
    {
        return $functionName=='getDriverCompleteName' ? $this->$functionName($journalEntry) : $this->$functionName($value);
    }
    
    /**
     * get max level of cost allocation
     *
     * @param $journalEntries
     *
     * @return mixed
     */
    public function getMaxCostAllocatonLevelCount($journalEntries)
    {
        $levelCountArr = array(0);
        if (count($journalEntries)>0) {
            foreach($journalEntries as $journalEntries) {
                $levelCountArr[] = count(explode(", ", $journalEntries['costAllocationLevel']));
            }
        }
        
        return max($levelCountArr);
    }
    
    /**
     * get max level of cost allocation
     *
     * @param $journalEntries
     *
     * @return mixed
     */
    public function getCostAllocationNameLevelWise($costAllocation, $level)
    {  
        if ($costAllocation!='') {
            $costAllocationArr = explode(", ",$costAllocation);
            
            return isset($costAllocationArr[$level-1]) ? $costAllocationArr[$level-1] : '';
        }
        
        return false;
    }

	/**
	 * Checks mail configuration settings for company
	 *
	 */
	public function validateCompanySettings($companyId)
	{
		return $this->container->get('core_mail')->validateCompanySettingData($companyId);
	}
	
	/**
	 * get cost allocation agianst a allocation
	 *
     * @param $allocation
     * @param $showBreadcrumbs
     *
     * @return string|null
     */	 
	public function getCostAllocationWithAllocation($allocation, $showBreadcrumbs=true)
	{
		$driverCostAllocationId = '';
		
		$driverCostAllocations =  $this->container->get('core_allocation')->getCostAllocationWithAllocation($allocation, true);
		
		if (!empty($driverCostAllocations)) {
			
			$driverCostAllocationId = $driverCostAllocations[0]->getCostAllocationId();

		}else{
			
			if($allocation->getDriver() && count($allocation->getDriver()) > 0) {
				
				$driverCostAllocations =  $this->container->get('CostAllocation')->getDriverMaxCostAllocationId($allocation->getDriver()->getId());
				
				if(!empty($driverCostAllocations)) {
				
					$driverCostAllocationId =  $driverCostAllocations->getCostAllocationId();
				}
			}
		}
		
		if ($showBreadcrumbs === true && $driverCostAllocationId > 0) {
			
			return $this->breadcrumbLinks($driverCostAllocationId);
		}
		
		return $driverCostAllocationId;
	}
	
	/**
	 * get cost allocation for pool or general use
	 *
     * @param $allocation
     *
     * @return string
     */
	public function costAllocationForPoolOrGeneralUse($allocation) 
	{
		$costPlaceBreadCrumbHtml = '';
		
		if (count($allocation->getVehicle())>0) {
            
            $startDate = $this->container->get('wbs_core')->validDate($allocation->getDateStart());
            $endDate = $this->container->get('wbs_core')->validDate($allocation->getDateEnd());
			$criteria = array('startDate' => $startDate, 'endDate' => $endDate);
			
			$costPlaceBreadCrumbHtml = $this->container->get('CostAllocation')->getcostAllocationBreadcrumbHtml($allocation->getVehicle(), 'vehicle', $criteria);
		}
		
		return $costPlaceBreadCrumbHtml;
	}
	
	/**
	 * get total EB for allocation
	 *
     * @param $allocation
     *
     * @return totalEB
     */	
     public function getAllocationTotalEB($allocation) {
     
		return $allocation->getFixedPersonalContribution()+$allocation->getBudgetOverdraw()+$allocation->getEb1()+$allocation->getEb2();
     }
     
    /**
     * get cost allocation tree
     *
     * @param $entity
     * @param $entityType
     * @param $allocation
     *
     * @return String
     */    
     public function getcostAllocationBreadcrumbHtmlWithEntity($entity, $entityType, $allocation = false)
     {
	     $breadCrumb = '-';
	     $criteria = false;
	     if (count($entity)>0) {
            
            if ($allocation!=false) {
                $startDate = $this->container->get('wbs_core')->validDate($allocation->getDateStart());
                $endDate = $this->container->get('wbs_core')->validDate($allocation->getDateEnd());
                $criteria = array('startDate' => $startDate, 'endDate' => $endDate);
            }
            $costAllocation = $this->getcostAllocationWithEntity($entity, $entityType, $criteria);
            if (count($costAllocation)>0) {
            
                $parentArr =explode("<!>", $costAllocation->getLevelNames());
	            $breadCrumb = $this->costAllocationBreadcrumbHtml($parentArr);
            } elseif ($entityType=='driver') {
                return $this->container->get('CostAllocation')->getLastMaxCostAllocation($entity, $entityType);
            }
	     }

	     return $breadCrumb;
     }
     
    /**
     * get cost alloction object
     *
     * @param $entity
     * @param $entityType
     * @param $criteria
     *
     * @return Object
     */   
     public function getcostAllocationWithEntity($entity, $entityType, $criteria=false) 
     {
        return $this->container->get('CostAllocation')->_costAllocationByEntity($entity, $entityType, $criteria);
     }
     
     /**
     * get cost alloction tree
     *
     * @param $entity
     * @param $entityType
     *
     * @return String
     */    
     public function getcostAllocationContactPerson($entity, $entityType)
     {
        if (count($entity)>0) {
            
            $costAllocation = $this->getcostAllocationWithEntity($entity, $entityType);
            if (count($costAllocation)>0) {
                if ($costAllocation->getcontactPerson() && count($costAllocation->getcontactPerson())>0) {
                
                    return $costAllocation->getcontactPerson()->getName();
                }
            }
        }
     }
     
     /**
     * Get formated value
     *
     * @param $value
     *
     * @return String
     */ 
    public function getBinaryValueToStringValue($value=false) 
    {
       return $this->container->get('wbs_core')->getBinaryValueToStringValue($value);
    }
    
    /**
     * Get contract field value
     *
     * @param $vehicle
     * @param $fieldName
     *
     * @return String
     */ 
    public function getContractAttributeName($vehicle, $fieldName) 
    {
        return $this->container->get('core_vehicle')->getContractAttributeName($vehicle, $fieldName);
    }
    
    /**
     * Get contract foriegn table value
     *
     * @param $vehicle
     * @param $fieldName
     *
     * @return String
     */ 
    public function getContractForeignAttributeName($vehicle, $fieldName) 
    {
        return $this->container->get('core_vehicle')->getContractForeignAttributeName($vehicle, $fieldName);
    }
	
	/**
	 * get array of parents
	 */
	public function getMonthName($monthNum, $year)
	{
		$jd = gregoriantojd($monthNum,1,$year);
		
		return jdmonthname($jd,0);
	}
	
	/**
	 * get array of parents
	 */
	public function calculateGoalMonths($goalCreatedAt, $goalTargetYear, $goalTargetMonth)
	{
		return $this->container->get('core_client_goal')->calculateMonths($goalCreatedAt, $goalTargetYear, $goalTargetMonth);
	}
	
	/**
	 * get array of parents
	 */
	public function calculateMonthsForAssets($date1, $date2)
	{
		return $this->container->get('core_client_goal')->returnNumberOfMonths($date1, $date2);
	}
	
	public function getFutureValue($principal, $rate, $period, $isMonthly=true)
	{
		return $this->container->get('core_client_goal')->getFutureValue($principal, $rate, $period, $isMonthly);
	}
	
	public function getRateAsPerUserAssumptionInTwig($userId, $asset)
	{
		$growthRate = 0;
		
		if(count($asset)>0) {

			if(count($asset->getAssumption())>0) {

				$assumptionId = $asset->getAssumption()->getId();
				$growthRate = $this->container->get('core_client_goal')->getRateAsPerUserAssumption($userId, $assumptionId);
			}
		}
		
		return $growthRate;
	}
	
	public function getTotalAssetValueForAGoal($clientGoal, $clientGoalAsset, $userId)
	{
		$futureValueForAssets = 0;
		
		if(count($clientGoalAsset)>0) {
			
			foreach($clientGoalAsset as $goalAsset) {
				$principal = $goalAsset->getValue();

				$fromDate = '';
				if(count($goalAsset->getAsset()) > 0) {

					if(count($goalAsset->getAsset()->getClientAsset()) > 0) {

						$clientAsset = $goalAsset->getAsset()->getClientAsset();
						foreach($clientAsset as $clientAssetSingle) {

							if($clientAssetSingle->getAsset()->getId() == $goalAsset->getAsset()->getId()) {

								$fromDate = $clientAssetSingle->getFromDate();
							}
						}
					}
				}

				$months = $this->calculateGoalMonths($fromDate, $clientGoal->getYear(), $clientGoal->getMonth());

				$rate = $this->getRateAsPerUserAssumptionInTwig($userId, $goalAsset->getAsset());
				
				if($goalAsset->getAsset() && $goalAsset->getAsset()->getInterestTypeMonthly()) {
					
					$futureValueForAssets = $this->calculateFutureValueForMonthlyInterestInTwig($principal, $rate, $months/12, 12);
				} else {
				
					$futureValueForAssets = $this->getFutureValue($principal, $rate, $months) + $futureValueForAssets;
				}
			}
		}

		return $futureValueForAssets;
	}

	public function calculateFutureValueForMonthlyInterestInTwig($principal, $rateOfInterest, $years, $noOfQuarters)
	{
		$futureValue = $this->container->get('core_client_goal')->calculateFutureValueForMonthlyInterest($principal, $rateOfInterest, $years, $noOfQuarters);
		
		return $futureValue;
	}

	public function checkIfRoleSelected($selectedPermissions, $roleId, $moduleId)
	{
		if(!empty($selectedPermissions[$roleId])) {

			if(in_array($moduleId, $selectedPermissions[$roleId])) {

				return 'checked="checked"';
			}
		}

		return '';
	}
	
	public function getPermittedActions($keyForModule)
	{
		$actions = $moduleActions = array();
		$permittedModules = $this->container->get('session')->get('modules');
		//echo '<pre>'; print_r($permittedModules); die;
		if(!empty($permittedModules) && isset($permittedModules[$keyForModule]) && count($permittedModules[$keyForModule])>0) {
			$moduleActions = $permittedModules[$keyForModule];
			if (isset($moduleActions['slug']) && count($moduleActions['slug'])>0) {
				$actions = $moduleActions['slug'];
			}
		}
		
		return $actions;
	}
	
	public function getCompanyNameFromId($companyId)
	{
		$em	=	$this->mr->getManagerForClass(get_class(new ClientGoal()));
		$companyName = '';
		
		if($companyId>0) {
			
			$company = $em->getRepository("CoreBundle:Company")->find($companyId);
			if(count($company)>0) {
				
				$companyName = $company->getName();
			}
		}
		
		return $companyName;
	}
}