<?php
namespace Swan\CoreBundle\EventListener;

use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\HttpFoundation\RedirectResponse;

class TokenListener
{
	protected $em;
	private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
	    setlocale(LC_TIME, "nl_NL");
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();
    
        /*
         * $controller passed can be either a class or a Closure. This is not usual in Symfony2 but it may happen.
         * If it is a class, it comes in array format
         */
        if (!is_array($controller)) {
            return;
        }
        
		$user = $this->container->get('security.context')->getToken() ? $this->container->get('security.context')->getToken()->getUser() : "";
		
		$request = $this->container->get('request');
		$routeName = $request->get('_route');
		
		$allowedRouteArray = array('');
		
		if (is_object($user) && !in_array($routeName, $allowedRouteArray) ) {
            
			$isAdmin = $user->getIsAdmin();
			$isUrlPermitted = $this->getAccessAsPerModuleActions($request, $isAdmin);
			
			if ($isUrlPermitted === false && $isAdmin === false) {
				
				$redirectUrl = $this->container->get('router')->generate('_core_access_denied');
                $event->setController(function() use ($redirectUrl) {
                    return new RedirectResponse($redirectUrl);
                });
			}
		}
    }
	
	private function getAccessAsPerModuleActions($request, $isAdmin)
	{
		$ignoreRoutesForPermission = array('_core_access_denied', '_core_asset_name');
		
		if (in_array($request->attributes->get('_route'), $ignoreRoutesForPermission)) {
			
			return true;
		}
		
		if (count($request->getSession()->get('moduleActions'))>0) {
			
			$routesWithType = array('_core_user_list', '_core_user_inactive', '_core_user_edit', '_core_user_new', '_core_calculator_form', '_core_user_delete', '_core_update_user_permissions');
			
			$permittedModuleActions = $request->getSession()->get('moduleActions');
			$controllerName = $request->attributes->get('_controller');
			$routeName = $request->attributes->get('_route');
			$routeParams = $request->attributes->get('_route_params');
			$type = '';
			
			$searchword = 'type';
			$matches = array();
			foreach($routeParams as $k=>$v) {
				if (stripos($k, $searchword) !== false) {
					$matches[$k] = $v;
				}
			}
			
			if(!empty($matches)) {
				$keys = array_keys($matches);
				reset($keys);
				$type = $matches[$keys[0]];
			}
			
			$url = explode("::", ltrim($controllerName, "Swan\CoreBundle\Controller"));
			
			$controllerName = explode("Controller", $url[0]);
			$actionName = $url[1];
			
			$urlToMatch = strtolower($controllerName[0]).'/'.$actionName;

			if(in_array($routeName, $routesWithType)) {
				//echo 33; die;
				if(in_array($routeName.'/'.$type, $permittedModuleActions)) {
					
					return true;
				}
			} else {
				//echo '<pre>'.$urlToMatch; print_r($permittedModuleActions); die;
				if(in_array($urlToMatch, $permittedModuleActions)) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	private function changeUserFromUrl($request, $user)
	{
		$userIdInUrl = '';
		
		if($request->get('userId') != '') {
			$userIdInUrl = $request->get('userId');
		}
		
		$loggedInUserId = $request->getSession()->get('user');
		
		$isAdmin = $user->getIsAdmin();
		if ($isAdmin !== true && $userIdInUrl != '' && $loggedInUserId != $userIdInUrl) {
			
			return false;
		}
		
		return true;
	}
}
